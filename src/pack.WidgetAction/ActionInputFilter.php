<?php
class ActionInputFilter extends ActionBase
{

	//---
	public function GetDefaultRule()
	{
		return new RuleLikeRough();	 
	}

	//---
	public function GetInnerMicro()
	{
		$val = (isset($this->parent->nav->param[$this->rule->field]))
				? $this->parent->nav->param[$this->rule->field]
				: "" ;

		$active = ($val) ? "tableSmartFilterInputActive" : "";

		$html = "<input
               type='text'
               value='{$val}'
               class='tableSmartFilterInput {$active}'
               placeholder=''
               onkeydown='FA.InputFilterKeyDown(event, this, {$this->parent->ajax->zone}, \"{$this->rule->field}\");' />";

		return $html;


	}

}
