<?php
 
/**
 * Class ActionMenuFilter
 *
 * @тип     Class
 * @package    Widget
 * @версия   2
 *
 *
 * v2 - Добавлено NO_CHOOSE_PARAMS
 *
 **/
class ActionBoolean extends ActionMenuFilter
{
    public function __construct()
    {
        $arr = func_get_args();

        if (isset($arr[0])) {
            $this->title = $arr[0];
        }
        $property = (isset($arr[1])) ? $arr[1] : array();

        $this->data = array(
            array("title" => "Yes", "equal" => 1, "data" => 1),
            array("title" => "No", "equal" => 0, "data" => 0)
        );

        if (isset($arr[3])) {
            $this->rule = $arr[3];
        } else {
            $this->rule = new RuleEqual();
        }

        if (isset($property['table']))
            $this->rule->table = $property['table'];
        if (isset($property['field']))
            $this->rule->field = $property['field'];

        if (!empty($property)) {
            foreach ($property as $key => $val) {
                $this->properties[$key] = $val;
            }
        }

        $this->uid = $this->getUniqueName();
    }


}