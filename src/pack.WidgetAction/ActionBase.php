<?php

/**
 * Class ActionBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ActionBase extends ClassBase
{
    public $name = "actions";
    public $title;
    public $uid;
    public $rule;
    public $data = array();
    public $properties = array();

    /**
     * ActionBase constructor.
     */
    public function __construct()
    {
        $arr = func_get_args();

        if (isset($arr[0])) $this->title = $arr[0];
        $property = (isset($arr[1])) ? $arr[1] : array();
        if (isset($arr[2])) $this->data = $arr[2];
        if (isset($arr[3])) {
            $this->rule = $arr[3];
        } else {

            if (method_exists($this, 'GetDefaultRule')) {
                $this->rule = $this->GetDefaultRule();
            } else {
                $this->rule = new RuleEqual();
            }

            if (isset($property['table'])) $this->rule->table = $property['table'];
            if (isset($property['field'])) $this->rule->field = $property['field'];
        }

        if (method_exists($this, 'setDefaults')) $this->setDefaults();

        if (!empty($property)) {
            foreach ($property as $key => $val) {
                $this->properties[$key] = $val;
            }
        }

        $this->uid = $this->getUniqueName();

    }

    /**
     *
     */
    protected function convertData()
    {
        if (count(current($this->data)) == 1) {

            $source = $this->data;
            $this->data = array();
            foreach ($source as $key => $item) {
                $this->data[] = array(
                    'title' => $item,
                    'equal' => $key,
                    'data' => $key
                );
            }
        }
    }

    /**
     * @param array $source
     * @param array $format
     *
     * @return array
     */
    public static function dataExtract($source = [], $format = [])
    {
        $output = [];

        if (!empty($format) && count($format) == 2) {

            foreach ($source as $row => $item) {
                $output[$item[$format[0]]] = $item[$format[1]];
            }
        }

        return $output;
    }

    /**
     *
     */
    public function Draw()
    {

        $active = (isset($this->parent->nav->param[$this->properties['field']])) ? true : false;
        $show = (isset($this->parent->nav->param["_fShow_" . $this->properties['field']])) ? true : false;
        $slide = (isset($this->parent->nav->param["_fSlide_" . $this->properties['field']])) ? true : false;
        $slide = ($active && $slide) ? false : $slide;
        $type = get_class($this);

        $value = ((isset($this->parent->nav->param[$this->rule->field])) ? $this->parent->nav->param[$this->rule->field] : "");
        $value = (!empty($value) && (method_exists($this, "PrepareValue")) ? $this->PrepareValue($value) : $value);

        $json = array(
            "key" => $this->properties['field'],
            "name" => $this->title,
            "filterOptions" => array(
                "active" => $active,
                "show" => $show,
                "slide" => $slide
            ),
            "value" => $value,
            "options" => ((method_exists($this, "GetOptions")) ? $this->GetOptions() : $this->data)
        );

        echo "<script>";
        echo "GF.Push{$type}(" . json_encode($json) . ")";
        echo "</script>";


    }


}