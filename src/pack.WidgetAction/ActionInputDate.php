<?php
class ActionInputDate extends ActionBase
{
	//---
	public function GetDefaultRule()
	{
		return new RuleDate();
	}

	//---
	public function PrepareValue($value)
	{
		$valExp = explode("_", $value);
		return $valExp[2].DS.$valExp[1].DS.$valExp[0];
	}

	/*

	public function DrawInner()
	{	  
		$val = (isset($this->parent->nav->param[$this->rule->field])) ? $this->parent->nav->param[$this->rule->field]

 	    if($val != "") 
		{
			$valExp = explode("_", $val);		
			$val = $valExp[2].DS.$valExp[1].DS.$valExp[0];		 	 	
		}	
	 			
		$this->parent->nav->freeze();
		$this->parent->nav->param[$this->rule->field]  = "replace".$this->uid;
		$this->parent->nav->param['page']				   = 1;
			
		$link  = $this->parent->nav->toString();
		
		unset($this->parent->nav->param[$this->rule->field]);
		$link2 = $this->parent->nav->toString();
		
		$this->parent->nav->unFreeze();
		
		$opacity = ($val == "") ? "opacity:0.6;" : "";
		 			
		?>
        
        <div style="padding-right:45px;">
           <button class="btn btn-small" style="margin-right:-45px; height:26px; line-height:22px; float:right; margin-top:1px;" 
           		onclick="<?php echo $this->uid; ?>Reset(); return false;">OK</button>   
        
          <div class='input-prepend' style="margin-top:1px; margin-bottom:0px;">             
                 <span class='add-on'><i class='icon-calendar'></i></span>
                 <input class='span2' type='text'  
                  style='width:125px; text-indent:5px; height:22px;' 
                   placeholder="dd/mm/yyyy" value="<?php echo $val; ?>"
    	         		id="<?php echo $this->uid; ?>" onchange="<?php echo $this->uid; ?>Change()">
                  
          </div> 
         </div>

         
          <script>
		  function <?php echo $this->uid; ?>Reset()
		  {
		 	  var str  = '<?php echo $link2; ?>';
			 <?php echo $this->parent->ajax->zone.".Go(str);"; ?>
		  }
		  
		  
		  function <?php echo $this->uid; ?>Change()
		  { 
			  var data = $('#<?php echo $this->uid; ?>').val();						    
			  var dataExp = data.split("/");
			      data = dataExp[2]+"_"+dataExp[1]+"_"+dataExp[0];		 
		  
			  var str  = '<?php echo $link; ?>';
			  str  = str.replace('replace<?php echo $this->uid; ?>', data);			 
		  
			  <?php echo $this->parent->ajax->zone.".Go(str);"; ?>
		  }
		  
		  $(function() {
			    $( "#<?php echo $this->uid; ?>" ).datepicker({
																  "dateFormat"      : "dd/mm/yy",
																  "showButtonPanel" : true,
																  "changeMonth"     : true,
																  "changeYear"      : true																   
															 })
												 .datepicker( "setDate", "<?php echo $val; ?>" );															 
 
		  }); 
           </script>
        <?php			
	 		
		
	}
	*/

}
