<?php
class ActionTabPath extends ClassBase
{
	public $name         = "tabs";
	public $title;
	public $uid;
	public $rule;
	public $data         = array();
	public $properties   = array();

	function __construct()
	{
		$arr = func_get_args();

		if (isset($arr[0])) $this->title = $arr[0];
		$property = (isset($arr[1])) ? $arr[1] : array();
		if (isset($arr[2])) $this->data = $arr[2];
		if (isset($arr[3])) {
			$this->rule = $arr[3];
		} else {

			if (method_exists($this, 'GetDefaultRule')) $this->rule = $this->GetDefaultRule();
			else                                        $this->rule = new RuleEqual();

			if (isset($property['table'])) $this->rule->table = $property['table'];
			if (isset($property['field'])) $this->rule->field = $property['field'];
		}

		if (method_exists($this, 'SetDefaults')) $this->SetDefaults();

		if (!empty($property)) {
			foreach ($property as $key => $val) {
				$this->properties[$key] = $val;
			}
		}

		$this->uid = $this->getUniqueName();

	}

	public function Draw()
	{   	
		echo "<div class='actionTab'>";					 					 

		$selected = implode("/",$this->parent->nav->path);
		
		foreach($this->data as $dkey => $dval)
		{				   
		  if( $selected == "")
			  $selected = $dval;
		 
		  $class_clr = ($selected == $dval) ? "actionTabSelected" : "actionTabUnSelected";
		  
		  if(! $this->parent->ajax)
		  	echo "<a href='{$dval}' class='{$class_clr}'>{$dkey}</a>";			 
		  else		   
		  	echo $this->parent->Link("{$this->parent->ajax->zone}.Go('{$dval}');",$dkey,"",$class_clr);
		}
		echo "</div>";	
	}
}