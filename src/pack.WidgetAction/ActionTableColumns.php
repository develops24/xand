<?php
class ActionTableColumns extends ActionBase
{
    public function SetDefaults()
    {
        $this->setProperty("field", "_ATC");

        $this->rule = new RuleDummy();
        $this->rule->field = "_ATC";
    }


    public function GetOptions()
    {
        $options    = array();
        $filterKeys = (isset($this->parent->nav->param["_ATC"])) ? explode(":", $this->parent->nav->param["_ATC"]) : array();

        foreach($this->parent->table->columns as $columns)
        {
            $options[] = array(
                $columns->key,
                $columns->name,
                (in_array($columns->key, $filterKeys))
            );

        }
        return $options;

    }

}
