<?php

/**
 * Class ActionRangeFilter
 * */

class ActionRangeFilter extends ActionBase {


    //---
    public function GetDefaultRule()
    {
        return new RuleRangeUnix();
    }

    //---
    public function PrepareValue($value)
    {

        if ($value) {
            $valExplode = explode("to", $value);
            if(count($valExplode) == 2)
            {
                $valExp = explode("_", $valExplode[0]);
                $valExplode[0] = (count($valExp) == 3) ? $valExp[2] . DS . $valExp[1] . DS . $valExp[0] : "";

                $valExp = explode("_", $valExplode[1]);
                $valExplode[1] = (count($valExp) == 3) ? $valExp[2] . DS . $valExp[1] . DS . $valExp[0] : "";
                return $valExplode[0]."to".$valExplode[1];
            }
        }
        return "to";
    }

}