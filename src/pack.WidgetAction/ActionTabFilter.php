<?php

/**
 * Class ActionTabFilter
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ActionTabFilter extends ActionBase
{
    public $name = "tabs";
    public $title;
    public $uid;
    public $rule;
    public $data = array();
    public $properties = array();

    /**
     * ActionTabFilter constructor.
     */
    public function __construct()
    {
        $arr = func_get_args();
        if (isset($arr[0])) {
            $this->title = $arr[0];
        }
        $property = (isset($arr[1])) ? $arr[1] : array();
        if (isset($arr[2])) {
            $this->data = $arr[2];
        }

        if (isset($arr[3])) {
            $this->rule = $arr[3];
        } else {
            if (method_exists($this, 'GetDefaultRule')) {
                $this->rule = $this->GetDefaultRule();
            } else {
                $this->rule = new RuleEqual();
            }

            if (isset($property['table'])) {
                $this->rule->table = $property['table'];
            }
            if (isset($property['field'])) {
                $this->rule->field = $property['field'];
            }
        }

        if (method_exists($this, 'setDefaults')) {
            $this->setDefaults();
        }
        if (!empty($property)) {
            foreach ($property as $key => $val) {
                $this->properties[$key] = $val;
            }
        }

        $this->uid = $this->getUniqueName();

    }

    /**
     *
     */
    public function Draw()
    {

        echo "<div class='actionTab'>";

        $selected = (isset($this->parent->nav->param[$this->rule->field]))
            ? $this->parent->nav->param[$this->rule->field]
            : "NO_CHOOSE_PARAMS";

        echo $this->parent->Link("{$this->parent->ajax->zone}.Link({page:1}, {{$this->rule->field} : 1});",
            (isset($this->properties["noChooseValue"])) ? $this->properties["noChooseValue"] : "Not selected",
            "",
            ($selected == "NO_CHOOSE_PARAMS") ? "actionTabSelected" : "actionTabUnSelected");

        if (!empty($this->data)) {

            $this->convertData();

            foreach ($this->data as &$val) {
                $value = (!is_numeric($val['data'])) ? "'" . trim($val['data']) . "'" : $val['data'];
                echo $this->parent->Link("{$this->parent->ajax->zone}.Link({page:1,{$this->rule->field} : {$value}});",
                    $val['title'],
                    "",
                    ($selected == $val['equal']) ? "actionTabSelected" : "actionTabUnSelected");
            }
        }

        echo "</div>";


    }
}