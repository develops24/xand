<?php

/**
 * Checkbox
 */
class ActionCheckboxMultiple extends ActionBase {

    public function GetOptions()
    {

        $prepend = '';
        if (isset($this->parent->nav->param[$this->rule->field]))
        {
            $current = $this->parent->nav->param[$this->rule->field];
            if (is_numeric($this->parent->nav->param[$this->rule->field])) {
                $complex[] = $this->parent->nav->param[$this->rule->field];
            } else {
                $complex = explode(':', $this->parent->nav->param[$this->rule->field]);
            }

            $selected = array_unique($complex);
            if (!is_numeric($current)) {
                foreach ($selected as $sl) {
                    $prepend.=$sl . ':';
                }
            }
        }
        else
        {
            $selected[0] = "NO_CHOOSE_PARAMS";
        }

        if ($selected[0] == 'NO_CHOOSE_PARAMS')
        {
            unset($selected[0]);
            if (isset($this->properties['defaultValue'])) {
                foreach ($this->properties['defaultValue'] as $df) {
                    $selected[] = $df;
                }
            }
        }

        /* old format convertor */
        if (isset($this->data[0]['equal'])) {

            $oldData    = $this->data;
            $this->data = array();

            foreach($oldData as $v)
            {
                $this->data[$v["equal"]] = $v["title"];
            }

            //Message::I()->Warning("Deprecated format (filter: {$this->title}): {data, title, equal}, you should use - Key:Value")->Draw();
        }
        /* old format convertor */

        $options   = array();

        if (!empty($this->data)) {
            foreach ($this->data as $key => &$val) {
                $prepend = '';
                $chosen = false;
                if (in_array($key, $selected)) {
                    $chosen = true;
                    $final = array_diff($selected, array($val));
                    foreach ($final as $sl) {
                        if (!empty($sl)) {
                            $prepend.=$sl . ':';
                        }
                    }
                } else {
                    foreach ($selected as $sl) {
                        if (!empty($sl)) {
                            $prepend.=$sl . ':';
                        }
                    }
                }

                $active    = (in_array($key, $selected)) ? true : false;
                $disabled  = (isset($this->properties['allowValues']) && ! in_array((int) $key, $this->properties['allowValues'])) ? true : false;

                $options[] = array($key, $val, $active, $disabled);

            }
        }

        return $options;


    }

}