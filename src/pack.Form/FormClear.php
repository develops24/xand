<?php
/**
 * Class FormClear
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   1
 *
 *
 * 
 *
 **/
class FormClear extends FormBase
{
	public function DrawHead()
	{			
		echo $this->name;			
		return $this;
	}	
	
	public function DrawFooter()
	{		
		if(isset($this->buttons))
			$this->DrawButtons();

		return $this;
	}
}
?>