<?php
/**
 * Class FormStatic
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   1
 *
 *
 * 
 *
 **/
class FormStatic extends FormBase
{
	public function DrawHead()
	{
		$style = ($this->getProperty("width")) ? "width: ".$this->properties["width"]."px; " : "";

		echo "<div align='left' class='formStatic' id='form{$this->domId}' style='{$style}'>";
		echo '<div class="formStaticTitle">'.$this->name.'</div>';		
		echo '<div class="formStaticContent">';		
		return $this;
	}	
	
	public function DrawFooter()
	{
		echo "</div>";
		if(isset($this->buttons))
		{				
			echo "<div class='formBaseButtons'> ";
			$this->DrawButtons();
			echo "</div>";	
		}	
		echo "</div>";
		echo "<div class='widgetMessages'></div>";
		return $this;
	}
}