<?php
/**
 * Class FormVirtual
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   1
 *
 *
 * 
 *
 **/
class FormVirtual extends FormBase
{
	public function DrawHead()
	{			
		return $this;
	}	
	
	public function DrawFooter()
	{		
		return $this;
	}
	
	public function AjaxReady()
	{	
		
		echo "<script>";            
		echo $this->getProperty("callback")."OnGetData = function(){ var forms_data = {";				
			$result = "";
			if(isset($this->tabs))		    foreach($this->tabs   as $key => $val)	$result .= $val->GetDataJson();
			if(isset($this->inputs))	    foreach($this->inputs as $key => $val)	$result .= $val->GetDataJson();
			echo substr($result,2, strlen($result));				
			echo "}; var packData = $.toJSON(forms_data);  packData = packData.replace(/\"/g, '^');
				  $('#".$this->getProperty("callback")."').val(packData);}; </script>";
		
		return $this;
	}
	
	
}
?>
