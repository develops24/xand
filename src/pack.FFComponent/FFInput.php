<?php

/**
 * Class FFInput
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFInput extends FFObjectBase
{
    /**
     * @param array $options
     */
    protected function SetDefaults($options = array())
    {
        $this->requiredRule = new ValidateText;
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function RenderBody($data)
    {
        return "<li> <input type='text'
                            name='{$this->name}'                            
                            value='{$data}'
                            placeholder='{$this->placeholder}'
                            class='{$this->class}'
                            required-type='{$this->requiredRule->name}'
                            {$this->requiredTT}{$this->readonly}>
                            </li>";
    }

}