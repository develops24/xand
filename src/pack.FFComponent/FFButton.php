<?php

/**
 * Class FFButton
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFButton extends FFObjectBase implements IFFActions
{
    /**
     *
     */
    protected function SetDefaults() {
        $this->options["align"] = (isset($this->options["align"])) ? $this->options["align"] : "center";

    }

    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data) {
        $class = ($this->class) ? $this->class." button" : "button";
        return "<button type='button' class='{$class}'>{$this->title}</button>";
    }


}