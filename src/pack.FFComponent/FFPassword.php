<?php

class FFPassword extends FFObjectBase
{
    //---
    public function SetDefaults($options = array())
    {
        $this->requiredRule  = new ValidatePassword;
    }


    //---
    protected function RenderBody($data)
    {
        return  "<li><input type='password'
                            name='{$this->name}'                          
                            value='{$data}'
                            required-type='{$this->requiredRule->name}'
                            placeholder='{$this->placeholder}'
                            class='{$this->class}'
                            {$this->requiredTT}{$this->readonly}>
                 </li>";
    }

}