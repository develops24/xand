<?php

/**
 * Class FFSelect
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFSelect extends FFObjectBase
{
    public $options = array();
    public $allowEmpty = true;
    public $action = "";
    public $emptyValue = "";

    /**
     * @param $options
     */
    protected function SetDefaults($options)
    {
        $this->requiredRule = new ValidateSelect;

        if (isset($options["allowEmpty"])) $this->allowEmpty = $options["allowEmpty"];

        $this->emptyValue = (isset($options["emptyValue"])) ? $options["emptyValue"] : I18n::__("%Not selected%");
        $this->action = (isset($options["action"])) ? "onchange='" . $options["action"] . "'" : "";
        $this->options = (isset($options["options"])) ? $options["options"] : array();

    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function RenderBody($data)
    {
        $html = "";
        $data = (int)$data;

        $html .= "<li><select name='{$this->name}' required-type='{$this->requiredRule->name}'
                      class='{$this->class}'{$this->requiredTT}{$this->readonly}>";

        if ($this->allowEmpty) {
            $html .= "<option value='0'>- {$this->emptyValue} -</option>";
        }

        if ($this->options)
            foreach ($this->options as $k => $v) {
                $selected = ($data === $k) ? "selected='selected'" : "";
                $html .= "<option value='{$k}' {$selected}>{$v}</option>";
            }

        $html .= "</select></li>";

        return $html;
    }

}