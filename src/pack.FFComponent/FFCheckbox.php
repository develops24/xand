<?php

/**
 * Class FFCheckbox
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFCheckbox extends FFObjectBase
{
    /**
     * @param array $options
     */
    public function SetDefaults($options = array())
    {
        $this->requiredRule = new ValidateExists;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        $requiredAster = ($this->required) ? " <b>*</b>" : "";

        return "
                 <ul>  
                        <li>
                            <input type='checkbox'
                            name='{$this->name}'
                            value='{$data}'
                            placeholder='{$this->placeholder}'
                            required-type='{$this->requiredRule->name}'
                            {$this->requiredTT}{$this->readonly} />

                            {$this->title}{$requiredAster}
                      </li>
                </ul>
                ";
    }


}