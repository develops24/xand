<?php

/**
 * Class FFRadio
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFRadio extends FFObjectBase
{
    public $options = array();

    /**
     * @param $options
     */
    protected function SetDefaults($options)
    {
        $this->options = (isset($options["options"])) ? $options["options"] : array();
        $this->titleWidth = (isset($options["titleWidth"])) ? $options["titleWidth"] : "16";

    }

    /**
     * @return string
     */
    protected function RenderTitle()
    {
        $titleWidth = ($this->titleWidth) ? " style='width:{$this->titleWidth}px;' " : "";
        return "<li{$titleWidth}></li>";
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function RenderBody($data)
    {
        $html = "";
        $iter = 1;
        $html .= "<li style='text-align: left;' class='radioWrap'>";

        foreach ($this->options as $k => $v) {
            $selected = ($data == $k) ? "checked='checked'" : "";
            $html .= "<label style='padding: 0; padding-bottom: 5px; height: 16px;'>
                            <input type='radio' name='{$this->name}' id='{$this->key}-{$iter}' value='{$k}' {$selected} />
                            <span>{$v}</span>
                      </label>";

            $iter++;
        }

        $html .= "</li>";

        return $html;


    }


}