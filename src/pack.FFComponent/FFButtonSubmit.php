<?php

/**
 * Class FFButtonSubmit
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFButtonSubmit extends FFButton
{
    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        return "<button type='submit' class='button'>{$this->title}</button>";
    }

}