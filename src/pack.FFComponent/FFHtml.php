<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 10.12.15
 * Time: 12:48
 */
class FFHtml extends FFObjectBase
{
    /**
     * Для вывода обычной верстки в кастомных случаях
     * передавать как new FFHtml(null,null,['data'=>'<html-code/>']
     * @param string $data html-код
     * @return string
     */
    public function Render($data) {
        $data = (isset($this->data)) ? $this->data : $data;
        return $data;
    }
}