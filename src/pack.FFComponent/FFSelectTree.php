<?php

/**
 * Class FFSelectTree
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFSelectTree extends FFSelect
{

    public $options = array();
    public $allowEmpty = true;
    public $action = "";
    public $parent = 0;

    /**
     * @param $options
     */
    protected function SetDefaults($options)
    {
        $this->requiredRule = new ValidateSelect;

        if (isset($options["allowEmpty"])) $this->allowEmpty = $options["allowEmpty"];
        if (isset($options["parent"])) $this->parent = $options["parent"];

        $this->emptyValue = (isset($options["emptyValue"])) ? $options["emptyValue"] : I18n::__("%Not selected%");
        $this->action = (isset($options["action"])) ? "onchange='" . $options["action"] . "'" : "";
        $this->options = (isset($options["options"])) ? $options["options"] : array();
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function RenderBody($data)
    {
        $data = (int)$data;

        if ($this->allowEmpty) {
            $options = "<option value='0'>- {$this->emptyValue} -</option>";
        } else {
            $options = '';
        }
        $optionCount = 0;

        foreach ($this->options as $k => $v) {

            $selected = ($data == $k) ? "selected='selected'" : "";
            $options .= "<option value='{$k}' {$selected}>{$v}</option>";
            $optionCount++;
        }

        $disable = ($optionCount == 0) ? "disabled='disabled'" : "";

        $req1 = ($this->required) ? "reqSelect" : "";
        $req2 = ($this->required) ? "required" : "";

        $html = "<li>
                    <select data-path='" . PATH_DS . SITE_CURRENT_LANG . "'  name='{$this->name}'  required-type='{$this->requiredRule->name}'
                        class='data {$req1}' {$this->action} {$disable} {$req2}>
				   {$options}
				   </select>
				 </li>";

        return $html;

    }

}