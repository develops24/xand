<?php

/**
 * Class FFButtonLink
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFButtonLink extends FFButtonSubmit
{

    public $url;
    public $class;

    /**
     * FFButtonLink constructor.
     *
     * @param string $title
     * @param string $url
     * @param array $options
     */
    public function __construct($title = "Unnamed", $url = "", $options = array())
    {
        $this->title = $title;
        $this->url = $url;

        $this->class = (isset($options["class"])) ? $options["class"] : "";
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        return "<a href='" . PATH_DS . SITE_CURRENT_LANG . DS . $this->url . "' class='button {$this->class}'>{$this->title}</a>";
    }

}