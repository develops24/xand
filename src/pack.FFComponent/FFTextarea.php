<?php

class FFTextarea extends FFObjectBase
{
    //---
    protected function SetDefaults($options = array())
    {
        $this->requiredRule =  new ValidateText;
    }

    //---
    protected function RenderBody($data)
    {
        return  "<li>
                           <textarea type='text'
                            name='{$this->name}'                              
                            value='{$data}'
                            placeholder='{$this->placeholder}'
                            class='{$this->class}'
                            required-type='{$this->requiredRule->name}'
                            {$this->requiredTT}{$this->readonly}></textarea>
                 </li>";
    }

}