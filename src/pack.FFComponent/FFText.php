<?php

class FFText extends FFObjectBase
{
    public $html = "";

    //---
    public function __construct($html = "empty")
    {
        $this->html  = $html;
    }

    //---
    public function Render($data)
    {
        return "<div style='padding:10px;'>{$this->html}</div>";
    }

}