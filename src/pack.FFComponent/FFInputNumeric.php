<?php

class FFInputNumeric extends FFObjectBase
{
    public $step, $min;

    //---
    protected function SetDefaults($options = array()) {
        $this->requiredRule = new ValidateExists;
        $this->step = (isset($options["step"])) ? $options["step"] : "0.01";
        $this->min = (isset($options["min"])) ? (int)$options["min"] : null;
    }

    //---
    protected function RenderBody($data) {
        return "<li><input type='number'
                            name='{$this->name}'                             
                            value='{$data}'
                            step='{$this->step}'
                            " . ($this->min !== null ? "min='{$this->min}'" : "") . "
                            placeholder='{$this->placeholder}'
                            class='{$this->class}'
                            required-type='{$this->requiredRule->name}'
                            {$this->requiredTT}{$this->readonly}></li>";
    }

}