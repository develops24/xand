<?php

class FFBlock extends FFObjectBase
{
    public $html = "";

    //---
    public function __construct($html = "empty")
    {
        $this->html  = $html;
    }

    //---
    public function Render($data)
    {
        return "<div>{$this->html}</div>";
    }

}