<?php

class FFBoxRequired extends FFObjectBase
{
    public $align = "center";

    //---
    public function SetDefaults($options = array())
    {
        $this->title = "";
        $this->key   = "_boxRequired";
        $this->align = (isset($options["align"]))    ? $options["align"]     : $this->align;
    }

    //---
    public function Render($data)
    {
        if($this->align == "left")
            return "<ul><li><label><b>*</b> - ".I18n::__("%Required fields%")."</label></li></ul>";
        else
            return "<ul><li></li><li><label><b>*</b> - ".I18n::__("%Required fields%")."</label></li></ul>";
    }

}