<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 08.12.15
 * Time: 9:20
 */
class FFInputHidden extends FFInput
{

    /**
     * @param $data
     * @return string
     */
    public function RenderBody($data) {
        return "<input type='hidden' name='{$this->name}' value='{$data}'>";
    }

    /**
     * @return string
     */
    protected function RenderTitle() {
        return "";
    }
}