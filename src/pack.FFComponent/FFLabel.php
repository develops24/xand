<?php

/**
 * Class FFLabel
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFLabel extends FFObjectBase
{
    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        $data = (isset($this->data)) ? $this->data : $data;

        return "<ul>
                    <li><label class='title'>{$this->title}</label></li>
                    <li><input type='text' readonly='readonly' class='label' value='{$data}'></li>
                </ul>
                ";
    }


}