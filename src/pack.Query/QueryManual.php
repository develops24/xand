<?php

/**
 * Class QueryManual
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class QueryManual extends QueryBase
{

    public $queryBody;
    public $queryCount;
    public $queryBodyHolders;
    public $useCache;

    /**
     * QueryManual constructor.
     *
     * @param $queryBody
     * @param array $queryBodyHolders
     * @param $queryCount
     * @param bool $useCache
     */
    function __construct($queryBody, $queryBodyHolders = [], $queryCount, $useCache = false)
    {
        $this->queryBody = $queryBody;
        $this->queryCount = $queryCount;
        $this->queryBodyHolders = $queryBodyHolders;
        $this->useCache = $useCache;
    }

    /**
     * @return array|bool|mixed|mysqli_result
     */
    public function execute()
    {
        $output = $this->queryBody;
        $output .= ($this->limit == false) ? "" : " LIMIT " . $this->limit;

        $result = (!$this->useCache)
            ? SDb::rows($output, $this->queryBodyHolders)
            : SDbM::rows($output, $this->queryBodyHolders);

        return $result;
    }

    /**
     * @return int
     */
    public function countExecute()
    {
        return SDb::cell($this->query_count);
    }

}