<?php

/**
 * Class QueryMulti
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class QueryMulti extends QueryBase
{

    public $joins = "";
    public $orderPriority = "";
    public $groupBy = "";
    public $having = "";

    public $counterCB = null;

    /**
     * @param $order
     * @param bool $desc
     */
    public function setPriorityOrder($order, $desc = false)
    {
        $dir = ($desc) ? ' DESC' : ' ASC';
        $this->orderPriority = $order . $dir;
    }

    /**
     * @param $bind
     *
     * @return $this
     */
    public function Bind($bind)
    {
        array_push($this->filters, $bind);
        return $this;
    }

    /**
     * @param $table
     * @param $filter
     *
     * @return $this
     */
    public function Join($table, $filter)
    {
        $tableOrig = $table;
        if (strpos($table, "AS") !== false) {
            $tmp = explode("AS", $table);
            $table = trim($tmp[1]);
        }

        foreach ($this->tables as $key => $val)
            if ($val == $table)
                unset($this->tables[$key]);

        $this->joins[$tableOrig] = " JOIN {$tableOrig} ON " . $filter . " ";
        return $this;
    }

    /**
     * @param $table
     * @param $filter
     *
     * @return $this
     */
    public function LeftOuterJoin($table, $filter)
    {
        $tableOrig = $table;
        if (strpos($table, "AS") !== false) {
            $tmp = explode("AS", $table);
            $table = trim($tmp[1]);
        }

        foreach ($this->tables as $key => $val)
            if ($val == $table)
                unset($this->tables[$key]);

        $this->joins[$tableOrig] = " LEFT OUTER JOIN {$tableOrig} ON " . $filter . " ";
        return $this;
    }

    /**
     * @param $table
     * @param $filter
     * @return $this
     */
    public function RightOuterJoin($table, $filter)
    {
        $tableOrig = $table;
        if (strpos($table, "AS") !== false) {
            $tmp = explode("AS", $table);
            $table = trim($tmp[1]);
        }

        foreach ($this->tables as $key => $val)
            if ($val == $table) {
                unset($this->tables[$key]);
            }
        $this->joins[$tableOrig] = " RIGHT OUTER JOIN {$tableOrig} ON " . $filter . " ";
        return $this;
    }

    /**
     * @param $groupBy
     *
     * @return $this
     */
    public function GroupBy($groupBy)
    {
        $this->groupBy = " GROUP BY " . $groupBy . " ";
        return $this;
    }

    /**
     * @param string $having
     * @return $this
     */
    public function having($having = "")
    {
        $this->having = " HAVING " . $having . " ";
        return $this;
    }

    /**
     * @return string
     */
    public function execute()
    {
        $distinct = ($this->distinct) ? " DISTINCT " : "";

        $output = " SELECT " . $distinct . implode(" , ", $this->select);
        $output .= " FROM " . implode(" , ", $this->tables);

        if (!empty($this->joins))
            $output .= " " . implode(" ", $this->joins);


        $output .= (!empty($this->criteria)) ? " WHERE " . implode(' AND ', $this->criteria) : "";
        $output .= $this->groupBy;
        if($this->having)
            $output .= $this->having;

        $priorityOrder = ($this->orderPriority) ? $this->orderPriority . "," : "";

        if ($this->order != false)
            $output .= ($this->order == false) ? "" : " ORDER BY " . $priorityOrder . $this->order;

        $output .= ($this->limit == false) ? "" : " LIMIT " . $this->limit;

        $data = SDb::rows($output, $this->holders);

        if (SDbBase::getLastError()) {
            Message::I()->Error(SDbBase::getLastError());
            echo SqlFormatter::format($output);
        }

        return $data;
    }

    /**
     * @param $cb
     */
    public function setCounterCallBack($cb)
    {
        $this->counterCB = $cb;
    }

    /**
     * @return int
     */
    public function countExecute()
    {
        if ($this->counterCB)
            return $this->counterCB($this);

        $output = " SELECT COUNT(*) AS qty";
        if ($this->having) {
            $output.=", ". implode(" , ", $this->select);
        }
        $output .= " FROM " . implode(" , ", $this->tables);
        if (!empty($this->joins))
            $output .= " " . implode(" ", $this->joins);
        $output .= (!empty($this->criteria)) ? " WHERE " . implode(' AND ', $this->criteria) : "";
        $output .= $this->groupBy;
        if($this->having)
            $output .= $this->having;

        if (!$this->groupBy)
            $count = SDb::cell($output, $this->holders);
        else
            $count = count(SDb::rows($output, $this->holders));

        return $count;
    }

}
