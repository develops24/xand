<?php

/**
 * Class QueryArray
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class QueryArray extends QueryBase
{

    public $data;

    /**
     * QueryArray constructor.
     *
     * @param array $data
     */
    function __construct($data = array())
    {
        $this->data = $data;
        $this->order = trim($this->order, '`');
    }

    /**
     * @param $a
     * @param $b
     *
     * @return int
     */
    public function cmp($a, $b)
    {

        $this->order = trim($this->order, '`');

        if ($this->order == "") return 0;

        if ($a[$this->order] == $b[$this->order]) {
            return 0;
        }
        return ($a[$this->order] < $b[$this->order]) ? -1 : 1;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return int
     */
    public function cmpDesc($a, $b)
    {

        $this->order = trim($this->order, '`');
        if ($a[$this->order] == $b[$this->order]) {
            return 0;
        }
        return ($a[$this->order] > $b[$this->order]) ? -1 : 1;
    }

    /**
     * @return array
     */
    public function execute()
    {

        $fSort = (substr_count($this->order, "DESC") == 1) ? "cmpDesc" : "cmp";
        $this->order = str_replace("DESC", "", $this->order);
        $this->order = trim($this->order);


        if (empty($this->data))
            return array();

        if (!isset($this->properties["disableSort"]))
            usort($this->data, array($this, $fSort));

        $lim = explode(",", $this->limit);
        $lim[0] = (int)$lim[0];
        $lim[1] = $lim[0] + (int)$lim[1];

        $output = array();

        $iterator = 0;

        foreach ($this->data as $row) {
            if ($iterator >= $lim[0])
                $output[] = $row;

            if ($iterator > $lim[1])
                break;

            $iterator++;
        }

        return $output;
    }

    /**
     * @return int
     */
    public function countExecute()
    {
        return count($this->data);
    }

}
