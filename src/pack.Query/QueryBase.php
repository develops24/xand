<?php

/**
 * Class QueryBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class QueryBase extends ClassBase
{

    public $name = "query";

    public $select = array();
    public $tables = array();
    public $filters = array();
    public $filtersRaw = array();
    public $criteria = array();
    public $holders = array();
    public $order;
    public $distinct = false;
    public $limit;

    /**
     * QueryMulti constructor.
     */
    function __construct()
    {
        $args = func_get_args();
        foreach ($args as &$val) {

            $val = str_replace("]", "", $val);
            $valArr = explode("[", $val);
            array_push($this->tables, $valArr[0]);


            $fields = explode(",", $valArr[1]);
            $result = array();

            foreach ($fields as $filed) {
                $filed = trim($filed);

                if (substr_count($filed, "GROUP_CONCAT") == 0 &&
                    substr_count($filed, "SELECT") == 0 &&
                    substr_count($filed, "COUNT") == 0 &&
                    substr_count($filed, "CONCAT") == 0 &&
                    substr_count($filed, "ROUND") == 0
                ) {
                    $result[] = $valArr[0] . "." . $filed;
                } else {
                    $result[] = $filed;
                }
            }
            array_push($this->select, implode(", ", $result));
        }
    }


    /**
     * @param $args
     *
     * @return $this
     */
    public function Select($args)
    {
        foreach ($args as $table => $val) {
            $this->tables[] = $table;

            foreach ($val as &$v) {
                if (strpos($v, "(") === false)
                    $v = $table . "." . $v;
            }
            $this->select[] = implode(",", $val);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function setDistinct()
    {
        $this->distinct = true;
        return $this;
    }


    /**
     * @param $filter
     *
     * @return $this
     */
    public function addFilter($filter)
    {

        if (is_array($filter)) {
            array_push($this->filters, $filter);
        } else {
            array_push($this->filtersRaw, $filter);
        }

        return $this;
    }

    /**
     * @param $sub_query
     *
     * @return $this
     */
    public function addInnerQuery($sub_query)
    {
        array_push($this->select, $sub_query);
        return $this;
    }

    /**
     * @param $joins
     *
     * @return $this
     */
    public function addJoin($joins)
    {
        $this->joins = $this->joins . " " . $joins . " ";
        return $this;
    }

    /**
     * @return $this
     */
    public function process()
    {
        $r = (array)$this->parent;

        if (isset($this->parent->nav->param['escape_order'])) {
            $esc = $this->parent->nav->param['escape_order'];
        } else {
            $esc = true;
        }
        $escChar = ($esc === true) ? '`' : '';
        $DESC = (isset($this->parent->nav->param['desc']) && $this->parent->nav->param['desc'])
            ? " DESC" : "";
        $this->order = "{$escChar}" . $this->parent->nav->param['order'] . "{$escChar}" . $DESC;

        if (isset($_GET["mode"]) && $_GET["mode"] == "csv")
            $this->parent->nav->param['ppage'] = 100000;

        $this->setHolders();
        $this->limit = $this->parent->pager->Calculate($this->countExecute());

        return $this;
    }


    /**
     * @return $this
     */
    public function setHolders()
    {
        if (!empty($this->filters))
            foreach ($this->filters as $filter) {
                $this->criteria[] = $filter[0];
                if (is_array($filter[1])) {
                    foreach ($filter[1] as $f) {

                        $this->holders[] = $f;
                    }
                } else {
                    $this->holders[] = $filter[1];
                }
            }

        if (!empty($this->filtersRaw))
            foreach ($this->filtersRaw as $fr) {
                if (empty($fr))
                    continue;
                $this->criteria[] = $fr;
            }

        return $this;
    }


    /**
     * @param $order
     * @param string $dir
     */
    public function setOrder($order, $dir = 'ASC')
    {
        //        $dir = ($dir!=='ASC') ? 'DESC': 'ASC';
        //        $this->parent->nav->param['order'] = '`'.$order.'` '.$dir;
    }

}
