<?php

/**
 * Class SDbQuery
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class SDbQuery
{

    const QUERY_INSERT = 0;
    const QUERY_UPDATE = 1;
    const QUERY_DELETE = 2;

    const SQL_QUOTE = "`";

    protected static $_instance;
    private $_table;
    private $_fields;
    private $_holders;
    private $_values;
    private $_criteria;
    private $_sorting;
    private $_limit;

    private $_postProcessEnable = false;
    private $_lastAction = false;
    private $_actionMessages = [];


    /**
     * Message constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param $tableName
     *
     * @return SDbQuery
     */
    public static function table($tableName)
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        self::$_instance->reset();
        self::$_instance->_table = $tableName;

        return self::$_instance;
    }

    public function getTableName()
    {
        return $this->_table;
    }


    /**
     *
     */
    public function reset()
    {
        $this->_table = false;
        $this->_holders = array();
        $this->_fields = array();
        $this->_values = array();

        $this->_criteria = "";
        $this->_sorting = "";
        $this->_limit = "";

        $this->_actionMessages = [
            self::QUERY_INSERT => [
                "success" => "The new record is successfully added",
                "error" => "An error occurred while adding an entry"
            ],
            self::QUERY_UPDATE => [
                "success" => "The record was successfully updated",
                "error" => "An error occurred while updating the record"
            ],
            self::QUERY_DELETE => [
                "success" => "Recording has been successfully removed",
                "error" => "An error occurred while deleting the recording"
            ]
        ];

    }

    /**
     * @param array $fields
     *
     * @return string
     */
    private function _getWrappedFields($fields = [])
    {
        $result = "";

        if (!empty($fields))
            foreach ($fields as $f) {
                $result .= (strpos($f, " ") === false) ? self::SQL_QUOTE . $f . self::SQL_QUOTE . "," : $f . ",";
            }

        return trim($result, ",");
    }

    /**
     * @param array $row
     * @return bool
     */
    public function insert($row = [])
    {
        if (empty($row))
            return false;

        $this->_prepareValues($row);
        $holdersStr = implode(",", $this->_holders);
        $fieldsStr = $this->_getWrappedFields($this->_fields);


        $query = "INSERT INTO `{$this->_table}` ({$fieldsStr}) VALUES ({$holdersStr})";

        $values = [];
        foreach ($row as $r) {
            if ($r !== NULL)
                $values[] = $r;
        }

        SDbBase::query($query, $values);

        if ($this->_postProcessEnable) {
            $this->_lastAction = self::QUERY_INSERT;
            $this->postProcessAction();
        }

        return (SDbBase::getLastError()) ? false : true;
    }

    /**
     * @param array $values
     */
    private function _prepareValues($values = array())
    {
        $this->_holders = [];

        if (!empty($values))
            foreach ($values as $key => $value) {
                if ($value === null) continue;
                $this->_holders[] = "?";
                $this->_fields[] = $key;
            }
    }

    /**
     * @return $this
     */
    public function postProcessAction()
    {
        if ((isset($this->_actionMessages[$this->_lastAction]))) {

            if (SDbBase::getLastError()) {
                Message::I()->Error($this->_actionMessages[$this->_lastAction]["error"] . ": " . SDb::getLastError());
            } else {
                Message::I()->Success($this->_actionMessages[$this->_lastAction]["success"]);
            }
        }
        return $this;
    }

    /**
     * @param array $row
     *
     * @return bool
     */
    public function update($row = array())
    {
        if (empty($row))
            return false;

        $this->_prepareValues($row);
        $holdersStr = "";
        foreach ($row as $key => $val) {
            $holdersStr .= self::SQL_QUOTE . $key . self::SQL_QUOTE . "=?,";
        }

        $holdersStr = trim($holdersStr, ",");
        $valuesMerged = array_merge($row, $this->_values);
        $valuesMerged = array_values($valuesMerged);

        $query = "UPDATE `{$this->_table}` SET {$holdersStr} {$this->_criteria}";
        $result = SDbBase::query($query, $valuesMerged);

        if ($this->_postProcessEnable) {
            $this->_lastAction = self::QUERY_UPDATE;
            $this->postProcessAction();
        }

        return (SDbBase::getLastError()) ? false : true;
    }

    /**
     * @param array $fields
     *
     * @return array|false
     */
    public function select($fields = array())
    {
        $fieldsStr = (empty($fields)) ? "*" : $this->_getWrappedFields($fields);
        $query = "SELECT {$fieldsStr} FROM `{$this->_table}`{$this->_criteria} {$this->_sorting} {$this->_limit}";

        return SDb::rows($query, $this->_values);

    }


    /**
     * @param array $fields
     *
     * @return array|mixed
     */
    public function selectRow($fields = array())
    {
        $result = $this->select($fields);
        return (empty($result)) ? array() : current($result);
    }

    /**
     * @param string $cell
     *
     * @return bool
     */
    public function selectCell($cell = "")
    {
        $result = $this->selectRow(array($cell));

        if (!empty($result)) {

            if (isset($result[$cell]))
                return $result[$cell];
        }

        return false;
    }


    /**
     * @return bool
     */
    public function delete()
    {

        $query = "DELETE FROM `{$this->_table}`{$this->_criteria}";
        $result = SDbBase::query($query, $this->_values);

        if ($this->_postProcessEnable) {
            $this->_lastAction = self::QUERY_DELETE;
            $this->postProcessAction();
        }

        return $result;
    }

    /**
     * @param array $sorting
     *
     * @return string
     */
    public function sort($sorting = array())
    {
        $this->_sorting = "";

        if (!empty($sorting)) {

            $this->_sorting .= " ORDER BY ";

            foreach ($sorting as $sField => $asc) {

                $ascStr = ($asc) ? "ASC" : "DESC";
                $this->_sorting .= "`{$sField}` {$ascStr},";
            }

            $this->_sorting = trim($this->_sorting, ",");
        }

        return $this;
    }

    /**
     * @param bool $from
     * @param bool $qty
     *
     * @return $this
     */
    public function limit($from = false, $qty = false)
    {
        $this->_limit = "";

        if ($from !== false && $qty !== false) {
            $this->_limit = "LIMIT {$from}, {$qty}";
        }

        return $this;
    }

    /**
     * @param array $criteria
     *
     * @return $this
     */
    public function where($criteria)
    {
        if (!empty($criteria))
            foreach ($criteria as $key => $value) {
                $this->_holders[] = "{$key} =?";
                $this->_values[] = $value;
            }

        $this->_criteria = "";
        if (!empty($this->_holders)) {
            $this->_criteria = " WHERE ";
            $this->_criteria .= implode(" AND ", $this->_holders);
        }

        return $this;
    }

    /**
     * @param string $criteriaString
     *
     * @return $this
     */
    public function whereRaw($criteriaString = "")
    {
        $this->_criteria = ($criteriaString) ? " WHERE " . $criteriaString : "";
        return $this;
    }


    /**
     * @param array $messages
     *
     * @return $this
     */
    public function enableMessages($messages = array())
    {
        if (!empty($messages))
            $this->_actionMessages = $messages;

        $this->_postProcessEnable = true;

        return $this;

    }

    /**
     *
     */
    public function count()
    {
        $query = "SELECT COUNT(*) as cnt FROM `{$this->_table}`{$this->_criteria}";
        $res = SDb::row($query, $this->_values);
        return (isset($res["cnt"])) ? $res["cnt"] : 0;
    }

    /**
     * @param array $row
     *
     * @return bool
     */
    public function upsert($row = array())
    {
        $count = $this->count();

        if ($count == 0) {
            return $this->insert($row);
        } else {
            return $this->update($row);
        }
    }

    /**
     *
     */
    private function __clone()
    {
    }


}