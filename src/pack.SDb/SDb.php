<?php

/**
 * Class SDb
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class SDb extends SDbBase
{

    /**
     * @param $table
     * @param bool $criteria
     * @param array $holders
     * @param bool $order
     * @param bool $limit
     *
     * @return mixed
     */
    public static function select($table, $criteria = false, $holders = [], $order = false, $limit = false)
    {

        $query = [];
        $query[] = "SELECT * FROM {$table}";

        if ($criteria)
            $query[] = "WHERE {$criteria}";

        if ($order)
            $query[] = "ORDER BY {$order}";

        if ($limit)
            $query[] = "LIMIT {$limit}";

        $stmt = self::execute(implode(" ", $query), $holders);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }


    /**
     * @param $table
     * @param string $fields
     * @param bool $criteria
     * @param array $holders
     * @param bool $order
     * @param bool $limit
     */
    public static function selectFields($table, $fields = "*", $criteria = false, $holders = [], $order = false, $limit = false)
    {
        $orderArr = [];
        if (is_array($order))
            foreach ($order as $key => $value) {
                $orderArr[] = "`{$key}` " . $value;
            }

        $orderStr = implode(", ", $orderArr);
        if (empty($orderStr))
            $orderStr = $order;

        $query = [];
        $query[] = "SELECT {$fields} FROM {$table}";

        if ($criteria)
            $query[] = "WHERE {$criteria}";

        if ($order)
            $query[] = "ORDER BY {$orderStr}";

        if ($limit)
            $query[] = "LIMIT {$limit}";

        $stmt = self::execute(implode(" ", $query), $holders);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @param $table
     * @param array $list
     * @param bool $criteria
     * @param array $holders
     * @param bool $order
     * @param bool $limit
     *
     * @return array
     */
    public static function selectList($table, $list = [], $criteria = false, $holders = [], $order = false, $limit = false)
    {
        if (count($list) != 2)
            exit("Wrong format for 2 argument");

        $raw = self::selectFields($table, implode(",", $list), $criteria, $holders, $order, $limit);
        if (empty($raw))
            return [];

        $result = [];
        foreach ($raw as $r) {
            $result[$r[$list[0]]] = $r[$list[1]];
        }

        return $result;
    }

    /**
     * @param $table
     * @param string $field
     * @param bool $criteria
     * @param array $holders
     * @param bool $order
     * @param bool $limit
     * @return array
     */
    public static function selectToList($table, $field = "", $criteria = false, $holders = [], $order = false, $limit = false)
    {
        if ($field == "")
            exit("Wrong value for 2 argument");

        $raw = self::selectFields($table, $field, $criteria, $holders, $order, $limit);
        if (empty($raw))
            return [];

        $result = [];
        foreach ($raw as $key => $r) {
            $result[$key] = $r[$field];
        }

        return $result;
    }


    /**
     * @param $query
     * @param $arguments
     *
     * @return mixed
     */
    public static function rows($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        $stmt = self::execute($query, $arguments);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }

    /**
     * @param $query
     * @param $arguments
     *
     * @return mixed
     */
    public static function rowsCount($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        $stmt = self::execute($query, $arguments);

        return $stmt->rowCount();
    }

    /**
     * @param $table
     * @param string $where
     * @param array $whereHolders
     *
     * @return int
     */
    public static function count($table, $where = "", $whereHolders = [])
    {
        $tables = (is_array($table)) ? implode(", ", $table) : $table;

        $query = [];
        $query[] = "SELECT COUNT(*) FROM {$tables}";

        if ($where)
            $query[] = "WHERE {$where}";


        $stmt = self::execute(implode(" ", $query), $whereHolders);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ($data) ? (int)current($data) : 0;
    }


    /**
     * @param $query
     * @param array $arguments
     *
     * @return mixed
     */
    public static function cell($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        $stmt = self::execute($query, $arguments);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return ($data) ? current($data) : false;
    }

    /**
     * @param $query
     * @param array $arguments
     */
    public static function row($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        $stmt = self::execute($query, $arguments);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @param $table
     * @param array $values
     * @param string $where
     * @param array $whereHolders
     *
     * @return bool
     */
    public static function update($table, $values = [], $where = "", $whereHolders = [])
    {
        $query = [];
        $query[] = "UPDATE {$table}";

        if (empty($values))
            return false;

        $holders = [];
        $holdersStr = "SET ";
        foreach ($values as $key => $val) {
            $holdersStr .= self::SQL_QUOTE . $key . self::SQL_QUOTE . "=?,";
            $holders[] = $val;
        }

        $query[] = trim($holdersStr, ",");

        if ($where)
            $query[] = "WHERE {$where}";

        $holders = array_merge($holders, $whereHolders);
        self::execute(implode(" ", $query), $holders);

        return (empty(self::$errors)) ? true : false;
    }

    /**
     * @param $table
     * @param array $values
     * @param string $where
     * @param array $whereHolders
     *
     * @return bool
     */
    public static function upsert($table, $values = [], $where = "", $whereHolders = [])
    {
        if (self::count($table, $where, $whereHolders) == 0) {
            return self::insert($table, $values);
        } else {
            return self::update($table, $values, $where = "", $whereHolders);
        }
    }

    /**
     * Getting an array of enum values in the array
     * @param string $table
     * @param string $column
     * @return array|bool|mixed
     */
    public static function getEnum($table = '', $column = '')
    {
        $enum_array = SDb::row("SHOW COLUMNS FROM {$table} LIKE '{$column}'");
        if (!empty($enum_array)) {
            $enum_array = str_replace('enum(', '', $enum_array['Type']);
            $enum_array = str_replace(')', '', $enum_array);
            $enum_array = str_replace("'", '', $enum_array);
            $enum_array = explode(',', $enum_array);

            return $enum_array;
        } else {
            return false;
        }
    }

    /**
     * @param $table
     * @param array $values
     *
     * @return bool
     */
    public static function insert($table, $values = [])
    {
        $query = [];
        $query[] = "INSERT INTO {$table}";

        if (empty($values))
            return false;
        //$fieldsStr ='';
        $holders = [];
        $fieldsArr = [];
        $holdersStr = "VALUES (";
        foreach ($values as $key => $val) {
            $holdersStr .= "?,";
            //$fieldsStr .= $key . ",";
            $fieldsArr[] = $key;
            $holders[] = (is_null($val)) ? NULL : $val;
        }
        $fieldsStr = implode("`,`", $fieldsArr);

        $query[] = "(`{$fieldsStr}`)";
        $query[] = "" . trim($holdersStr, ",") . ")";

        self::execute(implode(" ", $query), $holders);
        return (empty(self::$errors)) ? true : false;
    }

    /**
     * @param $table
     * @param string $where
     * @param array $whereHolders
     *
     * @return bool
     */
    public static function delete($table, $where = "", $whereHolders = [])
    {
        $query = [];
        $query[] = "DELETE FROM {$table}";

        if ($where)
            $query[] = "WHERE {$where}";

        self::execute(implode(" ", $query), $whereHolders);
        return (empty(self::$errors)) ? true : false;
    }

    /**
     * @param $table
     * @param $primary
     * @return bool|mixed
     */
    public static function emptyRow($table, $primary)
    {
        if (SDb::insert($table, [$primary => NULL])) {
            return SDbBase::getLastInsertedId();
        } else {
            return false;
        }
    }


    /**
     *
     */
    public static function transactionStart()
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }

        try {
            self::$connections[self::$current]->beginTransaction();
        } catch (Exception $e) {
            self::$errors[] = $e->getMessage();
        }
    }

    /**
     *
     */
    public static function transactionRollBack()
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }

        try {
            self::$connections[self::$current]->rollBack();
        } catch (Exception $e) {
            self::$errors[] = $e->getMessage();
        }
    }

    /**
     *
     */
    public static function transactionCommit()
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }

        try {
            self::$connections[self::$current]->commit();
        } catch (Exception $e) {
            self::$errors[] = $e->getMessage();
        }
    }

    /**
     * @param $table
     *
     * @return array|bool
     */
    public static function listFields($table)
    {

        $fields = SDb::rows("SHOW COLUMNS FROM {$table}");
        if ($fields) {
            $arr = array();
            foreach ($fields as $f)
                $arr[] = $f["Field"];

            return $arr;
        } else return false;
    }

    /**
     * @param $db
     *
     * @return array|bool
     */
    public static function tables($db)
    {
        $tables = SDb::rows("SHOW TABLES FROM {$db}");
        if ($tables) {
            $arr = array();
            foreach ($tables as $f) {
                $arr[] = $f["Tables_in_" . $db];
            }
            return $arr;
        } else return false;
    }

    /**
     *
     * @param string $table
     * @param array $params
     * @param boolean $ignore
     * @param boolean $delayed
     * @param string $onduplicateUpdate строка запроса для блока on duplicate key update( без самой этой фразы)
     * @param array $onduplicateHolders массив значений холдеров для блока update
     * @param boolean $lastInsertId возвращать last inserted id или нет
     * @return boolean|int - если указан флаг вернуть last insert id
     */
    public static function insertExt($table, array $params, $ignore = false, $delayed = false, $onduplicateUpdate = '', array $onduplicateHolders = array(), $lastInsertId = false)
    {
        $holders = array();
        for ($iter = 0; $iter < count($params); $iter++) {
            $holders[] = '?';
        }
        $holderVals = array_values($params);
        if (!empty($onduplicateUpdate) && !empty($onduplicateHolders)) {
            foreach ($onduplicateHolders as $onvl) {
                $holderVals[] = $onvl;
            }
        }

        $sql = "INSERT " . ($ignore ? "IGNORE" : "") . " " . ($delayed ? "DELAYED" : "") . " INTO " . $table . " (`" . implode('`,`', array_keys($params)) . "`) VALUES (" . implode(',', $holders) . ")"
            . (!empty($onduplicateUpdate) ? " ON DUPLICATE KEY UPDATE " . $onduplicateUpdate : "");
        try {
            $stmt = self::execute($sql, $holderVals);
            if ($stmt) {
                if ($lastInsertId) {
                    return self::getLastInsertedId();
                }

                return true;
            }
        } catch (PDOException $ex) {
            Debug::logMsg($sql . "\n" . print_r($holderVals, 1) . "\n" . $ex->getMessage(), "sdb" . DS . "errors.log");
            return false;
        } catch (Exception $ex) {
            self::$errors[] = $ex->getMessage();
            return false;
        }

    }

}