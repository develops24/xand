<?php 
/**
 * Class ButtonBan
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonBlacklist extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Blacklist";				
		$this->action 		= "Blacklist";
		
		$this
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/blacklist.png);")
			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Are you sure you want to put in the blacklist it?")
			 ->setProperty("script","DataAct");
	}
}
?>