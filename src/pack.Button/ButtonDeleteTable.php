<?php

/**
 * Class ButtonDeleteTable
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonDeleteTable extends ButtonDelete
{
    /**
     * 
     */
    public function setDefaults()
    {
        parent::setDefaults();
        $this->setProperty("script", "TableAct");
    }
}
