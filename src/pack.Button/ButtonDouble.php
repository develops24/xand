<?php 
/**
 * Class ButtonDouble
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonDouble extends ClassBase
{		
	public $objects = array();
	public $data;
	public $action  = "rand";
	
	//---
	function __construct()
	{		
		$this->objects = func_get_args();		

		$this->setProperty("dialogEnable", false)
			 ->setProperty("disabledDialog", "Access denied")
			 ->setProperty("enabled", true)
			 ->setProperty("drawed",  false);
			 
	}
	//---
	public function SetData($data)
	{
		$this->data = $data;
		$this->objects[0]->SetData($data)->assignParent($this->parent);
		$this->objects[1]->SetData($data)->assignParent($this->parent);
		
		return $this;
	}
	//---
	public function SetFieldData($data)
	{		
		$this->objects[2]->data = $data;	  
		return $this;
	}
	//---
	public function GetFieldNameDb()
	{					
		return $this->objects[2]->field;
	}

}
?>