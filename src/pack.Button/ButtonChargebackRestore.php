<?php

/**
 * Description of class
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class ButtonChargebackRestore extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Chargeback Revoke";
        $this->action = "ChargebackRestore";

        $this->setProperty("svg", "buttonChargeback-restore")
             ->setProperty("dialogEnable", true)
             ->setProperty("dialog", "Are you sure you want to revoke Chargeback?")
             ->setProperty("script", "DataAct");
    }

}

?>
