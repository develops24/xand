<?php
/**
 * Class ButtonUnban
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonUnban extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Разабанить";				
		$this->action 		= "Unban";


		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/unban.png);")
			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Вы точно хотите разбанить пользователя?")
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");		
	}
}
?>