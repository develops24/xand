<?php

/**
 * Description of class
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class ButtonRevocation extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Revocation";
        $this->action = "Revocation";

        $this->setProperty("cssClass", "buttons_ico")
                ->setProperty("styles", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/revocation.png);")
                ->setProperty("dialogEnable", true)
                ->setProperty("dialog", "Are you sure you want to make Revocation?")
                ->setProperty("script", "DataAct")
                ->setProperty("drawType", "DrawIco");
    }

}

?>
