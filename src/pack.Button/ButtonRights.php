<?php

//--------
class ButtonRights extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Rights";
        $this->action = "rights";

        $this->setProperty("cssClass", "buttons_ico")
                ->setProperty("styles", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/2users.png);")
//                ->setProperty("stylesBig", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/big-reset.png);")
                ->setProperty("dialog", "Are you sure you want to set this changes?")
                ->setProperty("dialogEnable", false)
                ->setProperty("script", "ModalAct")
                ->setProperty("drawType", "DrawIco");
    }

}

?>