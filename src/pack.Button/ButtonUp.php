<?php 
/**
 * Class ButtonUp
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonUp extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Up";				
		$this->action 		= "Up";	
		
		$this->setProperty("cssClass","buttons_ico_text")
             ->setProperty("fontClass", "fa-arrow-up")
             ->setProperty("fontColor", "#8ed74d")
			 ->setProperty("drawType","DrawIcoText")
			 ->setProperty("script","Path");
	}	
}
?>