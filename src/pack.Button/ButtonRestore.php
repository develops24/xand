<?php
/**
 * Class ButtonRestore
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonRestore extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Востановить";				
		$this->action 		= "Restore";
		
		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/restore.png);")
			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Вы точно хотите востановить запись?")
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");		
	}
}
?>