<?php
/**
 * Class ButtonSave
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/ 
class ButtonSaveAndContinue extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 	= "Save and continue edit";
		$this->action 	= "UpdateFinishContinue";
		
		$this->setProperty("script","JsonPushAct")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/ok.png);")
			 ->setProperty("drawType","DrawForms")
			 ->setProperty("cssClass","btn");
	}	
}