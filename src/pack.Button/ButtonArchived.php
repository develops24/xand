<?php
/**
 * Class ButtonArchived
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 *
 *
 *
 **/
class ButtonArchived extends ButtonBase
{
    public function SetDefaults()
    {
        $this->title 		= "Move into archive";
        $this->action 		= "Archived";

        $this->setProperty("svg","buttonArchive")
            ->setProperty("dialogEnable", false)
            ->setProperty("script","DataAct");
    }
}
?>