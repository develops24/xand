<?php

/**
 * Class ButtonEditModal
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonEditModal extends ButtonEdit
{
    /**
     *
     */
    public function setDefaults()
    {
        parent::setDefaults();
        $this->setProperty("script", "ModalAct");
    }
}