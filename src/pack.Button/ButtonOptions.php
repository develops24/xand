<?php
/**
 * Class ButtonView
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonOptions extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Options";
		$this->action 		= "OptionsReady";

		$this->setProperty("svg","buttonOptions")
			 ->setProperty("script","DataAct");
	}
}
