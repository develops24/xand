<?php

class ButtonDownload extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Скачать";				
		$this->action 		= "ViewReady";

		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/download.png);")	
			 ->setProperty("script","AJAX")
			 ->setProperty("drawType","DrawIco");				
	}
}

?>