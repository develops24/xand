<?php 
/**
 * Class ButtonDown
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonDown extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Down";				
		$this->action 		= "Down";	
		
		$this->setProperty("svg","buttonDown")
			 ->setProperty("script","DataAct");		
	}	
}
?>