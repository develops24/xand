<?php
/**
 * Class ButtonView
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonNumber extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Enter serial number";				
		$this->action 		= "EnterReady";

		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/number.png);")	
			 ->setProperty("script","AJAX")
			 ->setProperty("drawType","DrawIco");			
	}
}
?>