<?php

/**
 * Description of class
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class ButtonChargeback extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Chargeback";
        $this->action = "Chargeback";

        $this->setProperty("svg", "buttonChargeback")
             ->setProperty("dialogEnable", true)
             ->setProperty("dialog", "Are you sure you want to make Chargeback?")
             ->setProperty("script", "DataAct");
    }

}

?>
