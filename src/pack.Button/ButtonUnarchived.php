<?php
/**
 * Class ButtonUnarchived
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonUnarchived extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Move from archive";				
		$this->action 		= "Unarchived";
		
		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/tpl/svg/unarchive.svg);")
			 ->setProperty("stylesBig","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/tpl/svg/unarchive.svg);")
			 ->setProperty("dialogEnable", false)
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");	
	}
}
?>