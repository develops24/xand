<?php

/**
 * Class ButtonAppend
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonAppendModal extends ButtonAppend
{
    /**
     *
     */
    public function setDefaults()
    {
        parent::setDefaults();
        $this->setProperty("script", "ModalAct");
    }
}