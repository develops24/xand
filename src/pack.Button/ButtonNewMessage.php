<?php 
/**
 * Class ButtonNewMessage
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonNewMessage extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Новое сообщение";				
		$this->action 		= "NewMessage";
		
		$this->setProperty("cssClass", "buttons_ico_text")
			 ->setProperty("styles", "background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/mailsnd.png);")
			 ->setProperty("dialog", "Вы действительно хотите синхронизировать ?")
			 ->setProperty("script", "DataAct")
			 ->setProperty("drawType","DrawIcoText");	
				
	}
}
?>