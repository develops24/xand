<?php
/**
 * Class ButtonSave
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/ 
class ButtonSave extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 	= "Save";				
		$this->action 	= "UpdateFinish";
		
		$this->setProperty("script","JsonAct")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/ok.png);")
			 ->setProperty("drawType","DrawForms")
			 ->setProperty("cssClass","btn-primary");
	}	
}
?>