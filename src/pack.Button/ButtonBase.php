<?php

/**
 * Class ButtonBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonBase extends ClassBase implements IObjectsArray
{
    public $name = "buttons";
    public $action = "UndefinedAction";
    public $title = "";
    public $data = 0;
    public $domId = "";


    /**
     *  Scripts
     */
    const SCRIPT_MODAL = "ModalAct";
    const SCRIPT_TABLE = "TableAct";
    const SCRIPT_TABLE_MODAL = "TableModalAct";
    const SCRIPT_DATA = "DataAct";


    /**
     * ButtonBase constructor.
     */
    public function __construct() {
        $arr = func_get_args();

        $this->domId = $this->getUniqueName();

        $this->setProperty("dialogEnable", false)
            ->setProperty("disabledDialog", "Disallow")
            ->setProperty("enabled", true)
            ->setProperty("drawed", false);

        if (method_exists($this, 'setDefaults')) $this->setDefaults();

        if (isset($arr[0])) $this->action = $arr[0];
        if (isset($arr[1])) $this->title = $arr[1];
        if (isset($arr[2])) {
            foreach ($arr[2] as $key => $val)
                $this->setProperty($key, $val);
        }
        if (isset($arr[3])) $this->data = $arr[3];
    }

    /**
     * @param $data
     *
     * @return $this
     */
    public function SetData($data) {
        $this->data = $data;
        return $this;
    }

    /**
     *
     */
    public function Draw() {
        echo "";
    }

}