<?php

/**
 * Class ButtonView
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonView extends ButtonBase
{
    /**
     *
     */
    public function SetDefaults()
    {
        $this->title = "View";
        $this->action = "ViewReady";

        $this->setProperty("svg", "buttonMagnifier")
            ->setProperty("script", "ModalAct");
    }
}