<?php
/**
 * Class ButtonView
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonList extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "View";				
		$this->action 		= "ViewList";

		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/list2.png);")	
			 ->setProperty("script","AJAX")
			 ->setProperty("drawType","DrawIco");			
	}
}
?>