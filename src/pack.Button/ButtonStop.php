<?php
/**
 * Class ButtonUnpublish
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonStop extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Stop";				
		$this->action 		= "Stop"; 

		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("fontClass", "fa-minus-circle")
			 ->setProperty("fontColor", "#ffb300")  
		
			 
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/tpl/svg/play.svg);")
			  
			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Are you sure you want to stop the process?")
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");		
	}
}
?>