<?php 
/**
 * Class ButtonEdit
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/

class ButtonInsert extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Insert to";				
		$this->action 		= "InsertReady";
		
		$this->setProperty("svg","buttonsInsert")
			 ->setProperty("script","DataAct");
	}
}
?>