<?php

//--------
class ButtonReset extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Reset";
        $this->action = "Reset";

        $this->setProperty("svg", "buttonReset")
             ->setProperty("dialog", "Are you sure you want to reset?")
             ->setProperty("dialogEnable", true)
             ->setProperty("script", "DataAct");
    }

}

?>