<?php
/**
 * Class ButtonView
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonHistory extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "History";				
		$this->action 		= "HistoryReady";

		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("svg","buttonHistory")
			 ->setProperty("script","AJAX")
			 ->setProperty("drawType","DrawIco");			
	}
}
?>