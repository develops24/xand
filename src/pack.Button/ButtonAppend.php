<?php

/**
 * Class ButtonAppend
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonAppend extends ButtonBase
{
    /**
     * 
     */
    public function setDefaults()
    {
        $this->title = "Add";
        $this->action = "AddReady";

        $this->setProperty("fontClass", "fa-plus")
            ->setProperty("fontColor", "#8ed74d")
            ->setProperty("dialog", "Are you sure?")
            ->setProperty("script", "DataAct")
            ->setProperty("drawType", "DrawIcoText");
    }
}