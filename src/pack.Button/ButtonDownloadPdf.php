<?php

class ButtonDownloadPdf extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Download PDF";				
		$this->action 		= "DownloadPdfReady";

		$this->setProperty("svg","buttonPdf")
			 ->setProperty("script","Ajax");
	}
}

?>