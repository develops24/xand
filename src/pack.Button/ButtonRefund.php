<?php

/**
 * Description of class
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class ButtonRefund extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Payment done";
        $this->action = "Refund";

        $this->setProperty("cssClass", "buttons_ico")
                ->setProperty("styles", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/refund2.png);")
                ->setProperty("dialogEnable", true)
                ->setProperty("dialog", "Are you sure you want to complete refund operation?")
                ->setProperty("script", "DataAct")
                ->setProperty("drawType", "DrawIco");
    }

}

?>
