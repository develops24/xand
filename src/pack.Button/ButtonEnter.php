<?php
/**
 * Class ButtonEnter
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/

class ButtonEnter extends ButtonBase
{		
	public function SetDefaults()
	{
			
		$this->title 		= "Войти";				
		$this->action 		= "Enter";		
	
		$this->setProperty("cssClass","btn-primary")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/action_delete.png);")
			 ->setProperty("dialog","Вы точно хотите войти?")
			 ->setProperty("script","JsonAct")
			 ->setProperty("drawType","DrawForms");	
	}
}
?>