<?php
/**
 * Class ButtonView
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonPass extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Password managment";				
		$this->action 		= "PassReady";

		$this->setProperty("svg","buttonPassword")
			 ->setProperty("script","DataAct");
	}
}
?>