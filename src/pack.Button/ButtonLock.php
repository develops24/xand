<?php
/**
 * Class ButtonLock
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonLock extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Открыть";				
		$this->action 		= "Lock";

		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/lock.png);")
			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Вы точно хотите закрыть обсуждения?")
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");			
	}
}
?>