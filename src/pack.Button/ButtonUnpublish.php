<?php

/**
 * Class ButtonUnpublish
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonUnpublish extends ButtonBase
{
    public function SetDefaults()
    {
        $this->title = "Disable";
        $this->action = "Unpublished";

        $this->setProperty("fontClass", "fa-power-off")
            ->setProperty("fontColor", "rgba(202, 213, 221, 0.506)")
            ->setProperty("svg", "buttonPublished")
            ->setProperty("dialogEnable", false)
            ->setProperty("dialog", "You are sure?")
            ->setProperty("script", "DataAct");
    }
}
