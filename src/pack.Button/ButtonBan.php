<?php
/**
 * Class ButtonBan
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 *
 *
 *
 **/
class ButtonBan extends ButtonBase
{
    public function SetDefaults()
    {
        $this->title 		= "Ban";
        $this->action 		= "Ban";

        $this->setProperty("svg","buttonUser-ban")
            ->setProperty("dialogEnable", true)
            ->setProperty("dialog","Are you sure you want to ban a user?")
            ->setProperty("script","DataAct");
    }
}
?>