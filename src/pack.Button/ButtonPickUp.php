<?php 
/**
 * Class PickUp
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonPickUp extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Up";				
		$this->action 		= "Pickup";	
		
		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/up3.png);")
			 ->setProperty("stylesBig","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/up2.png);")
			 ->setProperty("drawType","DrawIco")
			 ->setProperty("script","DataAct");		
	}	
}
?>