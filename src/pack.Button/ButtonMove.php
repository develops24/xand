<?php
/**
 * Class ButtonMove
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonMove extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Move";
		$this->action 		= "MoveReady";
		
		$this->setProperty("fontClass", "fa-random")
             ->setProperty("fontColor", "#ff3f2a")
             ->setProperty("svg","buttonMove")
			 ->setProperty("script","ModalAct");
	}
}
?>