<?php

/**
 * Class ButtonPublish
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonPublish extends ButtonBase
{
    public function SetDefaults()
    {
        $this->title = "Enable";
        $this->action = "Published";

        $this->setProperty("fontClass", "fa-circle-o")
            ->setProperty("fontColor", "#8ed74d")
            ->setProperty("svg", "buttonUnpublished")
            ->setProperty("dialogEnable", false)
            ->setProperty("script", "DataAct")
            ->setProperty("dialog", "You are sure?");
    }
}
