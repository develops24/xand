<?php
/**
 * Class ButtonPublish
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonClone extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Clone";				
		$this->action 		= "Clone";
		
		$this->setProperty("dialogEnable", false)
			 ->setProperty("dialog","Are you sure you want to clone a record?")
			 ->setProperty("svg", "buttonClone")
			 ->setProperty("script","DataAct");
	}
}
?>