<?php

/**
 * Class ButtonForce
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonForce extends ButtonBase
{

    public function SetDefaults()
    {
        $this->title = "ForcePayment";
        $this->action = "ForceReady";

        $this->setProperty("svg", "buttonForce")
            ->setProperty("dialogEnable", false)
            ->setProperty("dialog", "Are you sure you want to make Force Payment?")
            ->setProperty("script", "ModalAct");
    }

}
 