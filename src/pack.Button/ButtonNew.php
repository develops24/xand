<?php

/**
 * Class ButtonAdd
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 * */
class ButtonNew extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Add";
        $this->action = "AddReady";

        $this->setProperty("dialog", "Вы действительно хотите добавить!")
                ->setProperty("styles", "")
                ->setProperty("script", "DataAct")
                ->setProperty("cssClass", "bBlue")
                ->setProperty("drawType", "DrawForms");
    }

}

?>