<?php
/**
 * Class ButtonPublish
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonStart extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Start";				
		$this->action 		= "Start";
		
		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("fontClass", "fa-arrow-right")
			 ->setProperty("fontColor", "#4CDBFF") 
		
		
			 
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/tpl/svg/stop.svg);")
			 ->setProperty("stylesBig","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/tpl/svg/play.svg);")
			 
			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Are you sure you want to start the process?")
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");	
	}
}
?>