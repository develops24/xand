<?php 
/**
 * Class ButtonAdd
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonPrint extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Print label";				
		$this->action 		= "ButtonPrint";
		
		$this->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/print.png);")
			 ->setProperty("dialog","Do you really want to add!")
			 ->setProperty("script","JsonAct")
			 ->setProperty("cssClass","buttons_ico")
			 ->setProperty("drawType","DrawIco");	
	}
}
?>