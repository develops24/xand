<?php

/**
 * Description of class
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class ButtonReceivedChecked extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Received & checked";
        $this->action = "ReceivedChecked";

        $this->setProperty("cssClass", "buttons_ico")
                ->setProperty("styles", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/ok2.png);")
                ->setProperty("dialogEnable", true)
                ->setProperty("dialog", "Confirm you have received and checked item")
                ->setProperty("script", "DataAct")
                ->setProperty("drawType", "DrawIco");
    }

}

?>
