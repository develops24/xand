<?php

//--------
class ButtonSynch extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Synch";				
		$this->action 		= "Synch";
		
		$this->setProperty("cssClass", "buttons_ico_text")
			 ->setProperty("styles", "background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/tpl/svg/sync.svg);")
			 ->setProperty("dialog", "You are sure?")
			 ->setProperty("script", "DataAct")
			 ->setProperty("drawType","DrawIcoText");	
			
	}
}

?>