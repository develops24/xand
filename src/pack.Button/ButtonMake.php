<?php

//--------
class ButtonMake extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Generate";
        $this->action = "Make";

        $this->setProperty("svg", "buttonPlus")
             ->setProperty("dialog", "Are you sure you want to generate new one?")
             ->setProperty("dialogEnable", true)
             ->setProperty("script", "DataAct");
    }

}

?>