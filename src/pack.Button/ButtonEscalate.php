<?php 
/**
 * Class ButtonEscalate
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonEscalate extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Escalate";				
		$this->action 		= "Escalate";	
		
		$this->setProperty("svg","buttonDanger-off")
			 ->setProperty("script","DataAct");		
	}	
}
?>