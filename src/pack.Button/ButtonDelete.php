<?php

/**
 * Class ButtonDelete
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonDelete extends ButtonBase
{
    /**
     *
     */
    public function setDefaults()
    {
        $this->title = "Delete";
        $this->action = "Delete";

        $this->setProperty("fontClass", " fa-trash-o")
            ->setProperty("fontColor", "#ff3f2a")
            ->setProperty("svg", "buttonDelete")
            ->setProperty("dialogEnable", true)
            ->setProperty("dialog", "Are you sure you want to delete a record?")
            ->setProperty("script", "DataAct")
            ->setProperty("drawType", "DrawIco");
    }
}