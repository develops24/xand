<?php
/**
 * Class ButtonView
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonSettings extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Settings managment";				
		$this->action 		= "SettingsReady";

		$this->setProperty("svg","buttonSettings")
			 ->setProperty("script","DataAct");
	}
}
