<?php 
/**
 * Class ButtonEdit
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/

class ButtonEdit extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Edit";				
		$this->action 		= "UpdateReady";
		
		$this->setProperty("svg","buttonEdit")
			 ->setProperty("script","DataAct");
	}
}
?>