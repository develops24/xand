<?php

//--------
class ButtonExport extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Export";				
		$this->action 		= "Export";
		
		$this->setProperty("cssClass", "buttons_ico_text")
			 
			 ->setProperty("fontClass", "fa-arrow-up")
			 ->setProperty("fontColor", "#4CDBFF") 
			 
			 ->setProperty("styles", "background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/table_export.png);")
			 ->setProperty("stylesBig", "background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/export2.png);")
			 ->setProperty("dialogEnable", false)
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIcoText");	
			
	}
}
