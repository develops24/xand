<?php

/**
 * Class RuleRange
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleRange extends RuleBase
{
    /**
     * @param $val
     *
     * @return string
     */
    public function GetRule($val)
    {
        //        echo $val;
        if (strpos($val, "to") > 0) {
            $val = str_replace('to', '-', $val);
        }
        $point = ($this->table == "") ? "" : ".";

        $valExplode = explode("-", $val);
        if (!is_numeric($valExplode[0])) {
            if (strpos($valExplode[0], '_') > 0) {
                $valExplode[0] = str_replace('_', '-', $valExplode[0]);
                $valExplode[1] = str_replace('_', '-', $valExplode[1]);
            }
            $valExplode[0] = strtotime($valExplode[0]);
            $valExplode[1] = strtotime($valExplode[1]);
        } else {
            $valExplode[0] = Date::ToUnix($valExplode[0]);
            $valExplode[1] = Date::ToUnix($valExplode[1]);
        }

        $valExplode[0] = (isset($valExplode[0])) ? $valExplode[0] : "";
        $valExplode[1] = (isset($valExplode[1])) ? $valExplode[1] : "";

        $valExplode[0] = ($valExplode[0] == "") ? "" : $this->table . $point . $this->field . " > " . $valExplode[0];
        $valExplode[1] = ($valExplode[1] == "") ? "" : $this->table . $point . $this->field . " < " . $valExplode[1];

        $valExplode[0] = ($valExplode[0] != "" && $valExplode[1] != "") ? $valExplode[0] . " AND " : $valExplode[0];

        return $valExplode[0] . $valExplode[1];
    }

}