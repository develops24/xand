<?php

/**
 * Class RuleMonthUnix
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleMonthUnix extends RuleBase
{
    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        $val = str_replace("_", "", $val);
        $criteria = ($this->table == "") ? $this->field : $this->table . "." . $this->field;
        $criteria = "DATE_FORMAT( FROM_UNIXTIME({$criteria}), '%Y%m' ) = ?";

        if ($this->provideHolders) {
            return array($criteria, array($val));
        }
        return $criteria. "=" . $val;
    }
}