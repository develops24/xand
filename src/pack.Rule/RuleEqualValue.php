<?php

/**
 * Class RuleEqualValue
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleEqualValue
{

    public $field;
    public $table;
    public $val;

    /**
     * RuleEqualValue constructor.
     *
     * @param string $field
     * @param string $table
     * @param string $value
     */
    function __construct($field = "", $table = "", $value = "")
    {
        $this->field = $field;
        $this->table = $table;
        $this->val = $value;
    }

    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        $point = ($this->table == "") ? "" : ".";
        if ($this->provideHolders) {
            return array($this->table . $point . $this->field . "=?", array($this->val));
        }
        return $this->table . $point . $this->field . " = " . $this->val;
    }

}