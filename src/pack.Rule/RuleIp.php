<?php

/**
 * Class RuleIp
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleIp extends RuleBase
{
    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        $val = ip2long($val);
        $point = ($this->table == "") ? "" : ".";
        if ($this->provideHolders) {
            return array($this->table . $point . $this->field . "=?", array($val));
        }
        return $this->table . $point . $this->field . " = " . $val;
    }
}