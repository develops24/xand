<?php

/**
 * Class RuleAbc
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleAbc extends RuleBase
{
    public function GetRule($val)
    {
        $point = ($this->table == "") ? "" : ".";
        if ($this->provideHolders) {
            $val = "{$val}%";
            return array($this->table . $point . $this->field . " LIKE ?", array($val));
        }
        return $this->table . $point . $this->field . " LIKE '" . $val . "%'";
    }
}