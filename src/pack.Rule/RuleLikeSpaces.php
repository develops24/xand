<?php

/**
 * Like-поиск с строке с удаленными пробелами
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class RuleLikeSpaces extends RuleBase
{
    public function GetRule($val) {
        $point = ($this->table == "") ? "" : ".";
        if ($this->provideHolders) {
            $val = "%{$val}%";
            return array("REPLACE(" . $this->table . $point . $this->field . ",' ','') LIKE ?", array($val));
        }
        return "REPLACE(" . $this->table . $point . $this->field . ",' ','') LIKE '%" . $val . "%'";
    }
}