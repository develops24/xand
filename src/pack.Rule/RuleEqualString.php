<?php

/**
 * Class RuleEqual
 * @Vadim funsport95@gmail.com
 */
class RuleEqualString extends RuleBase
{
    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        if (substr($val, 0, 1) == "!") {
            $operator = " <> ";
            $val = substr($val, 1, strlen($val));
        } else {
            $operator = " = ";
        }
        $point = ($this->table == "") ? "" : ".";
        if ($this->provideHolders) {
            return array($this->table . $point . $this->field . $operator . " ?", array($val));
        }

        return $this->table . $point . $this->field . $operator . "'" . $val . "'";
    }
}