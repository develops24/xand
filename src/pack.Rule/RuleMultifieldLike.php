<?php

/**
 * Description of class
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class RuleMultifieldLike extends RuleBase
{

    public function GetRule($val) {
//        $point = ($this->table == "") ? "" : ".";
//        return $this->table . $point . $this->field . " = " . $val;
        return $this->parseVal($val);
    }

    /**
     * Формирует строку запроса
     * @param string $val
     * @return string
     */
    private function parseVal($val) {
        if (substr_count($this->field, ',') > 0) {
            $fields = explode(',', $this->field);
        } else {
            $fields[] = $this->field;
        }
        $concat = '';
        $point = ($this->table == "") ? "" : ".";
        if (count($fields) > 1) {
            $concat = implode(",' '," . $this->table . $point, $fields);
            $concat = 'CONCAT(' . $concat . ')';
        } else {
            $concat = $this->table . $point . $this->field;
        }
        if ($this->provideHolders) {
            $val = "%" . $val . "%";
            return array($concat . " LIKE ?", array($val));
        }

        $criteria = $concat . " LIKE('%" . $val . "%')";
        return $criteria;
    }

}
