<?php

/**
 * Class RuleBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleBase
{
    public $field;
    public $table;
    public $callback;
    public $provideHolders = false;

    /**
     * @param string $field
     * @param string $table
     * @param bool|callable $callback
     */
    public function __construct($field = "", $table = "", $callback = false)
    {
        $this->field = $field;
        $this->table = $table;
        $this->callback = $callback;
    }
}