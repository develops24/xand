<?php

/**
 * Class RuleLikeChild
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleLikeChild extends RuleBase
{
    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        $point = ($this->table == "") ? "" : ".";
        if ($this->provideHolders) {
            $val = $val . "%";
            return array($this->table . $point . $this->field . " LIKE ?", array($val));
        }
        return $this->table . $point . $this->field . " LIKE '" . $val . "%'";
    }
}