<?php

class ValidatePassword implements IValidate
{
    public $name = "Password";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = false;

        if(mb_strlen($value, "UTF8") < 4 || mb_strlen($value, "UTF8") > 16)        {
            $errors = I18n::__("%Please fill field%");
        }

        if (! preg_match("#^[aA-zZ0-9]+$#", $value)) {
            $errors = I18n::__("%Please fill correct field%");
        }

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}