<?php

class ValidateExists implements IValidate
{
    public $name = "Exists";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = ($value == "") ? (($object->required === true) ? I18n::__("%Please fill field%") :  $object->required)
                                 : false;
        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}