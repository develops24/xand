<?php

class ValidateDateGroup implements IValidate
{

    public $name = "DateGroup";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {

        $errors = false;

        if($value < 2015 || $value > 2020)
            $errors = true;

        if($slaves[$object->slaveKey] < 0 || $slaves[$object->slaveKey] > 12)
            $errors = true;

        if($errors)
            $errors = ($object->required === true) ? I18n::__("%Please fill field%") :  $object->required;

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}