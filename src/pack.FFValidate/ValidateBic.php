<?php

/**
 * Class ValidateBic
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ValidateBic implements IValidate
{
    public $name = "Bic";

    /**
     * @param bool $value
     * @param FFObjectBase $object
     * @param array $slaves
     *
     * @return array
     */
    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = false;

        if (! $this->isBICValid($value))
            $errors = I18n::__("BIC is not valid");

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

    /**
     * @param $bic
     *
     * @return bool
     */
    public function isBICValid($bic) {
        if (!is_string($bic)) {
            return false;
        }
        if (!preg_match('/^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$/i', $bic)) {
            return false;
        }
        return true;
    }

}