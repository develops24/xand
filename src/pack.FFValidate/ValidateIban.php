<?php

/**
 * Class ValidateIban
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ValidateIban implements IValidate
{
    public $name = "Iban";

    /**
     * @param bool $value
     * @param FFObjectBase $object
     * @param array $slaves
     *
     * @return array
     */
    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = false;
        $iban = new IbanComponent();
 
        if (!$iban->isValid($value)) {
            $errors = I18n::__("IBAN is not valid");
        }

        return array(
            "errors" => $errors,
            "corrected" => $value
        );

    }

}