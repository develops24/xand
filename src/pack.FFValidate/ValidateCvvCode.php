<?php

class ValidateCvvCode implements IValidate
{
    public $name = "CvvCode";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = false;

        if(mb_strlen($value, "UTF-8") < 3 || mb_strlen($value, "UTF-8") > 4 )
            $errors = ($object->required === true) ? I18n::__("%Please fill field%") :  $object->required;

        //Debug::Dump($errors);
        //echo "<hr/>";

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}