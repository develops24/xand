<?php

class ValidateEmail implements IValidate
{
    public $name = "Email";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = false;

        if (!filter_var($value, FILTER_VALIDATE_EMAIL))
            $errors = ($object->required === true) ? I18n::__("%Please fill field%") :  $object->required;

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}