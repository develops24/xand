<?php

/**
 * Class Table
 *
 * @тип  	 Class
 * @package    Widget
 * @версия   2
 *
 *
 * v2 - Добавлено PostProcessing
 * v3 - Добавлено primary
 *
 * */
class TableBuffer extends TableBase {


    //--
    public function Draw() 
	{
		

        if (empty($this->data))
            $this->data = $this->parent->data;

        if (empty($this->primary))
            Message::Fast("EI|Укажите первичный ключ! (5 параметр конструктора обьекта TableColumn)");


        if (!empty($this->buttons) && $this->buttons[0]->parent == NULL) {
            foreach ($this->buttons as $b)
                $b->assignParent($this->parent);
        }

      
        $tableData = array();
        $counter = -1;

        if (!empty($this->data))
            foreach ($this->data as $key => $row) 
			{
                $counter++;
                $tableDataRow = array();
 
                foreach ($this->columns as $key2 => $item) 
				{
					
					if(isset( $this->parent->objects['state']->param["_ATC"]) && 
					 ! isset($this->parent->objects['state']->param["_ATC".$item->key]))  continue;
					
                    if (!isset($this->data[$key][$item->key]))
                        $this->data[$key][$item->key] = "-//-";
                    if (method_exists($item, "PostProcessing"))
                        $this->data[$key][$item->key] = $item->PostProcessing($this->data[$key][$item->key]); 
					 
				    $tableDataRow[$item->key] = $this->data[$key][$item->key]; 
                }



				$tableData[] = $this->GetActions($row, $key);
                
            }
       		
			$this->properties["tpl"]->assign("table", $tableData)->Load();
       		 
    }
  

    //--
}

?>