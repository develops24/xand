<?php

/**
 * Class Table
 *
 * @тип     Class
 * @package    Widget
 * @версия   2
 *
 *
 * v2 - Добавлено PostProcessing
 * v3 - Добавлено primary
 *
 * */
class Table extends TableBase
{

    public function DrawGroupSeparator($text)
    {
        $cs = $this->colspan + 1;
        echo "<tr><td colspan='{$cs}' style='padding:0;   background-color: #fff;'>
				  <div class='drawGroupSeparator'>{$text}</div>
				  </td></tr>";
    }


    //---
    protected function DrawSmartFilters()
    {

        $ATC = (isset($this->parent->nav->param["_ATC"])) ? explode(":", $this->parent->nav->param["_ATC"]) : array();

        $enabled = false;

        $html = "<tr>";
        $tdEmpty = "<td style='background-color: #f9f9f9;'></td>";

        if ($this->properties["numberOfString"])
            $html .= $tdEmpty;

        if (get_class($this) == "TableMass" && !isset($this->properties["showButtons"]))
            $html .= $tdEmpty;

        foreach ($this->columns as $k => $item) {

            if (!in_array($item->key, $ATC) && isset($this->parent->nav->param["_ATC"])) continue;

            $html .= "<td style='background-color: #f9f9f9;'>";

            if (isset($this->parent->actions[$item->key])) {
                if (method_exists($this->parent->actions[$item->key], "GetInnerMicro")) {
                    $html .= $this->parent->actions[$item->key]->GetInnerMicro();
                    $enabled = true;
                }
            }

            $html .= "</td>";
        }

        if ($this->buttons && !isset($this->properties["showButtons"])) $html .= $tdEmpty;

        $html .= "</tr>";

        echo ($enabled) ? $html : "";

    }

    //----
    public function Draw()
    {

        $this->colspan = count($this->columns) + 1;
        if ($this->properties["numberOfString"])
            $this->colspan++;

        if (empty($this->data))
            $this->data = $this->parent->data;

        $this->SetParent();

        $tableHover = ($this->properties["hover"]) ? "table-hover" : "";
        $stripped = ($this->properties["stripped"]) ? "table-striped" : "";
        $order = (isset($this->parent->nav->param['order'])) ? $this->parent->nav->param['order'] : false;

        $ATC = (isset($this->parent->nav->param["_ATC"])) ? explode(":", $this->parent->nav->param["_ATC"]) : array();


        echo "<div class='tableWrapper'>
				<table border='0' class='table {$stripped} {$tableHover}' cellpadding='0' cellspacing='0'>";
        echo "<thead id='thead'>";

        if ($this->properties["numberOfString"]) {
            echo "<th align='center' width='30px'></th>";
        }

        foreach ($this->columns as $item) {

            if (!in_array($item->key, $ATC) && isset($this->parent->nav->param["_ATC"])) continue;

            $this->parent->nav->freeze();
            $class = ($item->key == $order)
                ? "class='tableHeadActive tableHeadTooltip'" : "class='tableHeadTooltip'";

            echo "<th {$class} {$item->align} {$item->width}";

            $tTame = "<div ";
            if ($item->enableSort == true) {
                $desc = ($item->key == $order) ?
                    ((isset($this->parent->nav->param['desc'])) ? 0 : 1) : 1;

                $params = "{'desc':{$desc}, 'order':'{$item->key}'}";
                $tTame .= "onclick=\"{$this->parent->ajax->eid}.Link({$params}); return false;\"";
            }

            if ($item->enableSort == false) {
                $tTame .= "style='cursor:default;'";
            }

            $tTame .= ">{$item->name}</div>";


            $ttContent = (method_exists($item, "GetToolTip"))
                ? $item->GetToolTip($this->parent->ajax->controller)
                : "";


            echo ">{$tTame} {$ttContent}</th>";

            $this->parent->nav->unFreeze();
        }

        echo $this->GetActionsHead();

        $this->DrawSmartFilters();

        echo "</thead>";

        $counter = -1;

        //Debug::Dump($this->data);

        if (!empty($this->data))
            foreach ($this->data as $key => $row) {
                $counter++;

                if ($this->isEvent("OnDrawRow")) $this->events["OnDrawRow"]($this, $row);

                $class = ($this->getProperty("rowClass")) ? " {$this->properties['rowClass']}" : "";
                $class .= ($this->getProperty("rowDeleted")) ? " tableRowDeleted" : "";
                $class .= ($this->getProperty("rowDisabled")) ? " tableRowDisabled" : "";

                $massDisabled = ($this->getProperty("rowDeleted") || $this->getProperty("rowDisabled"))
                    ? "checkboxDisabled" : "";

                echo "<tr class='tableRows {$class}' onClick='TableRowsClick(this);'>";


                if ($this->properties["numberOfString"]) {
                    $count = ($counter + 1) + ($this->parent->pager->currentPage - 1)
                        * $this->parent->pager->countRowsOnPage;
                    echo "<td align='center'  valign='" . $this->properties["valign"] . "' width='30px'>" . $count . "</td>";
                }
                foreach ($this->columns as $key2 => $item) {

                    if (!in_array($item->key, $ATC) && isset($this->parent->nav->param["_ATC"])) continue;


                    if (!isset($this->data[$key][$item->key]))
                        $this->data[$key][$item->key] = "-//-";
                    if (method_exists($item, "PostProcessing"))
                        $this->data[$key][$item->key] = $item->PostProcessing($this->data[$key][$item->key]);
                    $class = "";
                    if ($item->key == $order) {
                        $class = ($counter % 2 != 0) ? "class='tableRowActiveNc'" : "class='tableRowActiveC'";
                    }


                    echo "<td {$class} {$item->align}  valign='" . $this->properties["valign"] . "' {$item->width}>{$this->data[$key][$item->key]}</td>";
                    $class = "";
                }

                echo $this->GetActions($row, $key);

                echo "</tr>";
            }
        echo "</table></div>";

        if (empty($this->data)) {
            Message::I()->Warning($this->properties["messageEmpty"])->Draw();
        }
    }


    //----
}
