<?php

class TableMass extends Table
{
    //---
    public function DrawGroupSeparator($text)
    {
        $cs = $this->colspan + 1;
        echo "<tr><td colspan='{$cs}' style='padding:0;   background-color: #fff;'>
				  <div class='drawGroupSeparator'>{$text}</div>
				  </td></tr>";
    }

    //---
    public function Draw()
    {
        if (empty($this->data))
            $this->data = $this->parent->data;

        if (empty($this->data)) Message::I()->Error($this->properties["messageEmpty"]);
        if (empty($this->primary)) Message::I()->Error("Primary key missed")->E();
        $order = (isset($this->parent->nav->param['order'])) ? $this->parent->nav->param['order'] : false;

        $this->colspan = count($this->columns) + 1;
        if ($this->properties["numberOfString"])
            $this->colspan++;

        $this->SetParent();

        $tableHover = ($this->properties["hover"]) ? "table-hover" : "";
        $stripped = ($this->properties["stripped"]) ? "table-striped" : "";

        echo "<div class='tableWrapper'>
				<table id='table{$this->parent->ajax->eid}'
					 class='table {$stripped} {$tableHover}' cellpadding='0' cellspacing='0'>";

        echo "<thead>";
        $this->DrawDynamic();
        echo $this->GetActionsHead();

        $this->DrawSmartFilters();

        echo "</thead>";

        $ATC = (isset($this->parent->nav->param["_ATC"])) ? explode(":", $this->parent->nav->param["_ATC"]) : array();

        $counter = -1;
        if (!empty($this->data))
            foreach ($this->data as $key => $row) {
                $counter++;

                if ($this->isEvent("OnDrawRow")) $this->events["OnDrawRow"]($this, $row);

                $class = ($this->getProperty("rowClass")) ? " {$this->properties['rowClass']}" : "";
                $class .= ($this->getProperty("rowDeleted")) ? " tableRowDeleted" : "";
                $class .= ($this->getProperty("rowDisabled")) ? " tableRowDisabled" : "";

                $massDisabled = ($this->getProperty("rowDeleted") || $this->getProperty("rowDisabled"))
                    ? "checkboxDisabled" : "";

                echo "<tr class='tableRows {$class} tableRow{$key}'>";

                if ($this->properties["numberOfString"]) {
                    $count = ($counter + 1) + ($this->parent->pager->currentPage - 1) *
                        $this->parent->pager->countRowsOnPage;
                    echo "<td align='center'  valign='" . $this->properties["valign"] . "' width='30px'>" . $count . "</td>";
                }

                if (!isset($this->properties["showButtons"])) {
                    echo "<td align='center' valign='" . $this->properties["valign"] . "' class='chBoxMulti'>
								  <a href='' onclick='RefreshCheckbox(this); return false;' data-value='{$key}'
								   class='rowCheckboxes cbMultiUnSelected {$massDisabled}'></a>
							  </td>";
                }

                foreach ($this->columns as $key2 => $item) {
                    if (!in_array($item->key, $ATC) && isset($this->parent->nav->param["_ATC"])) continue;

                    if (!isset($this->data[$key][$item->key])) $this->data[$key][$item->key] = "-//-";
                    if (method_exists($item, "PostProcessing"))
                        $this->data[$key][$item->key] = $item->PostProcessing($this->data[$key][$item->key]);

                    if ($item->key == $order && !isset($this->properties["sortHover"])) {
                        $class = ($counter % 2 != 0) ? "class='tableRowActiveNc'" : "class='tableRowActiveC'";
                    }
                    echo "<td {$class} {$item->align} valign='" . $this->properties["valign"] . "'>{$this->data[$key][$item->key]}</td>";
                    $class = "";
                }

                echo $this->GetActions($row, $key);


                echo "</tr>";

            }

        echo "</table></div>";

    }

    //ull_Flava_Sessions_with_J-PaNiK_and_SlickWillyDee_on_NuBreaksDotCom_14032014


    //---
    private function DrawDynamic()
    {
        $order = (isset($this->parent->nav->param['order'])) ? $this->parent->nav->param['order'] : false;


        if ($this->properties["numberOfString"]) {
            echo "<th align='center' width='30px'></th>";
        }
        if (!isset($this->properties["showButtons"])) {
            echo "<th width='17px' class='chBoxMulti'>
					  <a href='' onclick='TableMassClick(this); return false;' style='width: 5px;'
					   class='rowCheckboxes cbMultiUnSelected'></a>
				  </th>";
        }


        $ATC = (isset($this->parent->nav->param["_ATC"])) ? explode(":", $this->parent->nav->param["_ATC"]) : array();


        foreach ($this->columns as $item) {

            if (!in_array($item->key, $ATC) && isset($this->parent->nav->param["_ATC"])) continue;

            $this->parent->nav->freeze();

            $class = ($item->key == $order)
                ? "class='tableHeadActive tableHeadTooltip'" : "class='tableHeadTooltip'";

            echo "<th {$class} {$item->align} {$item->width}";

            $tTame = "<div ";
            if ($item->enableSort == true) {
                $desc = ($item->key == $order) ?
                    (isset($this->parent->nav->param['desc']) ? 0 : 1) : 1;

                $params = "{'desc':{$desc}, 'order':'{$item->key}'}";
                $tTame .= "onclick=\"{$this->parent->ajax->eid}.Link({$params}); return false;\"";
            }

            if ($item->enableSort == false) {
                $tTame .= "style='cursor:default;'";
            }

            $tTame .= ">{$item->name}</div>";


            $ttContent = (method_exists($item, "GetToolTip"))
                ? $item->GetToolTip($this->parent->ajax->controller)
                : "";

            echo ">{$tTame} {$ttContent}</th>";

            $this->parent->nav->unFreeze();
        }


    }


}
