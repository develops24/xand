<?php

class TableBase extends ClassBase
{

    public $name = "table";
    public $columns = array();
    public $buttons = array();
    public $colspan = 0;
    public $primary;
    public $data;
    protected $timer = false;
    protected $eid;

    function __construct() {
        $args = func_get_args();

        $this->setProperty("enableDrawButtons", true)
            ->setProperty("numberOfString", true)
            ->setProperty("valign", "center")
            ->setProperty("stripped", true)
            ->setProperty("hover", true)
            ->setProperty("messageEmpty", "Data not found.");

        foreach ($args as &$object) {
            if ($object instanceof TableColumn) {
                array_push($this->columns, $object);
            } elseif ($object instanceof ButtonBase || $object instanceof ButtonDouble) {
                $this->buttons[$object->action] = $object;
            }
        }

    }


    //----
    public function SetParent() {

        if (!empty($this->buttons)) {

            $current = current($this->buttons);
            if ($current->parent == NULL) {
                foreach ($this->buttons as $b)
                    $b->assignParent($this->parent);
            }
        }

        return $this;
    }


    //----
    public function SetPrimary($primary) {
        $this->primary = $primary;
        return $this;
    }

    //----
    public function Add() {
        $args = func_get_args();

        foreach ($args as &$object) {

            if ($object instanceof TableColumn) {
                array_push($this->columns, $object);
            } elseif ($object instanceof ButtonBase || $object instanceof ButtonDouble) {
                $object->assignParent($this->parent);
                $this->buttons[$object->action] = $object;
            } elseif ($object instanceof TemplateCms) {
                $this->setProperty("tpl", $object);
            }
        }
        return $this;
    }

    //----
    public function GetActionsHead() {
        if ($this->buttons && !isset($this->properties["showButtons"])) {
            if (isset($this->parent->nav->param["_CA"]))
                return '<th align="center"> A </th>';
            else
                return '<th align="center"> Actions </th>';
        }

    }

    //----
    public function GetAction($data, $row, $enabled) {
        $output = "";

        foreach ($this->buttons as $key => $item) {

            $item->SetData($data);

            if (!$item instanceof ButtonBase) {
                $item->SetFieldData($row[$item->GetFieldNameDb()]);
                $iterator = ($item->objects[2]->Equal()) ? 0 : 1;

                $item->objects[$iterator]->setProperty("enabled", $item->getProperty("enabled"));
                $item = $item->objects[$iterator];
            }

            $confirm = ($item->properties["dialogEnable"]) ? "data-confirm=\"{$item->properties["dialog"]}\"" : "";
            $class = (isset($item->properties["svg"])) ? "buttonTable " . $item->properties["svg"] : "buttonTable";
            $styles = (isset($item->properties["styles"])) ? "style=\"{$item->properties["styles"]}\"" : "";
            $href = "";
            $onclick = "";

            $enabledOld = $item->properties["enabled"];
            if (!$enabled) $item->setProperty("enabled", false);


            if (!$item->getProperty("enabled")) {
                $class .= " buttonTableInactive";
            } else {
                if (isset($item->properties["href"])) {
                    $href = "href=\"{$item->properties["href"]}\" target=\"_blank\"";
                } else {
                    $onclick = "onclick=\"Button" . $item->properties['script'] .
                        "(this, {$this->parent->ajax->eid}, '{$item->data}'); return false;\"";
                }
            }

            $output .= "<a {$href} class=\"{$class}\" {$confirm} data-action = \"{$item->action}\"
                         {$styles} {$onclick}></a>";

            $item->setProperty("enabled", $enabledOld);


        }

        return $output;


    }


    //----
    public function GetActions($row, $key) {
        $output = "";

        if (!isset($this->properties["showButtons"]) && $this->buttons) {

            $enabled = ((isset($this->properties["rowDeleted"]) && $this->properties["rowDeleted"]))   ? false : true;
            $enabled = ((isset($this->properties["rowDisabled"]) && $this->properties["rowDisabled"])) ? false : $enabled;

            if (isset($this->parent->objects['state']->param["_optionCompactActions"])) {
                $output .= "<td align='center' width='20px'><div class='tableActionWrapper'><i></i><div class='tableActionBody'>";
                if ($this->getProperty("enableDrawButtons"))
                    $output .= $this->GetAction($key, $row, $enabled);
                $output .= "</div></div>";

            } else {
                $actionsWidth = 21 * (count($this->buttons)) + 6;
                $actionsWidth = ($actionsWidth > 60) ? $actionsWidth : 60;

                $output .= "<td align='center' width='{$actionsWidth}'>";
                if ($this->getProperty("enableDrawButtons"))
                    $output .= $this->GetAction($key, $row, $enabled);

                $output .= "</td>";

            }


        }

        return $output;

    }


}