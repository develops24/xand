<?php

class TableColumnDateUpdate extends TableColumnDate
{	
	function __construct()
	{  		
		$arg 			  = func_get_args();		
		$this->formatLong = (isset($arg[0])) ? true : false; 			
		
		$this->key 		= "dateUpdate";
		$this->name 	= "Updated";
		$this->width 	= "width = '120px'";
		$this->align	= "align = 'center'";		  
	}	
	public function GetToolTip($controller){}
}

	
?>