<?php

/**
 * Class TableColumn
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class TableColumn
{
    public $key = "";
    public $name = "";
    public $width = "";
    public $align = "";
    public $primary = false;
    public $enableSort = true;

    /**
     * TableColumn constructor.
     */
    function __construct()
    {
        $args = func_get_args();
        $this->key = (isset($args[0])) ? $args[0] : "undefined key";
        $this->name = (isset($args[1])) ? $args[1] : $args[0];
        $this->width = (isset($args[2])) ? "width = '" . $args[2] . "px'" : "";
        $this->align = (isset($args[3])) ? "align = '" . $args[3] . "'" : "align = 'left'";
        $this->primary = (isset($args[4])) ? $args[4] : false;
        $this->enableSort = (isset($args[5])) ? $args[5] : true;
    }

    /**
     * @param $controller
     *
     * @return string
     */
    public function GetToolTip($controller)
    {
        return "";
    }
}