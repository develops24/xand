<?php

class TableColumnSpoiler extends TableColumn
{
	public $strlen;
	public $title;
	public $replacement;

    //---
    function __construct($key = "Undefined", $name = "Undefined", $width = false, $align = "left", $strlen=40, $enableSort = true)
    {
        $this->key 	       = $key;
        $this->name 	   = $name;
        $this->width 	   = ($width) ? 	"width = '{$width}px'" 	:  "";
        $this->align	   = "align = '{$align}'";
        $this->strlen	   = $strlen;
        $this->enableSort  = $enableSort;
		$this->replacement = array("<BR>" => ", ", "<br />" => ", ", "<br>" => ", ");

    }


	//---
	public function PostProcessing($str)
	{

        $str = str_replace("'","", $str);
		if (strlen($str) > $this->strlen)
		{
			$strCut  = mb_substr($str, 0, $this->strlen, "UTF8") . " ...";
			$strCut  = strtr($strCut, $this->replacement);

			if(strpos($strCut, ",") !== false)
			{
				$strCutArr = explode(",", $strCut);
				$strCut    = $strCutArr[0]." ...";
			}

			return "
 				     <div style='display: table'>
 					 	<i class='fa fa-plus-square-o tableColumnSpoilerAction' onclick='ColumnSpoilerAction(this);'></i>
					 	<div class='tableColumnSpoilerContent' title='{$str}'>{$strCut}</div>
					 </div>
					 ";
		}
		else
		{
			$str  = strtr($str, $this->replacement);
			return $str;
		}

	}

	//---
	public function GetToolTip($controller){}
	
}
