<?php

/**
 * Class TableColumnFlagIp
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class TableColumnFlagIp extends TableColumn
{
    /**
     * TableColumnFlagIp constructor.
     */
    function __construct()
    {
        $args = func_get_args();
        $this->key = (isset($args[0])) ? $args[0] : "undefined key";
        $this->name = (isset($args[1])) ? $args[1] : $args[0];
        $this->width = (isset($args[2])) ? "width = '" . $args[2] . "px'" : "width = '30px'";
        $this->align = "align = 'center'";
    }

    /**
     * @param $str
     *
     * @return string
     */
    public function PostProcessing($str)
    {
        $c = new Country();
        return $c->getHtmlBlock($str);
    }

    /**
     * @param $controller
     *
     * @return bool
     */
    public function GetToolTip($controller)
    {
        return false;
    }

}