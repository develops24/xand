<?php

class TableColumnDate extends TableColumn
{
    function __construct()
    {
        $args = func_get_args();
        $this->key = (isset($args[0])) ? $args[0] : "undefined key";
        $this->name = (isset($args[1])) ? $args[1] : $args[0];
        $this->formatLong = (isset($args[2])) ? true : false;
        $this->width = (isset($args[3])) ? "width = '" . $args[3] . "px'" : "width = '120px'";

        $this->align = "align = 'center'";
    }

    public function PostProcessing($str)
    {
        if ($str == "" || $str == 0) {
            return "-//-";
        } else {
            if (!is_numeric($str)) {
                $dt = DateTime::createFromFormat("Y-m-d H:i:s", $str);
                $str = $dt->modify('+1 hour')
                    ->format('U');
            }

            return ($this->formatLong) ? Date::FormatLong($str) : Date::Format($str);

        }

    }

    public function GetToolTip($controller)
    {
    }


} 