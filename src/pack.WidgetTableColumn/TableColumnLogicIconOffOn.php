<?php

class TableColumnLogicIconOffOn extends TableColumn
{	
	function __construct()
	{  
		$args=func_get_args(); 		
		$this->key 	= (isset($args[0])) 		    ? 	$args[0] 					: 	"undefined key";
		$this->name 	= (isset($args[1])) 		? 	$args[1] 					: 	$args[0];
		$this->width 	= (isset($args[2])) 		? 	"width = '".$args[2]."px'" 	: 	"width = '18px'";
		$this->align	= "align = 'center'";		  
	}
	
	public function PostProcessing($str)
	{ 
		return ($str) ? "<i class='fa fa-check-circle' style='color: #8ed74d;'></i>"
			  		  : "<i class='fa fa-minus-circle' style='color: #ED4545;'></i>";
	}
	  
	public function GetToolTip($controller){}
	
}
