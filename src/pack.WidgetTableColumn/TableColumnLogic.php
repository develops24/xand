<?php

class TableColumnLogic extends TableColumn
{	
	function __construct()
	{  
		$args=func_get_args(); 		
		$this->key 	= (isset($args[0])) 		? 	$args[0] 					: 	"undefined key";
		$this->name 	= (isset($args[1])) 		? 	$args[1] 					: 	$args[0];
		$this->width 	= (isset($args[2])) 		? 	"width = '".$args[2]."px'" 	: 	"width = '100px'";
		$this->align	= "align = 'center'";		  
	}
	
	public function PostProcessing($str)
	{
		return ($str) ? "Yes" : "No";
	}
	
	public function GetToolTip($controller){}
	
}

	
?>