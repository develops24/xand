<?php

/**
 * Class GrouperRow
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   2
 *
 *	
 * 
 *
 **/

class GrouperTab extends GrouperBase
{
	public function SetDefaults()
	{		
	    $this->setProperty("width","100%")
		 	 ->setProperty("leftWidth",100)		
			 ->setProperty("height",300)
			 ->setProperty("heightEnable", true)
			 ->setProperty("show", false);	  	   		
	}	
	
	public function Draw()
	{  	
		
		 $show   = ($this->properties['show'] == false) ? "display: none; " : "display: block; ";			 
		 $height = ($this->getProperty('height')) ? "height:{$this->getProperty('height')}px; " : "";
	 	 $height = ($this->getProperty('heightEnable')) ?   $height : "";		 
		 
		 echo "<div class='grouperTabContent' id='{$this->domId}'  style='{$height} {$show}'  id='".$this->domId."'>";							  
			   
		 foreach($this->dataArray as $key => $val)
		 {	 
			 if($val->data == "" && isset($this->parent->dataArray[0][$val->key]))  
				 $val->data = $this->parent->dataArray[0][$val->key];				 
			 $val->Draw();									
						
		 }	 		 		
		 echo "</div>";		
		 
		 if(isset($this->properties["onStart"]))
		 	echo "<script>{$this->properties['onStart']}</script>";
	}	
}
?>


