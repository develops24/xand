<?php
 

class GrouperRow extends GrouperBase
{
	public function SetDefaults()
	{		
	    $this->setProperty("width","100%")
		 	 ->setProperty("leftWidth",100)		
			 ->setProperty("class","controlGrouperWrap")
			 ->setProperty("show", false);	  	   		
	}	
	
	public function Draw()
	{  		 
		 $height = ($this->getProperty('height')) ? "style='height:{$this->getProperty('height')}px'" : "";
		
		
		 $cssShow = ($this->getProperty('show')) ? "controlGrouperDown" : "controlGrouperUp"; 
		 $sign    = ($this->getProperty('show')) ? "fa-chevron-down" : "fa-chevron-up";  
		 
		 echo "<div class ='formElement'>
		 	   <div class = 'controlGrouperTitle' 
			     onclick='GrouperSpoiler(this);'>
				 <i style='float: left; padding: 6px 6px 0  0;' class='fa {$sign}'></i> {$this->name}</div>
		 	   <div class='{$this->getProperty('class')} {$cssShow}' {$height}>";
		 foreach($this->dataArray as $key => $val)
		 {
//             var_dump($val->data);
			 if($val->data == "" && isset($this->parent->dataArray[0][$val->key]))
				 $val->data = $this->parent->dataArray[0][$val->key];
			 $val->Draw();									
						
		 }	 		 		
		 echo "</div></div>";		
	}	
}


