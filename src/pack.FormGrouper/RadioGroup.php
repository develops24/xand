<?php

class RadioGroup extends GrouperBase {

    public function SetDefaults() {
        $this->setProperty("width", "100%")
                ->setProperty("titleWidth", 160)
                ->setProperty("class", "controlGrouperWrap")
                ->setProperty("active", false)
                ->setProperty("show", false);
    }

    //---------------
    public function GetDataJson() {
        $result = ", '" . $this->key . "': document.getElementById('" . $this->domId . "').checked";
        foreach ($this->dataArray as $key => $val)
            $result .= $val->GetDataJson();
        return $result;
    }

    //---------------
    public function Draw() {
        ?>        
        <script>

            function ActivateContaner<?php echo $this->domId; ?>(dom)
            {
                $(".radioBoxBlocks").addClass("radioBoxDisabled");


                if ($(dom).attr("checked") == "checked")
                {
                    $("#radioBoxBlock<?php echo $this->domId; ?>").removeClass("radioBoxDisabled");

                }


            }

        </script> 

        <div style="height:3px;"></div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr> 
                <td valign="top" align="left" width="<?php echo ($this->properties['titleWidth']); ?>px">    
                   <input id="<?php echo $this->domId; ?>" onclick="ActivateContaner<?php echo $this->domId; ?>(this)" 
                    <?php
                    echo ($this->getProperty("active")) ? "checked='checked'" : "";
                    ?> style="height: 25px; line-height: 25px; display:inline-block; padding:0" type="radio" name="radioGroup"/>
                    
                    
                    <label style="height: 25px; line-height: 25px; display:inline-block"><?php echo $this->name; ?></label>
                </td>
             <td valign="top" align="left">
                 <?php if (!empty($this->dataArray)) : ?><div class="radioBox">            
            <?php
            foreach ($this->dataArray as $key => $val) {
                if ($val->data == "" && isset($this->parent->dataArray[0][$val->key]))
                    $val->data = $this->parent->dataArray[0][$val->key];

                $val->Draw();
            }
            ?>
            <div id="radioBoxBlock<?php echo $this->domId; ?>" class="radioBoxBlocks <?php
            echo ($this->getProperty("active")) ? "" : "radioBoxDisabled";
            ?>"></div>
            
        </div>
 		<?php endif; ?>
        <div style="height:3px;"></div>

                </td>
               

            </tr>
        </table> 
        <?php
    }

}