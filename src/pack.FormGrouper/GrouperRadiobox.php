<?php

class GrouperRadiobox extends GrouperBase {

    public function SetDefaults() {
        $this->setProperty("width", "100%")
                ->setProperty("titleWidth", 134)
                ->setProperty("class", "controlGrouperWrap")
                ->setProperty("active", false)
                ->setProperty("toggle", false)
                ->setProperty("show", false);
    }

    //---
    public function GetDataJson()
    {
        $result = ", '".$this->key."': $('#".$this->domId."').val()";
        foreach($this->dataArray as $key => $val)	$result .= $val->GetDataJson();
        return $result;
    }

    //---
    public function Draw() {
   {
            $active = ($this->getProperty("active")) ? true : false;
            $disabled = ($active) ? "" : "grouperCheckboxDisabled";

            echo "
                    <script>
                        var {$this->domId} = '{$this->data}';
                        function GrouperRadioboxClick(dom)
                        {

                             $('.grouperRadioboxBody > .grouperCheckboxBlocks').removeClass('grouperCheckboxDisabled').addClass('grouperCheckboxDisabled');
                             $('.grouperRadioboxHead > a').removeClass('radioSel').addClass('radioUnSel');

                             $(dom).removeClass('radioUnSel').addClass('radioSel');
                             $(dom).parent().next().find('.grouperCheckboxBlocks').removeClass('grouperCheckboxDisabled');

                             $('.grouperCheckboxData').val('');
                             $(dom).parent().next().find('.grouperCheckboxData').val('1');

                        }
                    </script>
                 ";
            ?>

                <div class="radioWrap grouperRadioboxHead">
                    <a href="" onclick="GrouperRadioboxClick(this); return false;"
                       class="<?php echo ($this->getProperty("active")) ? "radioSel" : "radioUnSel"; ?>">
                            <?php echo $this->name; ?>
                    </a>
                </div>

                <div class="grouperCheckboxMini grouperRadioboxBody" style="border: dotted 1px #aaa; margin-bottom: 20px; padding: 10px; ">

                    <?php
                    foreach($this->dataArray as $key => $val)
                    {
                        if($val->data == "" && isset($this->parent->dataArray[0][$val->key]))
                            $val->data = $this->parent->dataArray[0][$val->key];

                        $val->Draw();

                    }
                    echo "<div style='clear:both'></div>";

                    ?>
                    <div class="grouperCheckboxBlocks <?php echo $disabled;  ?>"></div>
                    <input type="hidden" class="grouperCheckboxData" id="<?php echo $this->domId; ?>" value="<?php echo $active; ?>" />

                </div>

            <?php
        }
    }

}