<?php

/**
 * Class GrouperBase
 *
 * @тип     Class
 * @пакет    Form
 * @версия   1
 *
 *
 **/
class GrouperBase extends ClassBase
{

    public $data;
    public $dataArray = array();

    public $domId = "";

    public $key = "";
    public $name = "";

    //------
    function __construct()
    {
        $arr = func_get_args();

        $this->name = (isset($arr[0])) ? $arr[0] : "";
        $this->key = (isset($arr[1])) ? $arr[1] : "notdefined";
        $properties = (isset($arr[2])) ? $arr[2] : array();
        $this->data = (isset($arr[3])) ? $arr[3] : "";
        $this->dataArray = (isset($arr[4])) ? $arr[4] : array();

        $this->domId = $this->getUniqueName();

        if (method_exists($this, 'SetDefaults')) $this->SetDefaults();

        if (!empty($properties)) {
            foreach ($properties as $key => $val) {
                $this->properties[$key] = $val;
            }
        }

    }

    //-------
    public function Add()
    {
        $args = func_get_args();
        foreach ($args as $object) {
            if ($object instanceof InputBase) {
                if ($object->data == false)
                    $object->data = (isset($this->parent->dataArray[0][$object->key]))
                        ? $this->parent->dataArray[0][$object->key]
                        : false;

                array_push($this->dataArray, $object);

            } elseif ($object instanceof GrouperBase) {
                $object->assignParent($this->parent);

                foreach ($object->dataArray as $key => $obj) {

                    $object->dataArray[$key]->data = (isset($this->parent->dataArray[0][$obj->key]))
                        ? $this->parent->dataArray[0][$obj->key]
                        : false;
                }

                array_push($this->dataArray, $object);
            } else echo get_class($object) . " : not supports InputBase";
        }
        return $this;
    }

    //-------
    public function GetDataJson()
    {
        $result = "";
        foreach ($this->dataArray as $key => $val) $result .= $val->GetDataJson();
        return $result;
    }


}