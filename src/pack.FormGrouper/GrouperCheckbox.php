<?php
class GrouperCheckbox extends GrouperBase
{
    public function SetDefaults()
	{		
	    $this->setProperty("width","100%")	
			 ->setProperty("titleWidth",30) 		 
			 ->setProperty("active", false)
			 ->setProperty("data", array())
			 ->setProperty("show", false)	  
			 ->setProperty("defaultColumnWidth", 160);		
	}	
	
	//---------------
	public function GetDataJson()
	{			
		$result = ", '".$this->key."': $('#".$this->domId."').val()";
		foreach($this->dataArray as $key => $val)	$result .= $val->GetDataJson();			
		return $result;
	}
	
	//---------------
	public function Draw()
	{   
	 
	   $active = ($this->getProperty("active")) ? true : false;	
				
	   if(isset($this->properties["data"][0][$this->key])) 
		   $checked = ($this->properties["data"][0][$this->key]) 
				  ? $this->properties["data"][0][$this->key] : false; 

	   $checked  = ($active) ? "checkboxSel" : "checkboxUnSel";
	   $disabled = ($active) ? "" 			 : "grouperCheckboxDisabled";	 
	 				   
					   
	   
		?>     
         <div style="height:3px;"></div> 
	     <div> 
         <div style="width: <?php echo $this->properties['titleWidth']; ?>px; float:left; padding-top:3px;"> 
         
            <div style="height: 25px; line-height: 25px; display:inline-block; padding:0; margin:0;  margin-right:3px; ">
            <a href="" class="checkbox <?php echo $checked; ?>" 
            	       onclick="ActivateContaner(this, '<?php echo $this->domId; ?>'); return false;">
				<?php echo $this->name; ?></a> 
            </div> 
            
         </div>
         <div style="padding-left: <?php echo $this->properties['titleWidth']; ?>px;">   
          	    <div class="grouperCheckboxMini">            
					<?php   
					 foreach($this->dataArray as $key => $val)
					 {	   				 
						  if($val->data == "" && isset($this->parent->dataArray[0][$val->key]))  
								$val->data = $this->parent->dataArray[0][$val->key];	 
							
						  $width = (isset($this->properties['columnsWidth'][$key])) ? $this->properties['columnsWidth'][$key] 
																				   : $this->getProperty("defaultColumnWidth"); 
							
						  echo "<div style='float:left; display:block; width: {$width}px;'>";
							  $val->Draw();									
						  echo "</div>";
									  
					 }	 
						 echo "<div style='clear:both'></div>"; 
							 
                    ?>
                   <div class="grouperCheckboxBlocks <?php echo $disabled;  ?>"></div>
                   <input type="hidden" id="<?php echo $this->domId; ?>" value="<?php echo $active; ?>" />
   			 	</div>
             
       	 </div>
       			 <div style="height:3px; clear:left;"></div> 
        </div>
        
        <?php 
	}	
}



?>

