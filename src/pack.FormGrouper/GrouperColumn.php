<?php

class GrouperColumn extends GrouperBase
{
    public function SetDefaults()
    {
        $this->setProperty("width", "100%")
            ->setProperty("titleWidth", 30)
            ->setProperty("active", false)
            ->setProperty("data", array())
            ->setProperty("show", false)
            ->setProperty("defaultColumnWidth", 160);
    }

    //---------------
    public function GetDataJson()
    {
        $result = "";
        foreach ($this->dataArray as $key => $val) $result .= $val->GetDataJson();
        return $result;
    }

    //---------------
    public function Draw()
    {

        ?>
        <div style="height:3px;"></div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="<?php echo $this->properties['titleWidth']; ?>px" align="left" valign="top">
                   <label style="line-height: 25px;"><?php echo $this->name; ?></label>
                </td>
                <td valign="top" align="left">


                    <?php
                    foreach ($this->dataArray as $key => $val) {
                        if ($val->data == "" && isset($this->parent->dataArray[0][$val->key]))
                            $val->data = $this->parent->dataArray[0][$val->key];

                        $val->properties["titleDisable"] = true;

                        $width = (isset($this->properties['columnsWidth'][$key])) ? $this->properties['columnsWidth'][$key]
                            : $this->getProperty("defaultColumnWidth");

                        echo "<div style='float:left; display:block; width: {$width}px;'>";
                        $val->Draw();
                        echo "</div>";

                    }
                    echo "<div style='clear:both'></div>";

                    ?>

                    <div style="height:3px;"></div>
                    <?php
                    if ($this->getProperty('requirements') != "")
                        echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";
                    ?>

                </td>
            </tr>
        </table>
        <?php
    }

}

?>

