<?php
/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 12.11.15
 * Time: 13:43
 */

namespace Lib\PackCms\BaseSericeCommand;

use Symfony\Component\Console\Command\Command;

class BaseServiceCommand extends Command{

    public function __construct($name = null){
        parent::__construct($name);
        error_reporting(E_ALL);
//        sleep(mt_rand(1,30));
        ini_set("memory_limit", "150M");
        ini_set("pcre.backtrack_limit", 10000000);
        ini_set('max_execution_time', 0);
        ini_set('max_input_time', 0);
    }

    public function __destruct(){

    }

}