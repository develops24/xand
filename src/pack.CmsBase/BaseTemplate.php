<?php

/**
 * Class BaseTemplate
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class BaseTemplate
{

    const TPL_FOLDER = "phtml";

    public $data = array();

    protected $tplFile;
    protected $dir;


    /**
     * @param $arr
     *
     * @return $this
     */
    public function assignArray($arr)
    {
        if (!($arr)) return $this;

        foreach ($arr as $k => $v)
            $this->assign($k, $v);

        return $this;
    }

    /**
     * @param $key
     * @param $val
     *
     * @return $this
     */
    public function assign($key, $val)
    {
        $this->data[$key] = $val;
        return $this;
    }


    /**
     * @param $content
     *
     * @return bool
     */
    public function RenderCompiled($content)
    {
        return true;
    }


    /**
     * @param $matches
     *
     * @return mixed|string
     */
    public function TplDecode($matches)
    {
        $match = explode(":", $matches[1]);
        if (is_callable([$this, $match[0]], true)) {
            $call = array_shift($match);
            if (!isset($match[0]))
                $match[0] = false;
            if (!isset($match[1]))
                $match[1] = false;
            $output = $this->$call(...$match);
        } else
            $output = "This method {$match[0]}, not supports in the X-CMF";
        return $output;
    }


    /**
     *
     */
    public function Load()
    {
        include_once($this->Compile());
    }

    /**
     * @return string
     */
    private function Compile()
    {
        $pathSource = PATH_REAL . DS . $this->dir . DS . $this->tplFile;
        $this->tplFile = str_replace(DS, "-", $this->tplFile);
        if (!Config::makeDirectory(PATH_CTPL))
            exit("Can't create system directories. Please check permission");
        $pathCompiledTpl = PATH_REAL . DS . PATH_CTPL . DS . ((defined('SITE_CURRENT_LANG')) ? SITE_CURRENT_LANG : SITE_DEFAULT_LANG);

        if (!is_dir($pathCompiledTpl)) {
            mkdir($pathCompiledTpl);
        }

        if (!(file_exists($pathCompiledTpl . DS . $this->tplFile) && FC_COMPILE_TEMPLATES)) {

            if (!file_exists($pathSource)) {
                exit("This template {$pathSource}, not found.");
            }

            $content = file_get_contents($pathSource);
            $content = $this->Render($content, false);
            $content = I18n::__($content);

            $sources = "";
            if (!empty($this->sources)) {
                $sources = "<?php " . implode("\r\n", $this->sources) . "?>";
            }

            file_put_contents($pathCompiledTpl . DS . $this->tplFile, $sources . $content);

        }

        return $pathCompiledTpl . DS . $this->tplFile;
    }

    /**
     * @param $content
     * @param bool $innerRender
     *
     * @return mixed|string
     */
    public function Render($content, $innerRender = true)
    {
        while (true) {
            if (substr_count($content, "{{") == 0) break;
            $content = preg_replace_callback("|{{(.*)}}|U", array($this, "TplDecode"), $content);
        }

        if ($innerRender) {
            ob_start();
            eval(" ?> " . $content . " <?php ");
            $content = ob_get_contents();
            ob_end_clean();
        }

        return $content;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        ob_start();
        include_once($this->Compile());
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }


    /**
     * @param $file
     *
     * @return bool|string
     */
    public function Inc($file)
    {
        $path = ($file[0] == DS)

            ? PATH_REAL . DS . PATH_APP . DS . $file . ".phtml"
            : PATH_REAL . DS . PATH_PUBLIC . PATH_TPL . DS . PATH_THEME . DS . "phtml" . DS . $file . ".phtml";

        return (file_exists($path)) ? file_get_contents($path) : "File does not exist: " . $path;
    }


    /**
     * @param $par
     * @param bool $call
     *
     * @return mixed|string
     */
    public function Data($par, $call = true)
    {
        if (isset($this->isMailer) && $this->isMailer === true) {
            $call = true;
        }
        if (substr_count($par, ".") == 1) {
            $parEx = explode(".", $par);
            if ($call) {
                return $this->data[$parEx[0]][$parEx[1]];
            } else {
                return (isset($this->data[$parEx[0]][$parEx[1]])) ? '<?php echo $this->Data("' . $par . '"); ?>' : "";
            }

        } else {
            if ($call) {
                return (isset($this->data[$par])) ? $this->data[$par] : "";
            } else {
                return (isset($this->data[$par])) ? '<?php echo $this->Data("' . $par . '"); ?>' : "";
            }
        }
    }


    /**
     * @return string
     */
    public function Dir()
    {
        return PATH_DS . PATH_TPL . DS . PATH_THEME;
    }

    /**
     * @return string
     */
    public function DirVendor()
    {
        return PATH_DS . "vendor/node_modules";
    }

    /**
     * @return string
     */
    public function DirModules()
    {
        return PATH_DS . "vendor/xand_modules";
    }

    /**
     * @return string
     */
    public function DirApp()
    {
        return PATH_DS . PATH_TPL . DS . "app";
    }


    /**
     * @return string
     */
    public function Lang()
    {
        return SITE_CURRENT_LANG;
    }


    /**
     * @return string
     */
    public function PathDs()
    {
        return PATH_DS;
    }

}
