<?php

/**
 * Class ClassBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ClassBase
{

    public $properties = array();
    public $events = array();

    protected $name = "objectName";
    protected $parent;

    /**
     * @return string
     */
    public function getUniqueName()
    {
        return "id" . substr(md5(rand(0, 1000) . rand(0, 1000) . rand(0, 1000)), 0, 10);
    }

    /**
     * @param $target
     *
     * @return $this
     */
    public function assignParent(&$target)
    {
        if (!$this->parent) $this->parent = $target;
        return $this;
    }

    /**
     * @return $this
     */
    public function cloneAjax()
    {
        if ($this->parent->ajax)
            $this->ajax = $this->parent->ajax;
        return $this;
    }

    /**
     * @return $this
     */
    public function cloneState()
    {
        if ($this->parent->nav)
            $this->nav = $this->parent->nav;
        return $this;
    }

    /**
     * @return $this
     */
    public function createParent()
    {
        if (!$this->parent) $this->parent = new WidgetDummy();
        return $this;
    }

    /**
     * @param $key
     *
     * @return mixed|string
     */
    public function getProperty($key)
    {
        return (isset($this->properties[$key])) ? $this->properties[$key] : "";
    }

    /**
     * @param $key
     * @param $val
     *
     * @return $this
     */
    public function setProperty($key, $val)
    {
        $this->properties[$key] = $val;
        return $this;
    }

    //---  EVENTS
    public function isEvent($key)
    {
        return (isset($this->events[$key])) ? true : false;
    }

    /**
     * @param $key
     * @param $val
     *
     * @return $this
     */
    public function setEvent($key, $val)
    {
        $this->events[$key] = $val;
        return $this;
    }

    /**
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        if ($this->{$method} instanceof Closure) {
            return call_user_func_array($this->{$method}, $args);
        }
        return false;
    }

}