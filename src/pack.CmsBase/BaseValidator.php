<?php

/**
 * Класс нельзя вызывать самого по себе, он должен работать только в модели
 *
 * @author kubrey
 */
class BaseValidator extends BaseComponent
{

    //
    const VALIDATION_NUMERIC = 'num';
    const VALIDATION_STRING = 'string';
    const VALIDATION_EMAIL = 'email';
    const VALIDATION_REQUIRED = 'required';
    const VALIDATION_MIN = 'min';
    const VALIDATION_MAX = 'max';
    const VALIDATION_IN = 'in array';
    const VALIDATION_POSITIVE = 'positive';
    const VALIDATION_LENGTH_MIN = 'min length';
    const VALIDATION_LENGTH_MAX = 'max length';
    const VALIDATION_REGEX = 'regex';
    const VALIDATION_EQUALS_TO = 'eq';
    const VALIDATION_COMMON = 'common'; //сабправило - в качестве аргумента принимает callable функцию, которая принимает весь массив model

    protected $validateBlocks = true;
    protected $blocks2validate = array();
    protected $validateOnlyExisted = false;
    protected $rules = array();
    protected $injectedRules = array();
    protected $ejectedRules = [];

    /**
     *
     * @throws Exception
     */
    public function __construct() {
        parent::__construct();
        if (!method_exists($this, 'getTableName')) {
            //проверка, вызван ли валидатор через модель, если нет-  исключение
            throw new Exception("You could not create instance of Validator class directly. Use models instead.");
        }
    }

    /**
     * Возврващает массив правил для валидации
     * @return array
     */
    public function getRules() {
        return $this->rules;
    }

    /**
     * Валидировать только те поля, которые есть в массиве
     * @param boolean $set
     * @return \BaseModel
     */
    public function validateOnlyExisted($set = true) {
        $this->validateOnlyExisted = $set;
        return $this;
    }

    /**
     *
     * @param boolean $do
     * @return \BaseModel
     */
    public function setBlockValidation($do) {
        $this->validateBlocks = $do;
        return $this;
    }

    /**
     *
     * @param array $blocks Для проверки безблочных элементов указать _base
     * @return \BaseModel
     */
    public function defineBlocks4Validation(array $blocks = array()) {
        $this->blocks2validate = $blocks;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function validate() {
        $errorsBefore = count($this->getErrors());
        $virtualFields = array(); //starts with *, to be removed after validation

        foreach ($this->model as $k => $v) {
            if (!is_array($v) && (!in_array($k, $this->getAttributes()) && !in_array('*' . $k, $this->getAttributes()))) {
                unset($this->model[$k]);
                continue;
            }
            if (in_array('*' . $k, $this->getAttributes())) {

                $virtualFields[] = $k;
            }
            if (is_string($v)) {
                $this->model[$k] = trim($v);
            }
        }

        $rules = $this->getFinalRules();

        foreach ($rules as $field => $types) {
            $fullname = $field;
            foreach ($types as $type) {

                $this->handleValidationRule($type, $fullname, $field);
            }
        }
        $errorsAfter = count($this->getErrors());
        foreach ($virtualFields as $vf) {

            if (isset($this->model[$vf])) {
                unset($this->model[$vf]);
            }
        }
        return ($errorsAfter > $errorsBefore) ? false : true;
    }

    /**
     *
     * @param mixed $type
     * @param string $fullname
     * @param string $field
     * @return void
     */
    private function handleValidationRule($type, $fullname, $field) {
        $arr = $this->isFormArray($fullname);
        if ($arr[0]) {
            if (!$this->validateBlocks) {
                return;
            }
            if ($this->blocks2validate && !in_array($arr[1], $this->blocks2validate)) {
                return;
            }
            $field = $arr[2];
            $block = $arr[1];
            $currElem = (isset($this->model[$arr[1]][$arr[2]]) ? $this->model[$arr[1]][$arr[2]] : null);
        } else {
            if ($this->blocks2validate && !in_array('_base', $this->blocks2validate) && $field == $fullname) {
                return;
            }
            $block = null;
            $currElem = (isset($this->model[$field]) ? $this->model[$field] : null);
        }
        if (!is_string($type)) {
            $this->validateSpecialCase($type, $currElem, $field, $block);
        } else {
            $this->validateUsual($type, $currElem, $field, $block);
        }
    }

    /**
     * Валидирует по сравнительным правилам и коллбэкам
     * @param  $rule
     * @param  $currElem
     * @param  $field
     * @param  $block
     * @return void
     */
    private function validateSpecialCase($rule, $currElem, $field, $block) {
        if (!$currElem) {
            return;
        }
        if (is_callable($rule)) {
            $call = call_user_func_array($rule, array('input' => $currElem));
            if (is_array($call) && !$call[0]) {
                $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__($call[1])));
            } elseif (!$call) {
                $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__("failed to pass validation")));
            }
        } elseif (is_array($rule)) {
            $keys = array_keys($rule);
            $ruleKey = current($keys);
            $checkvalue = $rule[$ruleKey];
            if (is_scalar($checkvalue)) {
                $this->scalarValidate($ruleKey, $checkvalue, $currElem, $field, $block);
            } else {
                $this->nonscalarValidate($ruleKey, $checkvalue, $currElem, $field, $block);
            }
        }
    }

    /**
     *
     * @param type $rule
     * @param type $currElem
     * @param type $field
     * @param type $block
     */
    private function validateUsual($rule, $currElem, $field, $block) {
        switch ($rule) {
            case self::VALIDATION_NUMERIC:
                if (!$currElem) {
                    break;
                }
                if (!is_numeric($currElem)) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__("should be numeric")));
                }
                break;
            case self::VALIDATION_EMAIL:
                if (!$currElem) {
                    break;
                }
                if (!filter_var($this->model[$field], FILTER_VALIDATE_EMAIL)) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__("should be valid email")));
                }
                break;
            case self::VALIDATION_REQUIRED:
                if (!$this->validateOnlyExisted && (!$currElem || empty($currElem) || $currElem == '-1')) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__("is required")));
                }
                break;
            case self::VALIDATION_STRING:
                if (!$currElem) {
                    break;
                }
                if (!is_string($currElem)) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__("should be string")));
                }
                break;
            case self::VALIDATION_POSITIVE:
                if (!$currElem) {
                    break;
                }
                if (!is_numeric($currElem) || $currElem <= 0) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__("is required")));
                }
                break;
            case BasePayment::VALIDATE_IBAN:
                if (!$currElem) {
                    break;
                }
                $iban = new IbanComponent();
                if (!$iban->isValid($currElem)) {
                    $this->setError(array($field => I18n::__("Iban is not valid")));
                }

                break;
            case BasePayment::VALIDATE_BIC:
                if (!$currElem) {
                    break;
                }
                $bp = new BasePayment;
                if (!$bp->isBICValid($currElem)) {
                    $this->setError(array($field => I18n::__("BIC is not valid")));
                }
                break;
            case BasePayment::VALIDATE_BLZ:
                if (!$currElem) {
                    break;
                }
                $bp = new BasePayment;
                if (!$bp->isBLZValid($currElem)) {
                    $this->setError(array($field => I18n::__("Bank Code is not valid")));
                }
                break;
            case BasePayment::VALIDATE_CC:
                if (!$currElem) {
                    break;
                }
                $bp = new BasePayment;
                $this->model[$field] = str_replace(' ', '', $this->model[$field]);
                $ccValid = $bp->isCreditCardValid($this->model[$field], BasePayment::$ccTypes[$this->model['ccType']]);

                if ($ccValid !== true) {
                    $this->setError(array($field => I18n::__($ccValid[1])));
                }
                break;
            case BasePayment::VALIDATE_CC_EXPIRED:
                if (!$currElem) {
                    break;
                }
                $bp = new BasePayment;
                //проверка по году, ожидаемое поле месяца - ccMonth
//                        echo cal_days_in_month(CAL_GREGORIAN, $this->model['ccMonth'], $this->model[$field]);
                $expireAt = strtotime($bp->daysInMonth($this->model['ccMonth'], $this->model[$field]) . "." . $this->model['ccMonth'] . "." . $this->model[$field]);
                if (time() >= $expireAt) {
                    $this->setError(array($field => I18n::__('Credit card is expired')));
                }
                break;
            case BasePayment::VALIDATE_CC_TYPE:
                if (!$currElem) {
                    break;
                }
                $bp = new BasePayment;
                if (!in_array($currElem, array_keys($bp->getAvailableCreditCards()))) {
                    $this->setError(array($field => I18n::__("Credit card is not supported")));
                }
                break;
            case BasePayment::VALIDATE_CC_CODE:
                if (!$currElem) {
                    break;
                }
                if (strlen($this->model[$field]) > 4 || strlen($this->model[$field]) < 3) {
                    $this->setError(array($field => I18n::__("CVV code is not valid")));
                }
                break;
            default:

                break;
        }
    }

    /**
     * Для сравнительных правил валидации
     * @param string $rule правило self::VALIDATE_*
     * @param mixed $checkValue значение для сравниния
     * @param mixed $currElem значения переменной, которую валидируем
     * @param string $field поле формы
     * @param string $block блок формы
     */
    private function scalarValidate($rule, $checkValue, $currElem, $field, $block) {
        if (!is_scalar($checkValue)) {
            //допускаются только скалярные величины
            return;
        }
        if (!$currElem) {
            //если переменной нет
            return;
        }
        switch ($rule) {
            case self::VALIDATION_LENGTH_MIN:
                if (mb_strlen($currElem) < $checkValue) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " "
                        . I18n::__("should have at least " . $checkValue . " symbols")));
                }
                break;
            case self::VALIDATION_LENGTH_MAX:
                if (mb_strlen($currElem) > $checkValue) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " "
                        . I18n::__("should have less than " . $checkValue . " symbols")));
                }

                break;
            case self::VALIDATION_MIN:
                if ($currElem < $checkValue) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " "
                        . I18n::__("should be larger than " . $checkValue)));
                }

                break;
            case self::VALIDATION_MAX:
                if ($currElem > $checkValue) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " "
                        . I18n::__("should be less than " . $checkValue)));
                }
                break;
            case self::VALIDATION_EQUALS_TO:
                if ($currElem != $this->model[$checkValue]) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " "
                        . I18n::__("is not equal to " . $this->getLabel($checkValue))));
                }
                break;
            case self::VALIDATION_REGEX:
                preg_match($checkValue, $currElem, $matches);
//                var_dump($matches,$currElem );
                if (!isset($matches[0]) || $matches[0] != $currElem) {
                    $this->setError(array($field => $this->getLabel($field, $block) . " "
                        . I18n::__("is invalid")));
                }
                break;

        }
    }

    /**
     *
     * @param string $ruleKey
     * @param array $checkValue
     * @param string $currElem
     * @param string $field
     * @param string $block
     * @return void
     */
    private function nonscalarValidate($ruleKey, $checkValue, $currElem, $field, $block) {
        if (is_scalar($checkValue)) {
            //допускаются только нескалярные величины
            return;
        }
        if (!$currElem) {
            //если переменной нет
            return;
        }
        switch ($ruleKey) {
            //пример правила :
            // 'id'=>array(self::VALIDATION_COMMON=>array($this,'validateFunc')
            //и validateFunc должна принимать аргумент $data, который содержит все данные
            case self::VALIDATION_COMMON:
                if (is_callable($checkValue)) {
                    $call = call_user_func_array($checkValue, array('data' => $this->getAppliedData()));
                    if (is_array($call) && !$call[0]) {
                        if (is_array($call[1])) {
                            //значит массив ошибок
                            foreach ($call[1] as $f => $err) {
                                $this->setError($err);
                            }
                        } else {
                            $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__($call[1])));
                        }
                    } elseif (!$call) {
                        $this->setError(array($field => $this->getLabel($field, $block) . " " . I18n::__("failed to pass validation")));
                    }
                }
                break;
            case self::VALIDATION_IN:
                if (is_array($checkValue)) {
                    if (!in_array($currElem, $checkValue)) {
                        $this->setError([$field => $this->getLabel($field, $block) . " " . I18n::__("is invalid")]);
                    }
                }
                break;
        }
        return;
    }

    /**
     *
     * @param string $key
     * @param string|array $rule
     * @return \BaseValidator
     */
    public function injectRule($key, $rule) {
        if (array_key_exists($key, $this->injectedRules)) {
            $this->injectedRules[$key][] = $rule;
        } else {
            $this->injectedRules[$key] = array($rule);
        }
        return $this;
    }

    /**
     * Удалит валидирование поля
     * @param $key
     * @return $this
     */
    public function ejectRule($key) {
        $this->ejectedRules[] = $key;
        return $this;
    }

    /**
     * @return array
     */
    private function getFinalRules() {
        $rules = $this->getRules();
        $injected = $this->injectedRules;
        $ejected = $this->ejectedRules;
        $finalRules = array();

        foreach ($rules as $field => $types) {

            foreach ($injected as $fieldInj => $typesInj) {
                foreach ($typesInj as $ruleInj) {
                    if ($field == $fieldInj) {
                        $rules[$field][] = $ruleInj;
                    }
                }
            }
            foreach ($ejected as $key) {
                if (isset($rules[$key])) {
                    unset($rules[$key]);
                }
            }
        }
        return $rules;
    }

    /**
     *
     * @param string $field
     * @param string $rule
     * @return boolean
     */
    protected function isRuleApplied($field, $rule) {
        $rules = $this->getRules();
        foreach ($rules as $f => $rl) {
            if ($f != $field) {
                continue;
            }
            if (in_array($rule, $rl)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Валидация кастомных данных без привязки к модели
     * @param array $data массив данных
     * @param array $rules массив с правилами
     * @return boolean
     */
    public function validateVirtual($data, $rules) {
        $modelReal = $this->model;
        $this->applyFormData($data);
        $errsBefore = count($this->getErrors());
        foreach ($rules as $field => $types) {
            $fullname = $field;
            foreach ($types as $type) {
                $this->handleValidationRule($type, $fullname, $field);
            }
        }
        $this->applyFormData($modelReal);
        $errsAfter = count($this->getErrors());
        if ($errsAfter > $errsBefore) {
            return false;
        }
        return true;
    }

}
