<?php

/**
 * Description of class
 *
 * @author kubrey
 */
class BaseConsole extends BaseComponent
{

    protected $args = [];
    protected $options = [];
    private $services;
    private $path;

    /**
     *
     * @throws Exception
     */
    public function __construct() {
        parent::__construct();
        if (php_sapi_name() != "cli") {
            throw new Exception("Not available to run not in CLI mode");
        }
        $args = func_get_args();

        if ($args) {
            $this->path = $args[0][1];
            $explode = explode(DS, $this->path);
            if (count($explode) == 2) {
                $module = $explode[0];
                Xand::$currentModule = $module;
            } else {
                Xand::$currentModule = "cli";
            }

            unset($args[0][0]);
            unset($args[0][1]);

            foreach ($args[0] as $key => $arv) {
                if ($key < 2) {
                    continue;
                }
                if (strpos($arv, '--') === 0) {
                    $optionName = ltrim($arv, "-");
                    $optionArr = explode('=', $optionName);
                    $opName = $optionArr[0];
                    if (isset($optionArr[1])) {
                        $this->options[$opName] = $optionArr[1];
                    } else {
                        $this->options[$opName] = true;
                    }
                } else {
                    $this->args[] = $arv;
                }
            }
        }
        $this->services = new CpServices();
        error_reporting(E_ALL);
        if (!defined('SITE_CURRENT_LANG')) {
            define('SITE_CURRENT_LANG', SITE_DEFAULT_LANG);
        }
        ini_set("memory_limit", "150M");
        ini_set("pcre.backtrack_limit", 10000000);
        ini_set('max_execution_time', 0);
        ini_set('max_input_time', 0);
    }

    /**
     * @param $name
     * @return bool
     */
    protected function hasOption($name) {
        return isset($this->options[$name]) ? true : false;
    }

    /**
     * @param $name
     * @param null $replace
     * @return null|string
     */
    protected function getOption($name, $replace = null) {
        return (isset($this->options[$name]) ? (is_string($this->options[$name]) ? trim($this->options[$name], "\"' ") : $this->options[$name]) : $replace);
    }

    /**
     * overwrite this method to implement ur logic
     */
    public function run() {

    }

    /**
     * Завершение работы
     */
    public function __destruct() {
        exit();
//        $arr = explode("/", $this->path);
//        if (count($arr) == 2) {
//            $module = $arr[0];
//            $file = $arr[1] . "Command.php";
//            $serv = $this->services->findOne("file='" . $file . "' and module='" . $module . "'");
//            if ($serv) {
//                $dateFinish = microtime(true);
//                $execTime = round($dateFinish - $serv['dateStart'], 4);
//                $this->services->updateById($serv['id'], array('isActive' => 0, 'dateEnd' => $dateFinish, 'timeExec' => $execTime));
//            }
//            echo $file . " finished\n";
//        }
    }

}
