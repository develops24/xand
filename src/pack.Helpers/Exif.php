<?php

//-----
//
// 
//
//-----


class Exif
{  
		const Model			  = 0;
		const Lens 			  = 1;
		
		const ExposureTime    = 2;
		const FNumber   	  = 3;		
		const ISOSpeedRatings = 4;
		const DateTime 		  = 5;
		const Software        = 6;
		const Copyright       = 7;
		
		//-----------------------------------------------------------------------------------------------------
		private static function Clean($str)
		{		
			 $str = str_replace("|", "", $str);
 			 $str = str_replace("'", "", $str);		
			 $str = str_replace('"', "", $str);
		 	 $str = str_replace("}", "", $str);		
			 $str = str_replace('{', "", $str);
			 
			 $str = substr($str, 0, 20);
			 			 
			 return $str;	
		}
		//-----------------------------------------------------------------------------------------------------
		public static function Exists($data, $key)
		{
			return ($data[$key] != "") ? true : false;				
		}		
		
		//-----------------------------------------------------------------------------------------------------
		public static function Read($sourceFile)
		{		
			 $exif_data = exif_read_data($sourceFile);		
			 $data      = array();					
			
			 $data[Exif::Model]				  = (isset($exif_data['Model']))			     ? self::Clean($exif_data['Model'])				  : "";
			 $data[Exif::Lens]	 			  = (isset($exif_data['UndefinedTag:0xA434']))   ? self::Clean($exif_data['UndefinedTag:0xA434']) : ""; 
			
			 $data[Exif::ExposureTime]	 	  = (isset($exif_data['ExposureTime'])) 	 	 ? self::Clean($exif_data['ExposureTime']) 		  : "";
			 $data[Exif::FNumber]		 	  = (isset($exif_data['FNumber'])) 		     	 ? self::Clean($exif_data['FNumber'])			  : "";
			 $data[Exif::ISOSpeedRatings]	  = (isset($exif_data['ISOSpeedRatings']))   	 ? self::Clean($exif_data['ISOSpeedRatings']) 	  : "";
			 $data[Exif::DateTime] 		  	  = (isset($exif_data['DateTime'])) 	     	 ? self::Clean($exif_data['DateTime'])			  : "";
			 $data[Exif::Software] 	     	  = (isset($exif_data['Software'])) 	     	 ? self::Clean($exif_data['Software'])		      : "";
			 $data[Exif::Copyright] 	 	  = (isset($exif_data['Copyright'])) 	     	 ? self::Clean($exif_data['Copyright'])		      : "";	
				
			 return $data;	
		}
		
		//-----------------------------------------------------------------------------------------------------
		public static function Pack($data)
		{		
			 $packData = implode("|",$data);				
			 return $packData;	
		}
		
		//-----------------------------------------------------------------------------------------------------
		public static function Unpack($packData)
		{		
			 $data = explode("|",$packData);				
			 return $data;	
		}
		
		
		
}

?>
