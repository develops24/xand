<?php
class Crypt
{	 	 
	 static function Simple($str, $passw="") 
	 { 
		   $salt = "Dn8*#2n!9j";
		   $len = strlen($str);
		   $gamma = '';
		   $n = $len>100 ? 8 : 2;
		   while( strlen($gamma)<$len )
		   {
				  $gamma .= substr(pack('H*', sha1($passw.$gamma.$salt)), 0, $n);
		   }
		   return $str^$gamma;				 
	 }
	 
	 static function SimpleEncode($str) 
	 {
		 return base64_encode(self::Simple($str, 'Dn8*#2n!9j'));
	 }
	 static function SimpleDecode($str) 
	 {
		 return Crypt::Simple(base64_decode($str), 'Dn8*#2n!9j');
	 }
	 
}
?>