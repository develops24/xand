<?php
class Arrays
{	 
	 public static function Pack($array) 
	 {		 
		 if(! is_array($array)) return false;
		 $o = array();
		 
		 foreach($array as $k => $a)
		 {
			 $t = explode(".", $k);
			 if(count($t) == 2)
			 {
				 if(! isset($o[$t[0]])) $o[$t[0]] = array();
				 $o[$t[0]][$t[1]] = $a;
			 }
			 else
			 {
				 $o[$t[0]] = $a;
			 }
		 }
		 return $o;
	 }
	
	 public static function UnPack($array) 
	 {
		 if(! is_array($array)) return false;
		 $o = array();
		 
		 foreach($array as $k => $a)
		 {
			 if(is_array($a))
			 {
				foreach($a as $kk => $aa)
				{
					$o[$k.".".$kk] = $aa;  
				}				 
			 }
			 else
			 {
				$o[$k] = $a; 
			 }
		 }
		 return $o;
	 }
	 
	 public static function Sksort(&$array, $subkey="id", $sort_ascending=false) 
	 {
		
			 if (count($array))
				 $temp_array[key($array)] = array_shift($array);
		
			 foreach($array as $key => $val){
				 $offset = 0;
				 $found = false;
				 foreach($temp_array as $tmp_key => $tmp_val)
				 {
					 if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
					 {
						 $temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
													 array($key => $val),
													 array_slice($temp_array,$offset)
												   );
						 $found = true;
					 }
					 $offset++;
				 }
				 if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
			 }
		
			 if ($sort_ascending) $array = array_reverse($temp_array);
		
			 else $array = $temp_array;
		 }

}



?>