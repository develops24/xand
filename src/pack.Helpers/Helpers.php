<?php

class Helpers
{

    public function __construct() {

    }

    /**
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    public function simple($a, $b) {
        return $a + $b;
    }

    /**
     *
     * @param int $R1
     * @param int $G1
     * @param int $B1
     * @param int $R2
     * @param int $G2
     * @param int $B2
     * @return type
     */
    public static function lumdiff($R1, $G1, $B1, $R2, $G2, $B2) {
        $L1 = 0.2126 * pow($R1 / 255, 2.2) +
            0.7152 * pow($G1 / 255, 2.2) +
            0.0722 * pow($B1 / 255, 2.2);

        $L2 = 0.2126 * pow($R2 / 255, 2.2) +
            0.7152 * pow($G2 / 255, 2.2) +
            0.0722 * pow($B2 / 255, 2.2);

        if ($L1 > $L2) {
            return ($L1 + 0.05) / ($L2 + 0.05);
        } else {
            return ($L2 + 0.05) / ($L1 + 0.05);
        }
    }

    /**
     *
     * @param type $R1
     * @param type $G1
     * @param type $B1
     * @param type $R2
     * @param type $G2
     * @param type $B2
     * @return type
     */
    public static function coldiff($R1, $G1, $B1, $R2, $G2, $B2) {
        return max($R1, $R2) - min($R1, $R2) +
        max($G1, $G2) - min($G1, $G2) +
        max($B1, $B2) - min($B1, $B2);
    }

    /**
     *
     * @param string $hex
     * @return array
     */
    public static function getContrastFrom($hex) {
        $rgb = self::hex2rgb($hex);

        $done = false;
        for ($i = 0; $i < 1000; $i++) {
            $rgb2 = array(mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            $lummDiff = self::lumdiff($rgb[0], $rgb[1], $rgb[2], $rgb2[0], $rgb2[0], $rgb2[0]);
            if ($lummDiff > 15) {
                $done = true;
                break;
            }
        }
        if (!$done) {
            if (array_sum($rgb) > 382) {
                return "000";
            } else {
                return "fff";
            }
        }
        return dechex($rgb2[0]) . dechex($rgb2[1]) . dechex($rgb2[2]);
    }

    public static function hex2rgb($hex) {
        list($r, $g, $b) = sscanf(trim($hex, '#'), "%02x%02x%02x");
        return array($r, $g, $b);
    }

    /**
     *
     * @param string $hexcolor
     * @return string
     */
    public static function getContrastYIQ($hexcolor) {
        $r = hexdec(substr($hexcolor, 0, 2));
        $g = hexdec(substr($hexcolor, 2, 2));
        $b = hexdec(substr($hexcolor, 4, 2));
        $yiq = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;
        return ($yiq >= 128) ? 'black' : 'white';
    }

    /**
     * Вовзращает количество будней в заданном промежутке
     * @param int $from unix
     * @param int $to unix
     * @return int
     */
    public static function countWeekdays($from, $to) {
        if ($from >= $to) {
            return 0;
        }
        $count = 0;
        for ($t = $from; $t < $to; $t += 86400) {
            if (!in_array(date('D', $t), array('Sat', 'Sun'))) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Возвращает количество праздников в будние дни в заданном интервале
     * @param int $from unix
     * @param int $to unix
     * @return int
     */
    public static function countWeekdaysHolidays($from, $to) {
        if ($from >= $to) {
            return 0;
        }
        $hols = array();
        $h1 = array_values(self::getHolidays(date('Y', $from)));
        $h2 = array();
        if (date('Y', $to) != date('Y', $from)) {
            $h2 = array_values(self::getHolidays(date('Y', $to)));
        }
        foreach ($h1 as $h) {
            $hols[] = $h;
        }
        foreach ($h2 as $h) {
            $hols[] = $h;
        }
        $count = 0;
        foreach ($hols as $holiday) {

            if (!in_array(date('D', strtotime($holiday)), array('Sat', 'Sun')) && strtotime($holiday) >= $from && strtotime($holiday) <= $to) {
//                echo date("d.m.Y",$from)."-".date("d.m.Y",$to)."/".($holiday)."<br>";
                $count++;
            }
        }

        return $count;
    }

    /**
     * Возвращает количество праздничных дней в указанном промежутке
     * @param int $from unix
     * @param int $to unix
     * @return int
     */
    public static function countHolidays($from, $to) {
        if ($from >= $to) {
            return 0;
        }
        $hols = array();
        $h1 = array_values(self::getHolidays(date('Y', $from)));
        $h2 = array();
        if (date('Y', $to) != date('Y', $from)) {
            $h2 = array_values(self::getHolidays(date('Y', $to)));
        }
        foreach ($h1 as $h) {
            $hols[] = $h;
        }
        foreach ($h2 as $h) {
            $hols[] = $h;
        }
        $count = 0;
        foreach ($hols as $holiday) {
            if (strtotime($holiday) >= $from && strtotime($holiday) <= $to) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * День пасхи
     * @param type $Year
     * @return int
     */
    private static function easterDate($Year) {
        /*
          G is the Golden Number-1
          H is 23-Epact (modulo 30)
          I is the number of days from 21 March to the Paschal full moon
          J is the weekday for the Paschal full moon (0=Sunday,
          1=Monday, etc.)
          L is the number of days from 21 March to the Sunday on or before
          the Paschal full moon (a number between -6 and 28)
         */


        $G = $Year % 19;
        $C = (int)($Year / 100);
        $H = (int)($C - (int)($C / 4) - (int)((8 * $C + 13) / 25) + 19 * $G + 15) % 30;
        $I = (int)$H - (int)($H / 28) * (1 - (int)($H / 28) * (int)(29 / ($H + 1)) * ((int)(21 - $G) / 11));
        $J = ($Year + (int)($Year / 4) + $I + 2 - $C + (int)($C / 4)) % 7;
        $L = $I - $J;
        $m = 3 + (int)(($L + 40) / 44);
        $d = $L + 28 - 31 * ((int)($m / 4));
        $y = $Year;
        $E = mktime(0, 0, 0, $m, $d, $y);

        return $E;
    }

    /**
     * @param int $year
     * @return array Возвращает массив праздников
     */
    public static function getHolidays($year) {
        $holidays = array();


        date_default_timezone_set('Europe/Berlin');

//Neujahrstag  Mittwoch 01-янв-14  Donnerstag 01-янв-15  
        $holidays["Neujahrstag"] = date("Y-m-d", strtotime($year . "-01-01"));

//Karfreitag  Freitag 18-апр-14  Freitag 03-апр-15 
        $dTemp = DateTime::createFromFormat("U", self::easterDate($year));
        $holidays["Karfreitag"] = $dTemp->modify("-1 days")->format("Y-m-d");

//Ostermontag  Montag 21-апр-14  Montag 06-апр-15
        $dTemp = DateTime::createFromFormat("U", self::easterDate($year));
        $holidays["Ostermontag"] = $dTemp->modify("+2 days")->format("Y-m-d");

//1. Mai / Tag der Arbeit  Donnerstag 01-май-14  Freitag 01-май-15
        $holidays["1. Mai / Tag der Arbeit"] = date("Y-m-d", strtotime($year . "-05-01"));

//Christi Himmelfahrt  Donnerstag 29-май-14  Donnerstag 14-май-15
        $dTemp = DateTime::createFromFormat("U", self::easterDate($year));
        $holidays["Christi Himmelfahrt"] = $dTemp->modify("+40 days")->format("Y-m-d");

//Christi Himmelfahrt  Montag 09-июн-14  Montag 25-май-15
        $dTemp = DateTime::createFromFormat("U", self::easterDate($year));
        $holidays["Pfingstmontag"] = $dTemp->modify("+51 days")->format("Y-m-d");

//Fronleichnam  Donnerstag 19-июн-14  Donnerstag 04-июн-15
        $dTemp = DateTime::createFromFormat("U", self::easterDate($year));
        $holidays["Fronleichnam"] = $dTemp->modify("+61 days")->format("Y-m-d");

//Tag der Deutschen Einheit  Freitag 03-окт-14  Samstag 03-окт-15
        $holidays["Tag der Deutschen Einheit"] = date("Y-m-d", strtotime($year . "-10-03"));

//1. Weihnachtstag  Donnerstag 25-дек-14  Freitag 25-дек-15
        $holidays["1. Weihnachtstag"] = date("Y-m-d", strtotime($year . "-12-25"));

//2. Weihnachtstag  Freitag 26-дек-14  Samstag 26-дек-15
        $holidays["2. Weihnachtstag"] = date("Y-m-d", strtotime($year . "-12-26"));

        return $holidays;
    }

    /**
     * @see http://httpd.apache.org/docs/1.3/logs.html#combined
     * @example LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" combined
     * %h is the remote host (ie the client IP)
     * %l is the identity of the user determined by identd (not usually used since not reliable)
     * %u is the user name determined by HTTP authentication
     * %t is the time the server finished processing the request.
     * %r is the request line from the client. ("GET / HTTP/1.0")
     * %>s is the status code sent from the server to the client (200, 404 etc.)
     * %b is the size of the response to the client (in bytes)
     * Referer is the page that linked to this URL.
     * User-agent is the browser identification string.
     * @param string $file
     * @return array
     */
    public static function parseApacheLog($file) {
//        $file = '31.31.15.124 - - [05/Jun/2014:13:15:55 +0400] "GET /lib/pack.Jquery/jquery-ui-min.js HTTP/1.1" 304 0 "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.17"';
        $regex = '#^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) -[a-zA-Z0-9_ ]{0,}- \[\d{2}\/[a-zA_Z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} [-+0-9]{5}\] "(GET|POST|PUT|DELETE) ([^"\']+) HTTP\/\d{1}\.\d{1}" (\d{3}) ([0-9]+) "([^"]+)" "([^"*\']+)"#ism';
        preg_match($regex, $file, $res);
        if (!empty($res[1])) {
            $data = array(
                'ip' => $res[1],
                'type' => $res[2],
                'request_url' => $res[3],
                'http_code' => $res[4],
                'data_size' => $res[5],
                'request_uri' => $res[6],
                'ua' => $res[7]
            );
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     *
     * @param int $timeStart timestamp начало интервала
     * @param int $timeEnd timestamp конец интервала
     * @param int $limitStart timestamp начало временного отрезка, в котором надо подчитать время
     * @param int $limitEnd конец
     * @return int количество секунд
     */
    public static function countTimeInPeriod($timeStart, $timeEnd, $limitStart, $limitEnd) {
        if ($timeStart >= $timeEnd || $limitStart >= $limitEnd) {
            return 0;
        }
        if ($timeEnd < $limitStart || $timeStart > $limitEnd) {
            return 0;
        }

        if ($timeEnd <= $limitEnd && $timeStart >= $limitStart) {
            // |...^---^...|
            return $timeEnd - $timeStart;
        } elseif ($timeEnd <= $limitEnd) {
            //^--|--^..|
            return $timeEnd - $limitStart;
        } elseif ($timeStart >= $limitStart) {
            //|..^--|--^
            return $limitEnd - $timeStart;
        } elseif ($timeStart < $limitStart && $timeEnd > $limitEnd) {
            //^--|----|--^
            return $limitEnd - $limitStart;
        }

        return 0;
    }

    /**
     * @param string $ip
     * @param string $range
     * @return bool
     *
     */
    public static function isIpInSubnet($ip, $range) {
        if (strpos($range, '/') == false) {
            $range .= '/32';
        }
        // $range в IP/CIDR формате 127.0.0.1/24
        list($range, $netmask) = explode('/', $range, 2);
        $range_decimal = ip2long($range);
        $ip_decimal = ip2long($ip);
        $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
        $netmask_decimal = ~$wildcard_decimal;
        return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
    }

    /**
     * @return bool
     */
    public static function isLocal() {
        $s = (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "");
        $array = explode(".", $s);
        if (!Geo::isLocalhost()) {
            return false;
        }
        return end($array) == 'loc' ? true : false;
    }

}
