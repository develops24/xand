<?php

/**
 * Class File
 *
 * @тип     Class
 * @пакет    Helpers
 * @версия   4
 *
 *
 *  v2.0 - Исправлен метод ConvertFileSize
 *  v3.0 - Добавлен метод GetUploadFolder
 *  v4.0 - Исправлен метод GetUploadPath
 *
 **/
class File
{

    //------
    //
    // Функция конвертирует ассоциативный массив в объект SimpleXMLElement
    // $array - исходный ассоциативный массив
    // $xml - ссылка на объект SimpleXMLElement
    //
    public static function ArrayToXmlFile($array, $file_path, $xml_tag)
    {
        $xml = new SimpleXMLElement("<" . $xml_tag . "/>");
        self::ArrayToSimpleXMLElement($array, $xml);//ковертируем массив
        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;//применяем форматирование
        file_put_contents($file_path, $dom->saveXML());
    }

    //------
    //
    // Функция сохраняет ассициативный массив в XML файл
    // $array - исходный ассоциативный массив
    // $file_path - полный путь к файлу
    // $xml_tag - имя главного XML тега
    //

    public static function ArrayToSimpleXMLElement($array, &$xml)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml->addChild("$key");
                    self::ArrayToSimpleXMLElement($value, $subnode);
                } else {
                    self::ArrayToSimpleXMLElement($value, $xml);
                }
            } else {
                $xml->addChild("$key", "$value");
            }
        }
    }

    //------

    public static function XmlArray($xmlObject, $out = array())
    {
        foreach ((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? self::XmlArray($node) : $node;

        return $out;
    }

    //------
    public static function XmlReplace($path, $key, $value)
    {
        if (file_exists($path)) {
            $xml = simplexml_load_file($path);
            $xml->$key = $value;
            $xml->asXML($path);
        } else
            Message::I()->Error("WriteOption. Невозможно открыть xml файл - " . $path)->E();
    }

    //------
    public static function GetUploadFolder($path)
    {
        if (!file_exists($path . '/config.xml'))
            Message::I()->Error("Config uploder file '{$path}/config.xml' does not exits!")->Draw()->E();

        $xml = simplexml_load_file($path . '/config.xml');

        $xml->current_counter++;
        if ($xml->current_counter > 99) {
            $xml->current_counter = 0;
            $xml->current_folder++;
        }
        $xml->asXML($path . '/config.xml');

        if (!is_dir($path . '/' . $xml->current_folder)) {
            mkdir($path . '/' . $xml->current_folder, 0777);
        }

        return $xml->current_folder;
    }

    //------
    public static function GetUploadPath($path)
    {
        if (!file_exists($path . '/config.xml'))
            Message::I()->Error("Config uploder file '{$path}/config.xml' does not exits!")->Draw()->E();

        $xml = simplexml_load_file($path . '/config.xml');

        $counter = ((int)$xml->current_counter) + 1;
        $xml->current_counter = $counter;

        if ($counter > 99) {
            $xml->current_counter = 0;
            $folder = ((int)$xml->current_folder) + 1;
            $xml->current_folder = $folder;
        }

        $xml->asXML($path . '/config.xml');

        if (!is_dir($path . '/' . $xml->current_folder)) {
            mkdir($path . '/' . $xml->current_folder, 0777);
        }

        $folder = $xml->current_folder . DS . $xml->current_counter;
        return $folder;
    }

    //------
    public static function WriteArray($aray, $namefile)
    {
        echo Config::$PATH_REAL . Config::PATH_STAT . $namefile;
        $str = serialize($aray);
        if (file_put_contents(Config::$PATH_REAL . Config::PATH_STAT . $namefile, $str) > 0)
            return true;
        else
            return false;
    }

    //------
    public static function ReadArray($namefile)
    {
        $str = file_get_contents(Config::$PATH_REAL . Config::PATH_STAT . $namefile);
        $aray = unserialize($str);
        return $aray;
    }

    //------

    public static function GetFileSize($path)
    {
        $file_size = filesize($path);

        return self::ConvertFileSize($file_size);
    }

    //------

    public static function ConvertFileSize($size)
    {
        $file_size = $size;

        if ($file_size > 1099511627776) $file_size = round($file_size / 1099511627776) . ' Tb';
        if ($file_size > 1073741824) $file_size = round($file_size / 1073741824) . ' Gb';
        if ($file_size > 1048576) $file_size = round($file_size / 1048576) . ' Mb';
        if ($file_size > 1024) $file_size = round($file_size / 1024) . ' Kb';

        return $file_size;
    }

}