<?php

/**
 * Class Breadcrumbs
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Breadcrumbs extends ClassBase
{
    public $crumbs = array();
    public $name = "breadcrumbs";

    protected $table;
    protected $primary;
    protected $fieldPath;
    protected $fieldTitle;

    /**
     * Breadcrumbs constructor.
     *
     * @param $table
     * @param $primary
     * @param string $fieldTitle
     * @param string $fieldPath
     */
    function __construct($table, $primary, $fieldTitle = "title", $fieldPath = "path")
    {
        $this->table = $table;
        $this->primary = $primary;
        $this->fieldTitle = $fieldTitle;
        $this->fieldPath = $fieldPath;
    }

    /**
     * @param $url
     * @param $title
     */
    public function Add($url, $title)
    {
        array_push(
            $this->crumbs,
            array(
                "url" => $url,
                "title" => $title,
            )
        );
    }

    /**
     * @param $current
     */
    public function Calculate($current)
    {
        //$path = DbOnce::Field($this->table, $this->fieldPath, "{$this->primary}={$current}");
        $path = implode(SDbQuery::table($this->table)->where([$this->primary => $current])->selectRow([$this->fieldPath]));
        $pathExp = explode("/", $path);

        foreach ($pathExp as $p) {
            if ($p == "") continue;

            //$title = DbOnce::Field($this->table, $this->fieldTitle, "{$this->primary}={$p}");
            $title = implode(SDbQuery::table($this->table)->where([$this->primary => $p])->selectRow([$this->fieldTitle]));
            $this->Add($p, $title);
        }

    }


}
