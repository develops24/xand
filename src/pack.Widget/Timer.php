<?php
 
class Timer extends ClassBase
{
	public $interval;
	public $name = "timer";
	
	//---
	function __construct($interval = 600000)
	{
		$this->interval = $interval;
		// $this->interval = 5000;
	}
	
    //--- 
    public function Draw() 
	{
		 $secs = $this->interval / 1000;
		 $secs+=2;
		 
		 echo "<div class='appTimerWrap'>
		 			<span>Refresh in: </span>
		 			<span class='appTimer' id='zoneClock'></span>
					<span class='appTimer' id='zoneClockHelper'></span>
					<span class='appTimerClock' style='display:none;' id='zoneTimer'>{$secs}</span>
			  </div> 
			  <script>TimerStart({$this->parent->ajax->eid}, {$secs});</script>";
    }

    //-----
}

