<?php

/**
 *
 **/
class RequestParams extends ClassBase
{
    /**
     * @var array
     */
    private $contentDecoderRules = array(
        "#AM#" => "&",
        "#OK#" => "'",
        "#DK#" => '"',
        "#PL#" => "+",
        "#SL#" => "\\",
        "#SLO#" => "/",
        "#BR#" => "\n"
    );

    /**
     * @return array
     */
    public function getParams()
    {
        $get = filter_input_array(1);
        if ($get) {
            array_shift($get);//удаляем q из массива
        }
        return (!$get) ? array() : $get;
    }

    /**
     * @param $key
     *
     * @return null|mixed
     */
    public function getParam($key)
    {
        $params = $this->getParams();
        if (isset($params[$key])) {
            return $params[$key];
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function GetPostPath()
    {
        return trim(((isset($_POST['PATH'])) ? $_POST['PATH'] : ""));
    }

    /**
     * @return string
     */
    public function getPostAction()
    {
        return trim(((isset($_POST['ACT'])) ? preg_replace("/[^a-zA-Z]+/ui", "", $_POST['ACT']) : ""));
    }

    /**
     * @return string
     */
    public function getPostData()
    {
        return trim(((isset($_POST['DATA'])) ? $_POST['DATA'] : ""));
    }

    /**
     * @return string
     */
    public function getPostDataInt()
    {
        return trim(((isset($_POST['DATA'])) ? preg_replace("/[^0-9]+/ui", "", $_POST['DATA']) : ""));
    }

    /**
     * @param $array
     *
     * @return array|string
     */
    private static function _StripSlashesArray($array)
    {
        return is_array($array) ? array_map('RequestParams::_StripSlashesArray', $array) : stripslashes($array);
    }

    /**
     * @return array
     */
    public function getPostDataJson()
    {

        $input = str_replace("^", "%", $this->getPostData());

        $data = json_decode($input);
        $ARR = array();
        if (!$data) {
            return [];
        }

        foreach ($data as $key => $val) {
            $val = urldecode($val);
            $ARR[$key] = strtr($val, $this->contentDecoderRules);
        }
        return $ARR;
    }

}