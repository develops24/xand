<?php

class WidgetDummy
{
    public $objects = array();

    function __construct() {
        $args = func_get_args();

        //----- обязательные
        $this->uid = "cnst_" . $args[0];

        if (!$args[1] instanceof Ajax) exit("Не обнаружен обязательный обьект Ajax");
        $this->ajax = $args[1];

        $this->ajax->setExemplarId($this->uid);
        $this->ajax->setZone($this->uid);
        $this->ajax->createJsExemplar();

        //----- необязательные

        $this->nav = (isset($args[2])) ? $args[2] : new Nav();
        $this->nav->setUid($this->uid);

    }

}