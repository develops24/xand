<?php

/**
 * Class WidgetCrud
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class WidgetCrud extends Widget
{
    const DATE_UPDATE = "dateUpdate";
    const DATE_CREATE = "dateCreate";


    /**
     *
     */
    public function actionAddReady()
    {
        $form = new FormModal("Add form");
        $form->assignParent($this)->cloneAjax()->cloneNav();

        if (method_exists($this, "getCrudFormComponents"))
            $form = $this->getCrudFormComponents($form);

        $form->Add(
            new ButtonAdd(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit;
    }


    /**
     *
     */
    public function actionUpdateReady()
    {
        $data = SDbQuery::table($this->tableName)
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->select();

        $form = new FormModal("Edit form", $data);
        $form->assignParent($this)->cloneAjax()->cloneNav();

        if (method_exists($this, "getCrudFormComponents"))
            $form = $this->getCrudFormComponents($form);

        $form->Add(
            new InputVar($this->primaryKey, $this->primaryKey),
            new ButtonSave(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit;
    }


    /**
     * @param array $data
     *
     * @return array
     */
    protected function preProcessAdd(array $data = [])
    {
        return $data;
    }


    /**
     *
     */
    public function actionAddFinish()
    {
        $data = $this->getPostDataJson();
        $data[self::DATE_CREATE] = time();
        $data[self::DATE_UPDATE] = time();

        if (method_exists($this, "preProcessAdd"))
            $data = $this->preProcessAdd($data);

        if (empty($data)) {
            Message::I()->Error("An error occurred");
        } else {
            SDbQuery::table($this->tableName)
                ->enableMessages()
                ->insert($data);
        }

    }


    /**
     * @param array $data
     *
     * @return array
     */
    protected function preProcessUpdate(array $data = [])
    {
        return $data;
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();
        $data[self::DATE_UPDATE] = time();

        if (method_exists($this, "preProcessUpdate"))
            $data = $this->preProcessUpdate($data);

        if (empty($data)) {
            Message::I()->Error("An error occurred");
        } else {
            SDbQuery::table($this->tableName)
                ->enableMessages()
                ->where([$this->primaryKey => $data[$this->primaryKey]])
                ->update($data);
        }

    }


    /**
     *
     */
    public function actionPublished()
    {
        $data = $this->extractMassID();

        $counter = 0;
        if (!empty($data)) {
            foreach ($data as $d) {

                if (!is_numeric($d)) continue;

                SDbQuery::table($this->tableName)
                    ->where([$this->primaryKey => $d])
                    ->update([
                        "isActive" => 1,
                        "dateUpdate" => time()
                    ]);

                if (SDbBase::getLastError()) {
                    Message::I()->Error(SDbBase::getLastError());
                    return;
                }

                $counter++;
            }

            $text = I18n::__("Selected products successfully updated");
            Message::I()->resetPostProcess()->Success("{$counter} {$text}");

        } else {
            Message::I()->Warning("Not selected any rows to update");
        }
    }

    /**
     *
     */
    public function actionUnpublished()
    {
        $data = $this->extractMassID();

        $counter = 0;
        if (!empty($data)) {
            foreach ($data as $d) {

                if (!is_numeric($d)) continue;

                SDbQuery::table($this->tableName)
                    ->where([$this->primaryKey => $d])
                    ->update([
                        "isActive" => 0,
                        "dateUpdate" => time()
                    ]);

                if (SDbBase::getLastError()) {
                    Message::I()->Error(SDbBase::getLastError());
                    return;
                }

                $counter++;
            }

            $text = I18n::__("Selected products successfully updated");
            Message::I()->resetPostProcess()->Success("{$counter} {$text}");

        } else {
            Message::I()->Warning("Not selected any rows to update");
        }
    }

    /**
     *
     */
    public function actionUnarchived()
    {
        $data = $this->extractMassID();

        $counter = 0;
        if (!empty($data)) {
            foreach ($data as $d) {

                if (!is_numeric($d)) continue;

                SDbQuery::table($this->tableName)
                    ->where([$this->primaryKey => $d])
                    ->update([
                        "archived" => 0,
                        "dateUpdate" => time()
                    ]);

                if (SDbBase::getLastError()) {
                    Message::I()->Error(SDbBase::getLastError());
                    return;
                }

                $counter++;
            }

            $text = I18n::__("Selected products successfully updated");
            Message::I()->resetPostProcess()->Success("{$counter} {$text}");

        } else {
            Message::I()->Warning("Not selected any rows to update");
        }

    }

    /**
     *
     */
    public function actionArchived()
    {
        $data = $this->extractMassID();
        $counter = 0;
        if (!empty($data)) {
            foreach ($data as $d) {

                if (!is_numeric($d)) continue;

                SDbQuery::table($this->tableName)
                    ->where([$this->primaryKey => $d])
                    ->update([
                        "archived" => 1,
                        "dateUpdate" => time()
                    ]);

                if (SDbBase::getLastError()) {
                    Message::I()->Error(SDbBase::getLastError());
                    return;
                }

                $counter++;
            }

            $text = I18n::__("Selected products successfully updated");
            Message::I()->resetPostProcess()->Success("{$counter} {$text}");

        } else {
            Message::I()->Warning("Not selected any rows to update");
        }
    }

    /**
     *
     */
    public function actionDelete()
    {
        $data = $this->extractMassID();
        $counter = 0;
        if (!empty($data)) {
            foreach ($data as $d) {

                if (!is_numeric($d)) continue;

                SDbQuery::table($this->tableName)
                    ->where([$this->primaryKey => $d])
                    ->delete();

                if (SDbBase::getLastError()) {
                    Message::I()->Error(SDbBase::getLastError());
                    return;
                }

                $counter++;
            }

            $text = I18n::__("rows successfully removed");
            Message::I()->resetPostProcess()->Success("{$counter} {$text}");

        } else {
            Message::I()->Warning("Not selected any rows to delete");
        }
    }


}