<?php

/**
 * Class Nav
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Nav
{

    public $uid;

    //-   variable

    public $path = array();
    public $param = array();

    private $freeze = array();

    /**
     * Nav constructor.
     */
    function __construct()
    {
    }

    /**
     * @param $uid
     *
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     *
     */
    public function freeze()
    {
        $this->freeze['path'] = $this->path;
        $this->freeze['param'] = $this->param;
    }

    /**
     *
     */
    public function unFreeze()
    {
        $this->path = $this->freeze['path'];
        $this->param = $this->freeze['param'];
    }

    /**
     * @param bool $inputArg
     *
     * @return $this
     */
    public function parse($inputArg = false)
    {

        if ($inputArg) {
            $input = $inputArg;
        } else {
            $input = urldecode($_SERVER['REQUEST_URI']);
            $input = (PATH_SITE != "") ? substr($input, strlen(PATH_SITE) + 2, strlen($input))
                : substr($input, 1, strlen($input));

        }

        if (PATH_SITE != "") $input = str_replace(DS . PATH_SITE . DS, "", $input);

        $parseUrl = parse_url($input);
        $path = array();
        if (isset($parseUrl["query"])) parse_str($parseUrl["query"], $this->param);
        else                           $this->param = array();

        if (!isset($parseUrl["path"]) || $parseUrl["path"] == "/") {
            $this->path = array();
        } else {
            $path = trim($parseUrl["path"], "/");
        }


        $this->path = ($path) ? explode("/", $path) : array();

        return $this;

    }

    /**
     * @param array $include
     * @param array $exclude
     *
     * @return string
     */
    public function toString(array $include = [], array $exclude = [])
    {

        if (!empty($include)) {
            $this->freeze();
            $this->param = array_merge($this->param, $include);
        }

        if (!empty($exclude)) {
            foreach ($exclude as $ex) {
                unset($this->param[$ex]);
            }
        }

        $param = (!empty($this->param)) ? "?" . http_build_query($this->param) : "";

        if (!empty($include) || !empty($exclude)) {
            $this->unFreeze();
        }

        return implode("/", $this->path) . $param;
    }


}
