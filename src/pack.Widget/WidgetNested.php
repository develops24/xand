<?php

/**
 *
 *   Class WidgetNested
 *   для многоуровневой структуру с i18n
 *
 * */
class WidgetNested extends Widget
{

    /**
     * Парсинг введённых данных
     * @param $data - введённые данные
     * @return array
     */
    public function parseI18nFields($data) {

        $arr_data = array();
        $arr_data_i18n = array();

        foreach ($data as $k => $a) {
            // Проверяем, поле обычное или перевод
            if (strpos($k, "translate") !== false) {
                // Поле для перевода имеет следующий шаблон имени - translate_{lang}_{field_name}
                $ar_field_name = explode("_", $k);
                $arr_data_i18n[$ar_field_name[1]][$ar_field_name[2]] = $a;
            } else {
                $arr_data[$k] = $a;
                // Дополнительная запись для дефолтного языка
                $arr_data_i18n[SITE_DEFAULT_LANG][$k] = $a;
            }
        }

        return array(
            $arr_data,
            $arr_data_i18n
        );
    }


    /**
     * Получение параметров родителя по параметру path
     * @return array - параметры родительского блока
     */
    public function GetParentObject() {
        $arr_parent = (isset($this->nav->param["path"]) && $this->nav->param["path"] != 0)
            ? $this->model->findById($this->nav->param['path']) : array();

        return $arr_parent;
    }

    /**
     * Разбиение url страницы на части - родительский url и url элемента
     * @param $full_url - полный url
     * @return array - ассоциативный массив,
     * 'url' - url самого элемента,
     * 'parentUrl' - url родительского блока
     */
    public function parseItemUrl($full_url) {
        $result = array();
        // Разбиваем 'URL' из БД на url страницы и родительский url
        $url_array = explode('/', trim($full_url, '/'));
        // Последний элемент - это url самого элемента
        $result['url'] = array_pop($url_array);
        // Соединяем оставшиеся части в родительский url
        $result['parentUrl'] = implode('/', $url_array);
        return $result;
    }

}