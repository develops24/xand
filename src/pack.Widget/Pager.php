<?php

class Pager extends ClassBase
{
    public $name = "pager";
    public $countRowsOnPage;
    public $cssClass;
    public $tpl;
    public $countRows;
    public $currentPage;
    public $data = array();
    public $mongoMode = false;

    private $manualMode;

    function __construct()
    {
        $arr = func_get_args();
        
        $this->countRows = 0;
        $this->tpl = (isset($arr[0])) ? $arr[0] : false;
        $this->countRowsOnPage = (isset($arr[1])) ? $arr[1] : false;
        $this->cssClass = (isset($arr[2])) ? $arr[2] : "pagerWrap";
        $this->manualMode = false;

        $this->setProperty("class", "pagerWrap");

    }

    //-----
    public function Skip($countRows)
    {
        if ($countRows == 0) return;

        $this->countRows = $countRows;

        $ppage = $this->parent->ppage->current;
        $countPages = (int)ceil($countRows / $ppage);
        $currentPage = ($this->parent->nav->param['page']) ? $this->parent->nav->param['page'] : 1;

        $this->currentPage = $currentPage;

        if ($this->parent->nav->param['ppage'] == $this->parent->ppage->format[0]) $this->parent->nav->param['ppage'] = false;

        $this->parent->nav->freeze();
        $this->parent->nav->param['page'] = false;
        array_push($this->data, array($this->parent->nav->toString(), "«", false));

        for ($i = $currentPage - 5; $i <= $currentPage + 5; $i++) {
            if ($i < 1 || $i > $countPages) continue;
            $this->parent->nav->param['page'] = ($i == 1) ? false : $i;
            array_push($this->data, array($this->parent->nav->toString(), $i, ($currentPage == $i) ? true : false));
        }

        $this->parent->nav->param['page'] = $countPages;
        array_push($this->data, array($this->parent->nav->toString(), "»", false));
        $this->parent->nav->unFreeze();

        $this->mongoMode = true;
        return $ppage * $currentPage - $ppage;

    }

    //-----
    public function Calculate($countRows)
    {
        if ($countRows == 0) return;

        if (!$this->countRowsOnPage) {
            $this->countRowsOnPage = (isset($this->parent->nav->param['ppage']) && $this->parent->nav->param['ppage'] > 0)
                ? $this->parent->nav->param['ppage']
                : $this->parent->ppage->format[0];
        }

        $this->currentPage = (!isset($this->parent->nav->param['page']))
            ? 1 : $this->parent->nav->param['page'];

        $a = $this->countRowsOnPage * $this->currentPage - $this->countRowsOnPage;
        $b = $this->countRowsOnPage;

        if ($this->countRowsOnPage == 0)
            $this->countRowsOnPage = 25;

        $countPages = ceil($countRows / $this->countRowsOnPage);

        $currentPageTemp = $this->currentPage;
        $path = PATH_SITE . "/";

        if ($this->currentPage < $countPages) $currentPageTemp++;
        else                           $currentPageTemp = $countPages;

        $this->countRows = $countRows;


        $azl = $this->parent->ajax->zone . ".Link";

        if (isset($this->parent->ajax)) {

            if (isset($_SESSION[AUTH_KEY])) {

                if ($countPages > 1)
                    array_push($this->data, array("{$azl}({'page' : 1});", "<i class='fa fa-angle-double-left'></i>", "arrow_left"));

                if ($this->currentPage == 1)
                    array_push($this->data, array("{$azl}({'page' : 1});", "1", "current"));

                else
                    array_push($this->data, array("{$azl}({'page' : 1});", "1", "all"));

                if ($countPages >= 5 && $this->currentPage >= 5) {
                    array_push($this->data, array("", "...", "points"));
                    $this->manualMode = true;
                }


                $limit_min = $this->currentPage - 2;
                if ($limit_min < 2)
                    $limit_min = 2;
                $limit_max = $this->currentPage + 3;
                if ($limit_max > $countPages)
                    $limit_max = $countPages;

                //for($i=1; $i <= $countPages; $i++)
                for ($i = $limit_min; $i < $limit_max; $i++) {
                    if ($this->currentPage == $i)
                        array_push($this->data, array(false, $i, "current"));

                    else
                        array_push($this->data, array("{$azl}({'page' : {$i}});", $i, "all"));
                }


                $currentPageTemp = $this->currentPage;
                if ($this->currentPage > 1) $currentPageTemp--;
                else                  $currentPageTemp = 1;

                if ($countPages >= 5 && $this->currentPage <= $countPages - 4) {
                    array_push($this->data, array("", "...", "points"));
                    $this->manualMode = true;
                }

                if ($countPages > 1) {
                    if ($this->currentPage == $countPages)
                        array_push($this->data, array("{$azl}({'page' : {$countPages}});",
                            intval($countPages), "current"));

                    else
                        array_push($this->data, array("{$azl}({'page' : {$countPages}});",
                            intval($countPages), "all"));//последняя страница

                    array_push($this->data, array("{$azl}({'page' : {$countPages}});",
                        "<i class='fa fa-angle-double-right'></i>", "arrow_right"));
                }


            } else {

                if ($countPages > 1) {
                    $sPage = $this->currentPage - 1;

                    if ($sPage < 1)
                        $sPage = 1;

                    array_push($this->data,
                        array("{$azl}({'page' : {$sPage}});",
                            "<i class='fa fa-angle-double-left'></i>", "arrow_left"));
                }

                array_push($this->data, array(false, $this->currentPage, "current"));

                if ($countPages >= 5 && $this->currentPage <= $countPages - 4) {
                    $this->manualMode = true;
                }

                if ($countPages >= 5 && $this->currentPage >= 5) {
                    $this->manualMode = true;
                }

                if ($countPages > 1) {
                    $sPage = $this->currentPage + 1;

                    if ($sPage > $countPages)
                        $sPage = $countPages;

                    array_push($this->data,
                        array("{$azl}({'page' : {$sPage});",
                            "<i class='fa fa-angle-double-right'></i>", "arrow_left"));
                }


            }


        }


        return $a . "," . $b;
    }

    //-----
    public function Draw()
    {
        if ($this->tpl) {
            if (!empty($this->data)) {
                $this->tpl->assignArray($this->data);
                $this->tpl->Load();
            }
        } else {

            if ($this->mongoMode) {
                echo "<div class='{$this->properties['class']}'>";
                if ($this->parent->ajax)
                    foreach ($this->data as $data)
                        echo ($data[2]) ? "<p>{$data[1]}</p>" : $this->parent->Link($this->parent->ajax->zone . ".Go('" . $data[0] . "');", $data[1]);

                else
                    foreach ($this->data as $data)
                        echo ($data[2]) ? "<p>{$data[1]}</p>" : "<a href='" . PATH_DS . "{$data[0]}'>{$data[1]}</a>";

                echo "</div>";
            } else {
                echo "<div class='{$this->cssClass}'>";
                if ($this->parent->ajax) {
                    foreach ($this->data as $data) {

                        if ($data[2] == "current") {
                            if ($this->manualMode) {
                                ?>
                                <input type="text"
                                       value="<?= $this->currentPage; ?>"
                                       onkeyup="PagerPageManualEvent(this, <?= $this->parent->ajax->zone; ?>);"/>
                                <?php
                            } else {
                                echo "<p>{$data[1]}</p>";
                            }

                        } else {
                            echo $this->parent->Link($data[0], $data[1]);

                        }


                    }
                } else {
                    foreach ($this->data as $data) {

                        if ($data[2] != "current") echo "<a href='" . PATH_DS . "{$data[0]}'>{$data[1]}</a>";
                        else                       echo "<p>{$data[1]}</p>";
                    }
                }

                echo "</div>";
            }
        }
    }


} 