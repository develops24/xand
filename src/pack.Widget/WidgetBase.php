<?php

/**
 * Class WidgetBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class WidgetBase extends RequestParams
{

    //-------- obj
    public $ajax;
    public $nav;
    public $stateRestore;

    public $query;
    public $table;
    public $form;
    public $pager;
    public $ppage;
    public $breadcrumbs;
    public $timer;

    public $actions = [];
    public $tabs = [];
    public $buttons = [];
    public $rules = [];

    //-------- vars
    public $uid;
    public $data;

    //-------- etc
    protected $type = 'Table';
    protected $eventHandlers = [];

    //-------- helpers
    protected $widgetName = '';
    protected $tableName = '';
    protected $primaryKey = '';


    /**
     * @param $field
     * @param string $desc
     * @param bool $escape
     *
     * @return $this
     */
    public function SetDefaultOrder($field, $desc = 'ASC', $escape = true)
    {
        if (!isset($this->nav->param['order'])) {
            $this->nav->param['order'] = $field;
            $this->nav->param['desc'] = $desc;
        }
        $this->nav->param['escape_order'] = $escape;
        return $this;
    }

    /**
     * @return $this
     */
    public function Add()
    {
        $args = func_get_args();

        foreach ($args as &$object) {

            $object->assignParent($this);


            if ($object instanceof IObjectsArray) {
                $this->{$object->name}[] = $object;
            } elseif ($object instanceof ActionBase) {
                $this->{$object->name}[$object->properties["field"]] = $object;
            } else {
                $this->{$object->name} = $object;
            }

            if (in_array($object->name, array("tabs", "actions"))) {
                array_push($this->rules, $object->rule);
                if ($this->query instanceof QueryBase) {
                    $object->rule->provideHolders = true;
                }

            }

        }
        return $this;
    }

    /**
     * @return array
     */
    public function extractCriteria()
    {
        $criteriaArr = [];

        $actions = array_merge($this->actions, $this->tabs);

        foreach ($actions as $action) {
            foreach ($this->nav->param as $key_filters => $filters) {
                if ($key_filters == $action->rule->field) {
                    $criteriaArr[] = $action->rule->GetRule($filters);
                }
            }
        }


        return $criteriaArr;

    }


}