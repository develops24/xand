<?php

/**
 * Class Ajax
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Ajax extends ClassBase
{
    public $name = "ajax";
    public $controller;
    public $zone;
    public $hash_set;
    public $method;
    public $eid;

    /**
     * Ajax constructor.
     */
    function __construct()
    {
        $arr = func_get_args();

        $this->controller = (isset($arr[0])) ? $arr[0] : NULL;
        $this->hash_set = (isset($arr[1])) ? ($arr[1]) ? "true" : "false" : "true";
        $this->method = (isset($arr[2])) ? $arr[2] : "html";

        if (isset($arr[3]))
            $this->zone = $arr[3];
    }

    /**
     * @param $eid
     *
     * @return $this
     */
    public function setExemplarId($eid)
    {
        $this->eid = $eid;
        return $this;
    }

    /**
     * @param $zone
     *
     * @return $this
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * @param $file
     * @param $zone
     */
    public static function Load($file, $zone)
    {
        ?>
        <div id='<?= $zone ?>'>Loading...</div>

        <script>
            $.ajax({
                type: 'POST',
                cache: false,
                url: '<?= $file ?>',
                dataType: 'html',
                success: function (html) {
                    $('#<?= $zone ?>').html(html);
                }

            });
        </script>
        <?php
    }

    /**
     * @param $file
     * @param $zone
     */
    public static function Reload($file, $zone)
    {
        $script = "<script>"
            . "if($('#{$zone}').length===1){"
            . "$.ajax({  
				 type:	'POST',  
				 cache:	 false,
				 url: 	 '{$file}', 				 
				 dataType: 'html',	   						       
				 success: function(html)
				 { 	
				 	$('#{$zone}').html(html);	 
				 }
				 
				 });"
            . "}"
            . "</script>";
        echo $script;
    }

    /**
     * @return $this
     */
    public function createJsExemplar()
    {

        if (!$this->eid)
            Message::I()->Error("Can not create an instance of the class js Ajax, unknown EID")->E();
        ?>

        <script>
            <?= $this->eid ?> = new Request('<?= $this->controller ?>', '<?= $this->zone ?>', <?= $this->hash_set ?>, '<?= $this->method ?>');
        </script>

        <?php
        return $this;
    }

    /**
     * @param $zone
     *
     * @return $this
     */
    public function setJsZone($zone)
    {
        echo "<script>{$this->eid}.Zone = '{$zone}';</script>";
        return $this;
    }


    /**
     * @param $type
     *
     * @return $this
     */
    public function setNavigation($type)
    {
        echo "<script>{$this->eid}.Navigation = '{$type}';</script>";
        return $this;
    }

    /**
     * @return $this
     */
    public function zoneJsRefresh()
    {
        $args = func_get_args();
        $arg = (isset($args[0])) ? $args[0] : "";
        echo "<script>{$this->eid}.Refresh({$arg});</script>";
        return $this;
    }

    /**
     * @param $act
     * @param $data
     * @param $rcv
     *
     * @return $this
     */
    public function JsAct($act, $data, $rcv)
    {
        //Act = function(Action,Data, Path)			
        echo "<script>{$this->eid}.Act('{$act}','{$data}','{$rcv}');</script>";
        return $this;
    }

}