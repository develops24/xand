<?php

/**
 * Class Ppage
 *
 * @тип  	 Class
 * @package    Widget
 * @версия   4
 *
 *
 * v2 - Добавлен метод для запоминание в сесии количество страниц
 * v3 - Переменная $wrapclass -> перенесена в массив свойств
 * v4 - Добавлена проверка countRows
 *
 **/

class Ppage extends ClassBase
{
	public $name = "ppage";
	public $format;
	public $tpl;
	public $data = array();
	public $current;

	//---
	function __construct()
	{
		$args = func_get_args();

		$this->format 		 = (isset($args[0]))  ? $args[0]  : array(25, 100, 500, 2500);
		$this->current 		 = $this->format[0];
		$this->tpl 			 = (isset($args[1]))  ? $args[1]  : false;
		$properties			 = (isset($args[2]))  ?	$args[2]  :	array();

		$this->setProperty("wrapClass","pagerWrap")
			->setProperty("memoryToSession", false);

		foreach($properties as $key => $val)
		{
			$this->properties[$key] = $val;
		}

	}

	//----
	public function SetCurrent()
	{
		$this->current = (int) (isset($this->parent->nav->param['ppage'])
			&& $this->parent->nav->param['ppage'] > 0)
			? (in_array($this->parent->nav->param['ppage'], $this->format))
				? $this->parent->nav->param['ppage'] : $this->format[0]
			: $this->format[0];


	}

	//----
	public function Draw()
	{

		if(isset($this->parent->pager->countRows) && $this->parent->pager->countRows == 0) return;

		$current_ppage = (isset($this->parent->nav->param['ppage']) && $this->parent->nav->param['ppage'] > 0)
			? $this->parent->nav->param['ppage']
			: $this->format[0];


		foreach($this->format as $item)
		{

			if($current_ppage == $item)

				array_push($this->data,
					array("{'page' : false, 'ppage' : {$item}}", $item, "current"));

			else
				array_push($this->data,
					array("{'page' : false, 'ppage' : {$item}}", $item, "all"));

		}

		//--------------------- отрисовка в шаблон
		if($this->tpl)
		{
			if($this->parent->ajax)
			{
				foreach($this->data as &$data)
				{
					if($data[2] != "current")   	 $data[0] = $this->parent->ajax->zone.".Link(".$data[0].");";
					else 							 $data[0] = "<p>{$data[1]}</p>";
				}
			}

			if(! empty($this->parent->pager->data))
			{
				$this->tpl->assignArray($this->data);
				$this->tpl->Load();
			}
		}

		//---  отрисовка в встроенный шаблон
		else
		{
			echo "<div class='".$this->getProperty("wrapClass")."'>";

			if($this->parent->ajax)
			{
				foreach($this->data as $data)
				{
					if($data[2] != "current")  echo $this->parent->Link($this->parent->ajax->zone.".Link(".$data[0].");", $data[1]);
					else 						 echo "<p>{$data[1]}</p>";
				}
			}
			else
			{
				foreach($this->data as $data)
				{
					if($data[2] != "current")  echo "<a href='".PATH_DS."{$data[0]}'>{$data[1]}</a>";
					else 						echo "<p>{$data[1]}</p>";
				}
			}

			echo "</div>";
		}

	}

}