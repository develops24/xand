<?php

/**
 * Class FFAjax
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFAjax extends FFBase
{

    protected $options = [];

    /**
     * @return string
     */
    protected function RenderScripts()
    {
        $success = (isset($this->callbacks['success'])) ? $this->callbacks['success'] : 'null';
        $err = (isset($this->callbacks['error'])) ? $this->callbacks['error'] : 'null';

        if ($this->getOption('ajax_loaded') === true) {
            $html = "
            <script>
            (function(event) {
                    $('#{$this->id}').AjaxForm({
                     'dataType' : '{$this->dataType}',
                     'validate' : '{$this->validateJS}',
                     'tooltips' : " . json_encode($this->tooltips) . ",
                     'callbackSuccess': {$success},
                     'callbackError':  {$err}
                     });
         })();
            </script>
            ";
        } else {
            $html = "
        <script>
        document.addEventListener('DOMContentLoaded', function(event) {
                    $('#{$this->id}').AjaxForm({
                     'dataType' : '{$this->dataType}',
                     'validate' : '{$this->validateJS}',
                     'tooltips' : " . json_encode($this->tooltips) . ",
                     'callbackSuccess': {$success},
                     'callbackError':  {$err}
                     });
         });
        </script>";
        }

        return $html;
    }

    /**
     * @param $name
     * @param $value
     *
     * @return $this
     */
    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
        return $this;
    }

    /**
     * @param $name
     *
     * @return null|mixed
     */
    public function getOption($name)
    {
        return (isset($this->options[$name])) ? $this->options[$name] : null;
    }

    /**
     * @return string
     */
    protected function RenderActions()
    {
        $html = "";
        foreach ($this->actions as $key => $o) {
            $html .= $o->Render(false);
        }

        switch ($this->actionsAlign) {

            case "left":
                $html = "<ul>
                                 <li style='text-align: left; padding-left: {$this->actionsPadding}px; padding-top: 10px;'>
                                    <div id='{$this->id}Response' style='float: right; '></div>
                                     <div style='float: left; padding: 10px 0; '> {$html}</div>

                                 </li>

                         </ul>";
                break;

            case "right":
                $html = "<ul>
                                 <li style='text-align: left; padding: 0; padding-top: 20px;'>
                                     <div id='{$this->id}Response'></div>
                                     <div style='float: right; margin-right: -5px;' class='buttons'> {$html}</div>

                                 </li>
                         </ul>";
                break;

            case "center":
                $html = "<ul>
                                 <li></li>
                                 <li style='text-align: left'>
                                    <div id='{$this->id}Response'></div>
                                    <div style='padding: 10px 0; '>{$html}</div>

                                 </li>

                         </ul>";
                break;

            default:
                $html = "<ul>
                            <li></li>
                            <li>
                                {$html}
                                <div id='{$this->id}Response' style='padding-top: 15px;'></div>
                             </li>

                           </ul>";


        }

        return $html;
    }


    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {

        $htmlObjects = $this->RenderObjects();
        $actions = $this->RenderActions();
        if ($this->getOption('scripts_allowed') !== false) {
            $scripts = $this->RenderScripts();
        } else {
            $scripts = "";
        }


        $html = "";
        $this->class = ($this->class) ? $this->class : 'ff-horizontal';

        $html .= "<div class='ff {$this->class}'>
                      <form id='{$this->id}'
                            method='POST' 
                            action='{$this->action}'
                            enctype='{$this->enctype}'>";


        if ($this->title)
            $html .= "<div style='padding-bottom: 10px;'>
                            <h4><i class='fa fa-angle-down'></i> {$this->title}</h4>
                            <hr/>
                       </div>";


        $html .= "{$htmlObjects}
                  {$actions} 
                      </form>
                      <div class='formOverlayLoader'></div>
                  {$scripts}
                  </div>
                  ";


        return $html;
    }


}