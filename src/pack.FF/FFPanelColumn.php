<?php

/**
 * Class FFPanelColumn
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFPanelColumn extends FFObjectBase implements IFFContainer
{
    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        $html = "<div class='groupColumn'></sup>";
        $html .= $this->RenderContainer();
        $html .= "</div>";

        return $html;

    }


}