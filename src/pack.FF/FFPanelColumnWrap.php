<?php

class FFPanelColumnWrap extends FFObjectBase implements IFFContainer
{

    //---
    private function RenderSeparator()
    {
        return "<div class='groupColumnSeparator'></div>";
    }

    //---
    public function Render($data)
    {
        $html = " <div class='groupColumnWrap'>
                    <h3><i class='fa fa-chevron-down'></i> {$this->title}</h3>
                    <div style='height: 15px'></div>
                    <div class='groupColumns'>";
        $iter = 0;


        foreach($this->objects as $key => $o)
        {
            if($o instanceof IFFContainer)
            {
                $o->Fill($this->model);
            }

            $iter++;

            if($iter == 2)
                $html .= $this->RenderSeparator();
            $html .= $o->Render($data);
        }

        $html .= "</div></div>";

        return $html;

    }



}