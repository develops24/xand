<?php

class FFPanel extends FFObjectBase implements IFFContainer
{
    //---
    public function Render($data)
    {
        $html = "<div class='group'>
                    <sup>{$this->title}</sup>";

        foreach($this->objects as $key => $o)
        {
            $value = (isset($this->model[$key]))    ? $this->model[$key]     : "";
            $html .= $o->Render($value);
        }

        $html .= "</div>";

        return $html;

    }



}