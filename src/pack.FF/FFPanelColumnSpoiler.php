<?php

/**
 * Class FFPanelColumnSpoiler
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFPanelColumnSpoiler extends FFObjectBase implements IFFContainer
{

    public $show;

    /**
     * @param $options
     */
    public function SetDefaults($options)
    {
        $this->show = (isset($options["show"])) ? $options["show"] : true;
    }

    /**
     * @return string
     */
    protected function RenderSeparator()
    {
        return "<div class='groupColumnSeparator'></div>";
    }


    /**
     * @return string
     */
    protected function RenderContainer()
    {
        $html = "";
        $iter = 0;

        foreach ($this->objects as $key => $o) {
            if ($o instanceof IFFContainer) {
                $o->Fill($this->model);
            }

            if ($this->key2) {
                $o->name = $this->key2 . "[" . $o->key . "]";
            }


            $iter++;

            if ($iter == 2)
                $html .= $this->RenderSeparator();

            $value = (isset($this->model[$key])) ? $this->model[$key] : "";
            $html .= $o->Render($value);

        }

        return $html;
    }


    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        $showIcon = ($this->show) ? "minus" : "plus";
        $showBlock = ($this->show) ? " groupColumnSpoilerWrapActive " : "";


        $html = " <div><div class='groupColumnSpoilerWrap {$showBlock}'>
                    <h3 onclick='GroupColumnSpoiler(this); return false;'>
                            <i class='fa fa-{$showIcon}-square-o'></i>
                            <span>{$this->title}</span>
                    </h3>
                    <div class='groupSpoiler'>
                    <div class='groupColumnsSpoiler'>
                    ";
        $iter = 0;

        $html .= $this->RenderContainer();
        $html .= "</div></div></div></div>";

        return $html;

    }


}