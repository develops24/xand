<?php

class FFPanelSpoiler extends FFObjectBase implements IFFContainer
{

    public $show;

    //---
    public function SetDefaults($options) {
        $this->show = (isset($options["show"])) ? $options["show"] : false;
    }


    //---
    public function Render($data) {


        $showIcon = ($this->show) ? "minus" : "plus";
        $showBlock = ($this->show) ? " groupColumnSpoilerWrapActive " : "";

        $html = " <div><div class='groupColumnSpoilerWrap {$showBlock}'>
                    <h3 onclick='GroupColumnSpoiler(this); return false;'>
                            <i class='fa fa-{$showIcon}-square-o'></i>
                            <span>{$this->title}</span>
                    </h3>
                    <div class='groupSpoiler'>";


        foreach ($this->objects as $key => $o) {
            if (is_string($o)) {
                $html .= $o;
                continue;
            }

            /**
             * @var FFObjectBase $o
             */
            if ($o instanceof IFFContainer) {
                $o->Fill($this->model);
            }

            if ($this->key2) {
                if ($o->arrayIndex !== false && is_numeric($o->arrayIndex)) {
                    $search = "_{$o->arrayIndex}";
                    $k = str_replace($search, '', $o->key);
                    $o->name = $this->key2 . "[" . $o->arrayIndex . "]" . "[" . $k . "]";
                } else {
                    $o->name = $this->key2 . "[" . $o->key . "]";
                }
            }

            if ($this->key2 && isset($this->model[$this->key2])) {
                if ($o->arrayIndex !== false && is_numeric($o->arrayIndex)) {
                    $k = str_replace('_' . (string)$o->arrayIndex, '', $o->key);
                    $value = (isset($this->model[$this->key2][$o->arrayIndex][$k])) ? $this->model[$this->key2][$o->arrayIndex][$k] : (string)$o->defaultValue;
                } else {
                    $value = (isset($this->model[$this->key2][$key])) ? $this->model[$this->key2][$key] : (string)$o->defaultValue;
                }
            } else {
                $value = (isset($this->model[$key])) ? $this->model[$key] : (string)$o->defaultValue;
            }

            $html .= $o->Render($value);
        }

        $html .= "</div></div></div>";

        return $html;

    }


}