<?php

class ControlAttachments extends InputBase
{
	//---
	public function SetDefaults()
	{
		$this->setProperty("ignore", false)
			->setProperty("callback_width", "300px")
			->setProperty("callback_style", "")
			->setProperty("callback_custom", false)
			->setProperty("requirements", "")
			->setProperty("titleWidth", 0)
			->setProperty("fileType", "");
	}

	//---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	return ", '".$this->key."':$('#".$this->domId."').val()";
		return false;
	}

	//---
	public function CallbackToString()
	{
		return "<div style='margin:5px 0 0 0;' id='".$this->domId."_callback' ".$this->getProperty('callback_style')." >".$this->getProperty('callback_data')."</div>";
	}

	//---
	public function Draw()
	{
		//var_dump($this);

		$this->properties["height"] = ($this->getProperty("height") != "") ? " style='height: {$this->properties['height']} ;'" : "";
		$this->properties["fileType"] = ($this->getProperty("fileType") != "") ? " accept='{$this->properties['fileType']}'" : "";

		?>
			<div class="attachmentsContaner">

				<div class="attachmentsBody" id="<?php echo $this->domId; ?>_list">

					<?php
					if($this->data)
					{
						$dataArr = explode(",", $this->data);

						foreach($dataArr as $d)
						{

							$htmlBlock =  "<div class='attachmentsBodyRow'>";
							$htmlBlock .= "<div class='attachmentsBodyColumn'>";
							$htmlBlock .= "<a class='attachmentsLink' target='_blank' href='{$d}'>{$d}</a>";
							$htmlBlock .= "</div>";

							$htmlBlock .= "<div class='attachmentsBodyColumn'>";
							$htmlBlock .= "<a href='' onclick='AjaxAttachmentsRemove(this); return false;' title='Remove' class='fa fa-trash-o'></a>";
							$htmlBlock .= "</div>";
							$htmlBlock .= "</div>";

							echo $htmlBlock;
						}

					}

					?>
				</div>

				<form  action="<?php echo PATH_DS.$this->getProperty('file'); ?>" style="height:20px; display:block; margin:0; overflow:hidden;"
					   method="post" id="form_<?php echo $this->domId; ?>" enctype="multipart/form-data" class="inputUploadForm">

					<div class="btn-uploader btn btn-mini">
						<span id="<?php echo $this->domId; ?>_text">Attach file</span>
					</div>

					<input type="hidden" name="MAX_FILE_SIZE" value="10485760" />

					<input type="file" class='inputUpload' name="file_upload" <?php echo $this->properties["fileType"]; ?>
						   onchange="AjaxAttachmentsFormSubmit('<?php echo $this->domId; ?>'); return false;">

					<input id="<?php echo $this->domId; ?>" type="hidden" value="<?php echo $this->data; ?>">

				</form>

			</div>

			<?php

			?>

		<?php

	}
	//---
}
