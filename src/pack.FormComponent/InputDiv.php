<?php

/**
 * Class InputDiv
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class InputDiv extends InputBase
{

    /**
     *
     */
    public function setDefaults()
    {
        $this->setProperty("class", "input_div")
            ->setProperty("titleWidth", 160)
            ->setProperty("ignore", true);
    }

    /**
     *
     */
    public function getDataJson()
    {

    }

    /**
     *
     */
    public function Draw()
    {

        $this->domId = (isset($this->properties["id"])) ? $this->properties["id"] : $this->domId;
        $this->properties["height"] = (isset($this->properties["height"])) ? " style='height: {$this->properties['height']}px; overflow-y: scroll;'" : "";

        if (!isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px;'>	
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        echo "<div class='{$this->properties['class']}'  style='margin: 5px 0;'
		       id='{$this->domId}' 
		       {$this->properties['height']}>" . nl2br($this->data) . "</div>";

        if (!isset($this->properties['titleDisable']))
            echo "</div>";
    }


}
