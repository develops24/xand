<?php
class InputColor extends InputBase
{	
	//---------------
	public function SetDefaults()
	{
		$this->setProperty("titleWidth",160) 
	  		 ->setProperty("width", 160)
			 ->setProperty("requirements","") 
			 ->setProperty("class","inputText");
	}	
	//---------------
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	return ", '".$this->key."':$('#".$this->domId."').val()";			
	}
	//---------------
	public function Draw()
	{
		
		if($this->data == "") 	$this->data = "0";
		 
		if(! isset($this->properties['titleDisable']))
		echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";	
			  
		echo "<input type='color' id='{$this->domId}' style='width:{$this->properties['width']}px; margin-right:10px;'
			   class='{$this->properties['class']}' value='{$this->data}'/>";		
			   
	   	if($this->getProperty('requirements') != "") echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";	
			   	  		
		if(! isset($this->properties['titleDisable'])) echo "</div>";
	}
	//---------------
}