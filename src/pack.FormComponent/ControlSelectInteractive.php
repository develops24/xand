<?php

class ControlSelectInteractive extends ControlSelect
{
    private $extended_dataArray = array();

    //----
    public function GetDataJson()
    {
        return ", '" . $this->key . "': $('#csiData" . $this->properties["action"] . "').val()";
    }

    //----
    public function ManualAdd($arr)
    {
        $this->extended_dataArray[key($arr)] = $arr[key($arr)];
    }

    //----
    public function Draw()
    {
        echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
			  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        echo "<div id='csi{$this->properties["action"]}'>";
        $this->DrawInteractive();
        echo "</div>";

        echo "<input id='csiData" . $this->properties["action"] . "' type='hidden'/>";

        echo "</div>";
    }

    //----
    public function DrawInteractive($data = 0, $eid = false)
    {

        $data = ($this->data) ? $this->data : (int)$data;
        $eid = (isset($this->parent->ajax->eid)) ? $this->parent->ajax->eid : $eid;

        $this->properties['filter'] = (isset($this->properties['filter'])) ? "AND {$this->properties['filter']}" : "";
        $formatOut = array($this->properties['fieldTitle'], $this->properties['fieldPrimary']);

        $selectFields = [
            $this->properties['fieldPrimary'],
            $this->properties['fieldParent'],
            $this->properties['fieldTitle'],
            $this->properties['fieldPath']
        ];

        $csi = new ControlSelectClassic("", $this->properties['fieldPrimary']);
        $csi->setProperty("onchange", "CSIChange(this, {$eid}, \"{$this->properties['action']}\");");

        $model = (new Model())->setTableName($this->properties['table']);
        $current = $model->findAll($this->properties['fieldPrimary'] . " = {$data} {$this->properties['filter']}", $selectFields);
        $children = $model->findAll($this->properties['fieldParent'] . "  = {$data} {$this->properties['filter']}", $selectFields);


        echo "<script>CSIUpdateValue('csiData" . $this->properties["action"] . "', {$data});</script>";

        if ($current) {
            $arr_node = explode("/", $current[0][$this->properties['fieldPath']]);

            foreach ($arr_node as $val) {
                if ($val == "") continue;

                $idp = $model->findField($this->properties['fieldPrimary'] . " = " . $val, $this->properties['fieldParent']);

                $csi->properties['no_choose_val'] = $idp;

                $csi->dataArray = ControlSelectClassic::DataFormat(
                    $model->findAll($this->properties['fieldParent'] . " = {$idp} {$this->properties['filter']}"),
                    $formatOut);

                $csi->data = $val;
                $csi->Draw();
                $csi->dataArray = NULL;
            }
        }

        if ($children) {
            $csi->dataArray = ControlSelectClassic::DataFormat($children, $formatOut);
            $csi->properties['no_choose_val'] = 0;
            $csi->data = NULL;
            $csi->Draw();
        }


    }


}
