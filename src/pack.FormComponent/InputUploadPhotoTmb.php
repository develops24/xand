<?php
 
class InputUploadPhotoTmb extends InputBase
{	
	//---
	public function SetDefaults()
	{
		$this->setProperty("ignore", false)			
		     ->setProperty("callback_width", "300px")
			 ->setProperty("callback_style", "")
			 ->setProperty("callback_custom", false)	
		 	 ->setProperty("requirements", "")
			 ->setProperty("titleWidth",160) 
			 ->setProperty("tmbDataDefault", "50,50,300,300")
			 ->setProperty("fileType", "");
	}

	//---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	
			return ", '".$this->key."':$('#".$this->domId."_value').val()".
	 			   ", '".$this->key."Data':$('#".$this->domId."_data').val()";		
	}

	//---
	public function DrawCallback()
	{
		     ?><div id='<?php echo $this->domId; ?>_callback'><?php

			 $pathData = explode(",", $this->getProperty('tmbData'));
			 if(count($pathData) != 4)
				 $pathData = explode(",", $this->getProperty('tmbDataDefault'));

			 $pathData[2] += $pathData[0];
			 $pathData[3] += $pathData[1];

			 if($this->properties['callback_data'])
				 echo "<img id='{$this->domId}img' src='{$this->properties['callback_data']}' />";

		    ?>
            </div>

            <script>
            $( "#<?php echo $this->domId; ?>img" ).load(function() {

                JCropInit(this, '<?php echo $this->domId; ?>', [<?php echo implode(",", $pathData);  ?>]);

            });
            </script>


      <?php	
	}

	//---
	public function CallbackToString()
	{	
		return "<div style='margin:5px 0 0 0;' id='".$this->domId."_callback' ".$this->getProperty('callback_style')." >".$this->getProperty('callback_data')."</div>";
    }

	//---
	public function Draw()
	{
			//var_dump($this);
			
	 		$this->properties["height"] = ($this->getProperty("height") != "") ? " style='height: {$this->properties['height']} ;'" : "";
			
			$this->properties["fileType"] = ($this->getProperty("fileType") != "") ? " accept='{$this->properties['fileType']}'" : "";	
			
						 
			?> 
 
            
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="formElement">
                     <tr>                           
						  <td valign="middle" width="<?php echo $this->properties['titleWidth']; ?>px">                          
						  <?php  echo "<span>{$this->name}</span>";  ?>   
                          </td>   
                          
                          <td width="120px">
                            <form  action="<?php echo $this->getProperty('file'); ?>"
                            method="post" id="form_<?php echo $this->domId; ?>" enctype="multipart/form-data" class="inputUploadForm">
                               
                               <div class="inputUploadMask">                               		
                                    <span id="<?php echo $this->domId; ?>_text">Choose file</span>    
                                    <span id="<?php echo $this->domId; ?>_loading"  class="inputUploadLoading"></span> 
                              	   
                               </div>
                               <input type="file" class='inputUpload' name="file" <?php echo $this->properties["fileType"]; ?>
                               		onchange="JCropInputUploadProccess('<?php echo $this->domId; ?>'); return false;">

                            <input id="<?php echo $this->domId; ?>" type="text" name="domId" value="<?php echo $this->domId; ?>">
                            <input id="<?php echo $this->domId; ?>_value" type="text" name="domId_value"
                            		 value="<?php echo $this->data; ?>">  
                            <input id="<?php echo $this->domId; ?>_data" type="text" name="domId_data" 
                            		value="<?php echo $this->getProperty('tmbData'); ?>"> 
                                                          
                            </form>
                          </td>                         
                                                              
                          <td valign="middle">                        
						  <?php
                          	 if($this->getProperty('requirements') != "") 	
							 {	
								echo "<div class='formRequirements'>".$this->properties['requirements']."</div>";		 						
							 }
                          ?>   
                          </td></tr>                          
                          <tr>
                          <td></td><td colspan="2"><div style="height:5px;"></div>  
                            <?php	 $this->DrawCallback();	 ?> 
                          </td>
                          </tr>
                          
                          </table>

			<?php			
			
	}
	//-----
}