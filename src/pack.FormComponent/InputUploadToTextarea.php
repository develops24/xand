<?php
 
class InputUploadToTextarea extends InputBase
{	
	//---------------
	public function SetDefaults()
	{
		$this->setProperty("ignore", false)			
		     ->setProperty("callback_width", "300px")
			 ->setProperty("callback_style", "")
			 ->setProperty("callback_custom", false)	
		 	 ->setProperty("requirements", "")
			 ->setProperty("titleWidth", 0) 
                         ->setProperty("buttonsAdditionals", "")
			 ->setProperty("fileType", "");
	}	
	//---------------
	public function GetDataJson()
	{
		return false;
	}	
	
	//---------------
	public function DrawCallback()
	{
		?>
		 <div id='<?php echo $this->domId; ?>_callback'><?php echo $this->getProperty('callback_data'); ?></div>	                       
      <?php	
	}
	
	//---------------
	public function CallbackToString()
	{	
		return "<div style='margin:5px 0 0 0;' id='".$this->domId."_callback' ".$this->getProperty('callback_style')." >".$this->getProperty('callback_data')."</div>";
    }
	
	//---------------
	public function Draw()
	{
			//var_dump($this);
			
	 		$this->properties["height"] = ($this->getProperty("height") != "") ? " style='height: {$this->properties['height']} ;'" : "";			
			$this->properties["fileType"] = ($this->getProperty("fileType") != "") ? " accept='{$this->properties['fileType']}'" : "";				
						 
			?>   
           <div style="height:30px; padding-top:5px; padding-left:<?php echo $this->getProperty('titleWidth'); ?>px"> 
              
                     
                 <button class="btn btn-mini btn-danger" 
                                onClick="ClearTextarea(this)" 
                 		style="display:block; margin:0; margin-right:5px; float:left; ">Clear</button>
                
                 <form  action="<?php echo PATH_DS.$this->getProperty('file'); ?>" style="width:70px; display:block;  
                 		margin:0; float:left;  overflow:hidden;"
                    method="post" id="form_<?php echo $this->domId; ?>" enctype="multipart/form-data" class="inputUploadForm"> 
                       <div class="btn-uploader btn btn-mini">                               		
                            <span id="<?php echo $this->domId; ?>_text">Attach file</span>    
                                                    
                       </div> 
                       
                       <input type="hidden" name="MAX_FILE_SIZE" value="10485760" /> 
                       
                       <input type="file" class='inputUpload' name="file_upload" <?php echo $this->properties["fileType"]; ?>
                              onchange="AjaxFormSubmit('<?php echo $this->domId; ?>'); return false;">     
                       <input id="<?php echo $this->domId; ?>" type="hidden" name="domId" style="height:24px" 
                              value="<?php echo $this->domId; ?>">
                       <input id="<?php echo $this->domId; ?>_value" type="hidden" name="domId_value" 
                              style="height:24px" value="<?php echo $this->data; ?>">  
                    </form>
               
               <?php 
			   
			  
			   ?>
                    <?php echo $this->properties["buttonsAdditionals"]; ?>  
               
            </div>   
             
             
             <div>
                <?php	 $this->DrawCallback();	 ?> 
             </div>
                     
            <script>
		<?php echo $this->domId; ?>Success = null;  			       
            </script>                 	
            <?php			
			
	}
	//---------------
}
?>