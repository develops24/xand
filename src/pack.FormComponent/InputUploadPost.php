<?php

class InputUploadPost extends InputBase
{
    //---
    public function SetDefaults()
    {
       $this->setProperty("titleWidth", 160)
            ->setProperty("width", 160)
            ->setProperty("requirements", "")
            ->setProperty("class", "inputText");
    }

    //---
    public function GetDataJson()
    {
        return false;
    }

    //---
    public function Draw()
    {

        if(! isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        $width = (isset($this->properties['width'])) ? "style='width:".$this->properties['width']."px'" : "";

        echo "<input id='{$this->domId}'
                type='file'
                class='{$this->properties['class']}'
                {$width}/>";

        if($this->getProperty('requirements') != "") echo "<div class='formRequirements'>{$this->properties['requirements']}</div>";
        if(! isset($this->properties['titleDisable'])) echo "</div>";
    }

    //---
}
?>

