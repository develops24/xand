<?php
class ControlAjax extends InputBase
{
	//-----
	public function SetDefaults()
	{
		$this->setProperty("path", "path")			 
			 ->setProperty("ignore", true);
		
	}

	//-----
	public function GetDataJson(){}

	//-----
	public function Draw()
	{				
		Ajax::Load($this->getProperty("path"), $this->domId."a");	
	}

}