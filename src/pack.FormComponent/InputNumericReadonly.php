<?php

/**
 * Class InputNumericReadonly
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */

class InputNumericReadonly extends InputNumeric
{

    /**
     * @return bool
     */
    public function getDataJson()
    {
        return false;
    }

    /**
     *
     */
    public function Draw()
    {

        $this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;
        if ($this->data == "") {
            $this->data = "0";
        }

        if (!isset($this->properties['titleDisable'])) {
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
        }
        echo "<input type='number' id='{$this->domId}' 
			    style='width:{$this->properties['width']}px; padding-left: 10px; text-indent: 20px;'
				max='{$this->properties['maxValue']}'  
				min='{$this->properties['minValue']}'
				name='{$this->key}'
			    step='{$this->properties['increment']}'
			    class='{$this->properties['class']}'
			    readonly='readonly'
			    disabled='disabled'
			    placeholder='{$this->properties['placeholder']}'
			    value='{$this->data}'/>";

        if ($this->getProperty('requirements') != "") echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";

        if (!isset($this->properties['titleDisable'])) echo "</div>";
    }

}
