<?php

/**
 * Class ControlSelectMultiple
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ControlSelectMultiple extends ControlSelect
{
    /**
     *
     */
    public function setDefaults()
    {
        $this->setProperty("titleWidth", 160)
            ->setProperty("ignore", false)
            ->setProperty("width", 200)
            ->setProperty("maxHeight", 200)
            ->setProperty("noChoose", true)
            ->setProperty("noChooseTitle", "Not selected")
            ->setProperty("noChooseVal", "");
    }

    /**
     * @return string
     */
    public function getDataJson()
    {
        if ($this->properties['ignore'] == false) {
            return ", '" . $this->key . "':GetSelectMultipleById('{$this->domId}')";
        }
    }

    /**
     * @param $arr
     */
    public function ManualAdd($arr)
    {
        $this->dataArray[key($arr)] = $arr[key($arr)];
    }

    /**
     * @param $arr
     * @param $arr2
     */
    public function Fill($arr, $arr2)
    {
        foreach ($arr as $key => $val) {
            $this->dataArray[$val[$arr2[1]]] = $val[$arr2[0]];
        }
    }

    /**
     *
     */
    public function Draw()
    {

        $options = "";

        if (!empty($this->dataArray)) {

            $this->convertData();

            $options = "";
            $title = ($this->getProperty("noChoose")) ? $this->getProperty("noChooseTitle") : $this->dataArray[0]['title'];
        }


        if (!is_array($this->data))
            $this->data = array($this->data);

        if (!empty($this->dataArray)) {

            foreach ($this->dataArray as $val) {
                if (!isset($val['equal'])) {
                    $val['equal'] = $val['data'];
                }

                $select = (in_array($val['equal'], $this->data)) ? "selected='selected'" : "";
                $options .= "<option value='" . $val['data'] . "' " . $select . ">{$val['title']}</option>";
            }
        }

        $size = count($this->dataArray);

        if (!isset($this->properties['titleDisable'])) {
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
            echo "<select multiple='multiple' size={$size} id='" . $this->domId . "' class='controlSelectMultiple' style='max-height:" . $this->properties['maxHeight'] . "px;overflow-y:auto;'>";
        }

        echo $options;

        echo "</select>";

        if ($this->getProperty('requirements') != "") {
            echo "<div class='formRequirements' style='margin: 5px 0  5px 0;'>{$this->properties['requirements']}</div>";
        }

        if (!isset($this->properties['titleDisable'])) {
            echo "</div>";
        }
    }
}
