<?php

/**
 * Class InputBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class InputBase extends ClassBase
{

    public $data;
    public $dataArray = array();
    public $domId = "";
    public $key = "";
    public $name = "";

    /**
     * InputBase constructor.
     */
    function __construct()
    {
        $arr = func_get_args();

        $this->name = (isset($arr[0])) ? $arr[0] : "";
        $this->key = (isset($arr[1])) ? $arr[1] : "notdefined";
        $properties = (isset($arr[2])) ? $arr[2] : array();
        $this->data = (isset($arr[3])) ? $arr[3] : "";
        $this->dataArray = (isset($arr[4])) ? $arr[4] : array();

        $this->setProperty("virtual", false)
            ->setProperty("ignore", false);

        if (method_exists($this, 'SetDefaults')) {
            $this->SetDefaults();
        }

        if (!empty($properties)) {
            foreach ($properties as $key => $val) {
                $this->properties[$key] = $val;
            }
        }

        $this->domId = (isset($this->properties['domId']) && $this->properties['domId']) ? $this->properties['domId'] : $this->getUniqueName();

    }

    /**
     *
     */
    protected function ConvertData()
    {
        if (count(current($this->dataArray)) == 1) {

            $source = $this->dataArray;
            $this->dataArray = array();
            foreach ($source as $key => $item) {
                $this->dataArray[] = array(
                    'title' => $item,
                    'equal' => $key,
                    'data' => $key
                );
            }
        }
    }

}
