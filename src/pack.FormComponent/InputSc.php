<?php
class InputSc extends InputBase
{	  
	//------ 
	public function SetDefaults()
	{	 
		$this->setProperty("height",200)
			 ->setProperty("titleWidth",160) 
		     ->setProperty("width","100%")
			 ->setProperty("init", true); 
	}
	
	//------ 
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	
		{
			return ", '".$this->key."': ".$this->domId."GetText()";
		}		
	} 
	
	//------ 
	public function Draw()
	{	  
		
		if(! isset($this->properties['titleDisable']))				   

            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
		  	      <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
			  
		    echo "<textarea id='".$this->domId."' name='bbcode_field'
			      style='height:".$this->properties['height']."px; width:100%;'>".$this->data."</textarea>";
			  
			 
		if(! isset($this->properties['titleDisable'])) echo "</div>"; 
		
		
		
		
		?>   
        
        <script type="text/javascript"> 
				<?php echo $this->domId; ?>initEditor = function() {
						$("#<?php echo $this->domId; ?>").sceditor({
							emoticonsRoot:"<?php echo PATH_DS."tools/sceditor/"; ?>",
							plugins: "xhtml",
						 
							style: "<?php echo PATH_DS."tools/sceditor/"; ?>minified/jquery.sceditor.default.min.css"
							});
				}; 	 
				
				function <?php echo $this->domId; ?>GetText()
				{
					var inst = $("#<?php echo $this->domId; ?>").sceditor("instance"); 
					var content = "";
				 	
					if(!inst)
					{
						 content = $("#<?php echo $this->domId; ?>").val();
					}
					else
					{
						 content = inst.val(); 
					} 

					content = content.replace(/\r\n|\r|\n/g, "#RN#");   
					content = ContentPrepare(content);	    
					content = content.replace(/#RN#/g, "\r\n"); 	
				
			  		return content;    
				}
				 
				<?php if($this->properties['init']) { ?>
					<?php echo $this->domId; ?>initEditor();	
				<?php } ?>		 
			 
		</script> 
         
        
		<?php	
		 
	 
	}
 
}
?>