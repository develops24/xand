<?php
class InputRte extends InputBase
{	 	
	public $rte;
	
	//------ 
	public function SetDefaults()
	{	 
		$this->setProperty("height",500)
			 ->setProperty("titleWidth",160) 
		     ->setProperty("width","100%");
			 

		include_once(PATH_REAL."/tools/richtexteditor/include_rte.php");
		$this->rte=new RichTextEditor(); 
	}
	
	//------ 
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	
		{
			return ", '".$this->key."': editorGetText()";
		}		
	} 
	
	//------ 
	public function Draw()
	{	 
		$this->rte->Text = $this->data; 
	    $this->rte->MvcInit(); 	
		 
		  
	 	if(! isset($this->properties['titleDisable']))				   
		 	echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
		  	  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";		 
		      
			 echo $this->rte->GetString(); 
			 
		if(! isset($this->properties['titleDisable'])) echo "</div>"; 
		
		?>   
        
        <script type="text/javascript">
			var editor;
			function RichTextEditor_OnLoad(rteeditor) 
			{
				editor = rteeditor;
			}   
			
			function editorGetText()
			{
				var content = editor.GetText();
					content = content.replace(/\r\n|\r|\n/g, "#RN#");   
					content = ContentPrepare(content);	    
					content = content.replace(/#RN#/g, "\r\n");   
			    return content;   
			}
			 
		</script>
		<?php	
		 
	 
	}
 
}
?>