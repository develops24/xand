<?php
class InputBb extends InputBase
{	

	//---------------
	public function SetDefaults()
	{						
		$this->setProperty("height",280);
	}	
	
	//---------------
	public function GetDataJson()
	{
		if(!$this->properties['ignore'])			
			 return ", '".$this->key."':BbEncoder($('#".$this->domId."').val())";								
	}	
	
	//---------------
	public static function Parse($str)
	{
		 if(!function_exists("_myFunc"))
		 {
				 function _myFunc($matches)
				 {			
					return str_replace("<br />", "\n",  $matches[0]);
				 }
		 }
				  
		 $str = preg_replace_callback('#\[php\](.+)\[/php\]#U'	, '_myFunc', $str);
		 $str = preg_replace_callback('#\[js\](.+)\[/js\]#U'	, '_myFunc', $str);
		 $str = preg_replace_callback('#\[css\](.+)\[/css\]#U'	, '_myFunc', $str);
		 $str = preg_replace_callback('#\[html\](.+)\[/html\]#U', '_myFunc', $str);	 
		
		 $search = array(
					'@\[(?i)b\](.*?)\[/(?i)b\]@si',
					'@\[(?i)i\](.*?)\[/(?i)i\]@si',
					'@\[(?i)u\](.*?)\[/(?i)u\]@si',
					'@\[h2\](.*?)\[/h2\]@si',
					'@\[h3\](.*?)\[/h3\]@si',
					'@\[h4\](.*?)\[/h4\]@si',
					'@\[(?i)img\](.*?)\[/(?i)img\]@si',
					'@\[(?i)url=(.*?)\](.*?)\[/(?i)url\]@si',
					'@\[(?i)php\](.*?)\[/(?i)php\]@si',
					'@\[(?i)js\](.*?)\[/(?i)js\]@si',
					'@\[(?i)css\](.*?)\[/(?i)css\]@si',
					'@\[(?i)html\](.*?)\[/(?i)html\]@si'
					);
			
		  $replace = array(
					'<b>\\1</b>',
					'<i>\\1</i>',
					'<u>\\1</u>',
					'<h2>\\1</h2><hr>',
					'<h3>\\1</h3>',
					'<h4>\\1</h4>',
					'<a href="\\1" target="_blank"><img style="max-width:750px;" src="\\1"></a>', 
					'<a href="#mod/wiki/\\1" style="color:#333;" onclick="ajaxprotocol.Go(\'\\1\');">\\2</a>',	  
					'<pre class="brush: php;">\\1</pre>',
					'<pre class="brush: js;">\\1</pre>',
					'<pre class="brush: css;">\\1</pre>',
					'<pre class="brush: html;">\\1</pre>'
					
		  );
		
		 $str = preg_replace($search , $replace, $str);	
		 $str = str_replace("<br /><h3>", "<h3>", $str);	 
		 $str = str_replace("<br /><h4>", "<h4>", $str);	
		 $str = str_replace("</h3><br />", "</h3>", $str);	 
		 $str = str_replace("</h4><br />", "</h4>", $str);	 	
		 
		 
		  
		 return $str;
	}	
	
	//---------------
	public function Draw()
	{	 				
		$this->data = str_replace("<br />", "\n", $this->data);
		
		echo "<textarea id='{$this->domId}' class='inputTextareaBb' style = 'height: {$this->properties['height']}px;'>{$this->data}</textarea>
			  <script>
				$('#{$this->domId}').BbReady();
			  </script>
			  ";	
	}
	//---------------
}
?>

