<?php
class InputDate extends InputBase
{	

	//---
	public function SetDefaults()
	{
		$this->setProperty("titleWidth",160) 
	  		 ->setProperty("width", 150)
			 ->setProperty("requirements","")
			 ->setProperty("class","inputText");
	}

	//---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	return ", '".$this->key."':$('#".$this->domId."').val()";			
	}

	//---
	public function Draw()
	{ 
		$this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;	
		
		if($this->data == "") 	$this->data = "0";

		if(strlen($this->data) == 10 && is_numeric($this->data))
		{
			$this->data = date("Y-m-d", $this->data);
		}

		if(! isset($this->properties['titleDisable']))
		echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";	
			  
			   
	   	if($this->getProperty('requirements') != "") echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";	
			   	  		
		echo "<input type='text' id='{$this->domId}' style='width:{$this->properties['width']}px;' placeholder='yyyy-mm-dd'
			   class='{$this->properties['class']}' value='{$this->data}'/>";
		if(! isset($this->properties['titleDisable'])) echo "</div>";

		?>

		<script>
			setTimeout(function(){

				$('#<?php echo $this->domId; ?>').datepicker();
				$('#<?php echo $this->domId; ?>').datepicker( "option", "dateFormat", "yy-mm-dd" )
												 .datepicker( "setDate", "<?php echo $this->data; ?>" );
			}, 10);

		</script>

		<?php


	}
	//---
}
