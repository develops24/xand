<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 25.12.15
 * Time: 12:28
 */
class InputCheckboxCompactPretty extends InputCheckboxCompact
{

    public function Draw() {

        $this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;

        $check = ($this->data == 1) ? 'Sel' : 'UnSel';
        $class = ($this->properties['disabled']) ? " checkboxDisabled checkboxDisabled{$check}" : "checkbox{$check}";

        echo "<div class='formElementCompact'>
                 <a href='' id='{$this->domId}' class='checkbox {$class}' onclick='CheckboxClick(this); return false;'>
				     {$this->name}
			     </a>
			     <span class='phpdoc'>" . $this->getProperty('requirements') . "</span>
              </div>";


    }
}