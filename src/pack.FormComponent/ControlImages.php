<?php

/**
 * Class ControlImages
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   3
 *
 *
 **/


class ControlImages extends InputBase
{	
		
	//---------------
	public function SetDefaults()
	{
		$this->setProperty("is_ignore", false)
			 ->setProperty("callback_position", "bottom")
		     ->setProperty("callback_width", "300px")
			 ->setProperty("callback_style", "")
			 ->setProperty("callback_custom", false)	
		 	 ->setProperty("requirements", "")
			 ->setProperty("fileType", "");
	}	
	//---------------
	public function GetDataJson()
	{
		if($this->properties['is_ignore'] == false)	return ", '".$this->key."':$('#".$this->domId."_value').val()";
		return false;
	}	
			
		
	//---------------
	public function DrawLoader()
	{
		?>
	
        <div class="formElement" style="width:120px">
                    <form  action="<?php echo PATH_DS.$this->getProperty('service'); ?>" 
                       method="post" id="form_<?php echo $this->domId; ?>" enctype="multipart/form-data" class="inputUploadForm">
                       
                       <div class="inputUploadMask">                               		
                            <span id="<?php echo $this->domId; ?>_text">Choose file</span>    
                        	<span id="<?php echo $this->domId; ?>_loading"  class="inputUploadLoading"></span> 
                       </div>
                       <input type="file" class='inputUpload' name="file_upload" <?php echo $this->properties["fileType"]; ?>
                       onchange="ajaxFormSubmit('<?php echo $this->domId; ?>'); return false;">  
                       <input id="<?php echo $this->domId; ?>" type="hidden" name="domId" style="height:24px" value="<?php echo $this->domId; ?>">
                      
                    </form>
		</div>                                                
           
	<?php
	
	}	
	//---------
	
	public function Draw()
	{			
		
		$this->DrawLoader();
		
		$images = explode(",", $this->data);		
		$this->data = $this->data.",";	
		$this->data = str_replace(",,",",", $this->data);
		?>    
        
                  
        <div class="controlImagesWrap">
            
            <div id="controlImagesInteractiveArea">
				<?php 
				foreach($images as $key => $im) 
				{ 
					if($im == "") continue;					
					?>
					<div class="controlImagesItem" style="background-image:url('<?php echo $this->properties['imagesPath'].$im."_min.jpg"; ?>');">
			
							<div class="controlImagesItemOverlay"></div>
							<a href="" class="fa fa-check"  onclick="CiMakeMain(this); return false;" title="Make main"></a>
							<a href="" class="fa fa-times"  onclick="CiDelBlock(this); return false;" title="Delete"></a>
							<input class='CiItemData' type="hidden" value="<?php echo $im;?>" />
							
					</div>
					<?php
				}			
				?>            
             </div>
            <div style="clear:both;"></div>
        </div>
        
        
         
            <?php
                       if($this->getProperty('requirements') != "") 	
                       {	
                          echo "<div class='formRequirements' style='margin:10px 0;'>";
                          echo $this->getProperty('requirements');
                          echo "</div>";							
                       }	
                       else
                       {
                           echo "<div style='border:solid 1px #fff;'></div>";
                       }
                    ?>        
        
        <input id="<?php echo $this->domId; ?>_value" type="hidden" name="domId_value" 
        		style="height:24px; width:80%;" value="<?php echo $this->data; ?>">  
        <input id="<?php echo $this->domId; ?>_value_new" type="hidden" name="domId_value_new" value="">  
        <div id='<?php echo $this->domId; ?>_callback' style="margin-top:5px;"></div>	  
       
        <script>
				
		<?php echo $this->domId; ?>Success = null;			
		
		
		
		function CiMakeMain(obj)
		{			
			var bg 		= $(obj).parent().css("background-image");		
			var bg_main = $("#controlImagesInteractiveArea > .controlImagesItem:first").css("background-image");
			$("#controlImagesInteractiveArea > .controlImagesItem:first").css("background-image", bg);
			$(obj).parent().css("background-image",bg_main);			
			
			var data   		=  $(obj).parent().find(".CiItemData").val();
			var data_main   =  $("#controlImagesInteractiveArea > .controlImagesItem:first").find(".CiItemData").val();			
			$("#controlImagesInteractiveArea > .controlImagesItem:first").find(".CiItemData").val(data);
			$(obj).parent().find(".CiItemData").val(data_main);
			
			var value	  = $('#<?php echo $this->domId; ?>_value').val();
			var value_arr = value.split(",");
			
			
			for(var i=0; i < value_arr.length; i++)
			{
				if(value_arr[i] == data)  
				{				
					var temp     = value_arr[0];
					value_arr[0] = value_arr[i];
					value_arr[i] = temp;
				}
			}
			
			$('#<?php echo $this->domId; ?>_value').val(value_arr.join(','));
		}		
		
		function CiAddNew()
		{	
			var value	  = $('#<?php echo $this->domId; ?>_value').val();
			var value_new = $('#<?php echo $this->domId; ?>_value_new').val();
			
			if(value_new == "") return;
			
			$('#<?php echo $this->domId; ?>_value_new').val('');
			
			value = value + value_new + ',';
			$('#<?php echo $this->domId; ?>_value').val(value);		
			
			$("#controlImagesInteractiveArea").append("<div class='controlImagesItem'></div>");
			$("#controlImagesInteractiveArea > .controlImagesItem:last").css("background-image", "url(<?php echo $this->properties['imagesPath']; ?>"+value_new+"_min.jpg)");
			$("#controlImagesInteractiveArea > .controlImagesItem:last").append("<div class='controlImagesItemOverlay'></div>");
			$("#controlImagesInteractiveArea > .controlImagesItem:last").append("<a href='' class='fa fa-check'  onclick='CiMakeMain(this); return false;' title='Make main'></a>");
			$("#controlImagesInteractiveArea > .controlImagesItem:last").append("<a href='' class='fa fa-times'  onclick='CiDelBlock(this); return false;' title='Delete'></a>");
			
			$("#controlImagesInteractiveArea > .controlImagesItem:last").append("<input class='CiItemData' type='hidden' value='"+value_new+"' />");
			
			CiHightLightFirst();	
		}		
		
		function CiDelBlock(obj)
		{
			$(obj).parent().remove();
			
			var repl = $(obj).parent().find(".CiItemData").val()+',';
			
			var str = $('#<?php echo $this->domId; ?>_value').val();
			str = str.replace(repl, "");
			$('#<?php echo $this->domId; ?>_value').val(str);
			
			CiHightLightFirst();	
		}		
		
		
		function ajaxFormSubmit(el_uid)
		{				
			
			var options = {	
				
				  beforeSubmit: function()
				  {
					   $('#'+el_uid+'_text').css("display", "none");	
					   $('#'+el_uid+'_loading').css("display", "block");	
					   $('#'+el_uid+'_callback').html('Loading...');	
				  },
			
				  success: function(responseText ) {
					  
					  $('#'+el_uid+'_text').css("display", "block");
					  $('#'+el_uid+'_loading').css("display", "none");								
					  $('#'+el_uid+'_callback').html(responseText);	
					  CiAddNew();		
					  if(<?php echo $this->domId; ?>Success != null) <?php echo $this->domId; ?>Success();		
					  												
				  }
				};
				
			$('#form_'+el_uid).ajaxSubmit(options);			
			
		}	
		
		function CiHightLightFirst()
		{
			$(".controlImagesItem").removeClass("controlImagesItemMain");
			$(".controlImagesItem:first").addClass("controlImagesItemMain");
		}
		
		CiHightLightFirst();						       
		 </script>   
        
        <?php	

	}
	//---
}
?>