<?php

class Label extends InputBase
{
    //---
    public function SetDefaults()
    {
    }

    //---
    public function GetDataJson()
    {
    }

    //---
    public function Draw()
    {
        $arrow = "";
        if ($this->key)
            $arrow = "<i class='fa fa-arrow-down'></i>";
        echo "<h3>{$arrow} {$this->name}</h3>";

    }
    //---
}


