<?php
class ControlMap extends InputBase
{	
	//---------------
	public function SetDefaults()
	{	
		$this->setProperty("ignore", true) 
			 ->setProperty("show", true)
			 ->setProperty("geoStart", "50.068, 8.239");	
	}	
	//---------------
	public function GetDataJson()
	{
		return ", '".$this->key."': ".$this->domId."lat + ', ' + ".$this->domId."lng";	
	}
	//---------------
	public function Draw()
	{	  
		
		$this->properties["geoStart"] = (isset($this->data) && $this->data != "") ? $this->data : $this->properties["geoStart"];
		$latLngArrInit = explode(",", $this->properties["geoStart"]);
		 
		?>  
        
        <div style="position:relative; height:100%; width:100%; ">
	        <div id='mapCanvas'  style='height:100%; width:100%;'></div>
    	
	        <div style="position:absolute; top:0; right:0; height:25px; left:0; opacity:0.7; background-color:#FFF;"></div>
           
            <div style="position:absolute; top:0; right:0; height:25px; left:0; border-bottom:solid 2px #fff;">
            			<input id='address' style='width:350px; float:left; padding:3px; margin:1px; border:solid 1px #ddd; font-size:12px;'  
                         type='textbox' value='' placeholder="location" onkeyup="geoKeyUp(event);">            
                         
                         <div id='addressFind' style="float:right; padding:5px; text-shadow:1px 0  1px #fff; font-size:13px;
                         font-weight:700; color:#555;"></div>                         
            </div>
        </div>
        
        
       
        <script> 
					
					    var map 	 = null; 
						var geocoder;						    
						var marker;
						var myLatlng = new google.maps.LatLng(<?php echo $this->properties["geoStart"]; ?>);
						
						var <?php echo $this->domId; ?>lat = <?php echo $latLngArrInit[0]; ?>;
						var <?php echo $this->domId; ?>lng = <?php echo $latLngArrInit[1]; ?>;						
					   
						function gMapInitial()
						{							 
							 
							map = new google.maps.Map(document.getElementById('mapCanvas'), {
							  zoom: 12,
							  disableDefaultUI: true,
							  center: myLatlng,
							  mapTypeId: google.maps.MapTypeId.ROADMAP
							});	
							  
							google.maps.event.addListenerOnce(map, 'idle', function() {
								google.maps.event.trigger(map, 'resize');
								map.setCenter(myLatlng); // be sure to reset the map center as well
							}); 
							geocoder = new google.maps.Geocoder();
							marker = new google.maps.Marker({
								  position: myLatlng,
								  draggable: true,
								  map: map,
								  title: "Marker"
							 });
							 
							 google.maps.event.addListener(marker, "dragend", function(event) {

								   var point = marker.getPosition();  
								   <?php echo $this->domId; ?>lat  = point.lat();
								   <?php echo $this->domId; ?>lng  = point.lng();								 							
								  
								   var latlng = new google.maps.LatLng(point.lat(), point.lng());
									 
								   geocoder.geocode({'latLng': latlng}, function(results, status)
								   {
									if (status == google.maps.GeocoderStatus.OK) 
										  if (results[0]) 										   	 
											 $("#addressFind").html(results[0].formatted_address);		
								   });
							  }); 
						
						  }   
						  
						  
						  function geoKeyUp(e)
						  {
							 if(e.keyCode == 13)  codeAddress();
						  }
						  
						  
						  function codeAddress() {
							var address = document.getElementById('address').value;
							geocoder.geocode( { 'address': address}, function(results, status) {
							  if (status == google.maps.GeocoderStatus.OK) {
								map.setCenter(results[0].geometry.location);								
								marker.setPosition(results[0].geometry.location);								
								
							  } else {
								alert('Geocode was not successful for the following reason: ' + status);
							  }
							});
						  }
											
					</script>
        
        
        <?php
		 
		
	}
	//---------------
}
 
					   
                    
 ?>            
