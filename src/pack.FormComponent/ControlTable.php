<?php

/**
 * Class ControlTable
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ControlTable extends InputBase
{
    /**
     *
     */
    public function setDefaults()
    {
        $this->setProperty("ignore", true)
            ->setProperty("class", "controlGrouperWrap")
            ->setProperty("parentClass", "formElement")
            ->setProperty("columnNames", [])
            ->setProperty("columnWidth", 100)
            ->setProperty("width", false)
            ->setProperty("border", true)
            ->setProperty("align", "left")
            ->setProperty("show", true);
    }

    /**
     *
     */
    public function getDataJson()
    {
    }

    /**
     *
     */
    public function Draw()
    {
        $this->data = (isset($this->properties['data'])) ? $this->properties['data'] : $this->data;

        if ($this->getProperty("rows")) {
            $rows = $this->properties["rows"];
        } else {
            if (!empty($this->data)) {
                foreach (reset($this->data) as $k => $v)
                {
                    $name = ucfirst($k);
                    $rows[$k] = $name;
                }

                if (empty($this->properties["columnNames"])) {
                    $this->properties["columnNames"] = $rows;
                }

            }
        }

        $doNotStretch = ($this->getProperty('doNotStretch')) ? "style='display:inline-block;'" : "";

        $cssShow = ($this->getProperty('show')) ? "controlGrouperDown" : "controlGrouperUp";
        $sign = ($this->getProperty('show')) ? "fa-chevron-down" : "fa-chevron-up";
        $width = ($this->properties["width"]) ? "style='width:{$this->properties["width"]}px;'" : "";

        echo "<div class ='" . $this->getProperty('parentClass') . "' {$doNotStretch}{$width}>
		 	 <div class = 'controlGrouperTitle' onclick='GrouperSpoiler(this);'>
			 <i style='float: left; padding: 6px 6px 0  0;' class='fa {$sign}'></i> {$this->name}</div>
		 	 <div class='{$this->getProperty('class')} {$cssShow}' style='padding: 1px;'>";


        if (!empty($rows)) { ?>

            <table class='table table-striped' cellpadding='0' cellspacing='0'>

                <thead>
                <?php
                $first = true;
                foreach ($rows as $kk => $v) {
                    $align = ($first) ? "left" : $this->properties["align"];
                    echo "<th align='" . $align . "' class='tableHead' style='padding:4px 8px'>{$this->properties["columnNames"][$kk]}</th>";
                    $first = false;
                }
                ?>
                </thead>

                <?php
                $counter = 0;
                if (!empty($this->data))
                    foreach ($this->data as $k => $v) {

                        $class = ($counter % 2 != 0) ? "class='tableRows tableRowNc'" : "class='tableRows tableRowC'";
                        $first = true;

                        echo "<tr {$class}>";
                        foreach ($rows as $kk => $r) {
                            $align = ($first) ? "left" : $this->properties["align"];
                            $this->data[$k][$kk] = (isset($this->data[$k][$kk])) ? $this->data[$k][$kk] : "-//-";
                            echo "<td align='" . $align . "' style='padding:2px 8px'>" . $this->data[$k][$kk] . "</td>";
                            $first = false;
                        }
                        echo "</tr>";
                        $counter++;
                    }
                ?>
            </table>
            <?php
        } else {
            echo "No data";
        }


        echo "<div style='clear:both'></div>";
        echo "</div></div>";


    }

}