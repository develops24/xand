<?php
class InputInteractive extends InputBase
{	
	public $search_str;
	
	/*
									format_query" 		=> "articul",
									"format_out_name"	=> "name",   // <A> которая в выпадающем  name
									"format_out_key
	
	*/
	
	
	//---------------
	public function SetDefaults()
	{
		$this->setProperty("height", 20)
			 ->setProperty("width", 202)
			 ->setProperty("titleWidth", 160) 
			 ->setProperty("db_filter","")  	
			 ->setProperty("button","") 
			 ->setProperty("format_out_name","name")  	  		
			 ->setProperty("class","inputText");  
	}	
	
	//---------------
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	return ", '".$this->key."':".$this->domId."_input_value";			
	}
	
	//---------------
	public function SetSearch($str)
	{
		$this->search_str = $str;		
		return $this;	
	}	
	
	//---------------
	public function DrawInteractive()
	{
	   
	    if($this->properties['db_filter'] != "")  $this->properties['db_filter'] .= " AND ";

        $model = (new Model())->setTableName($this->properties['table']);

        $arr = $model->findAll("{$this->properties['db_filter']} {$this->properties['format_query']} LIKE '%{$this->search_str}%'
						 								  AND `{$this->properties['format_query']}` != ''
														 ", [], [$this->properties['format_out_name'] => "ASC"], "0,20");
		if(!empty($arr))
			foreach($arr as $val)
			{				
				 echo "<a href='";
				 if(is_array($this->properties['format_out_key']))
				 {			 	
					
					$format_out_key_implode = array();
					
					foreach($this->properties['format_out_key'] as $format_out_item)
					{
						array_push($format_out_key_implode, $val[$format_out_item]);
					}	
					echo implode("#", $format_out_key_implode);				
				 }
				 else
				 {				 
				 	echo $val[$this->properties['format_out_key']];				
				 }
				 
				
				$original  = $val[$this->properties['format_out_name']];
				$_needle   = mb_strtolower($this->search_str, 'UTF-8');				
				$_haystack = mb_strtolower($val[$this->properties['format_out_name']], 'UTF-8');
				
				$pos = stripos($_haystack,$_needle);
				
				$ot  = substr($original,0, $pos);
				$do  = substr($original,$pos + strlen($_needle), strlen($original));
				
								 
				$start_hl = "<strong>";				 
				$end_hl   = "</strong>";	
				
				$val[$this->properties['format_out_name']] =	$ot.$start_hl.$this->search_str.$end_hl.$do;
								
				echo "' onclick='{$this->domId}Click(this); return false;' title='{$original}'>
						{$val[$this->properties['format_out_name']]} 
						</a>";
			}
		else echo("Not found"); 
		
		//echo Db::$log;
		
	}
	
	//---------------
	public function Draw()
	{
		
		if(! isset($this->properties['titleDisable']))
		echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";	  
 			  			
		echo "<div id='{$this->domId}_wrap' class='input_interactive_wrap'>
			  <input id='{$this->domId}' type='text' class='{$this->properties['class']}'  value='{$this->data}'
					 style = 'width:{$this->properties['width']}px;' onkeyup='{$this->domId}Press(this);'  />					
			  <div class='input_interactive_dd' id='{$this->domId}_drop_down'></div>
			  	{$this->properties['button']}
			  </div>";	
			  
		if(! isset($this->properties['titleDisable'])) echo "</div>";		   			  
		
		?>
             
		<script>
		var <?php echo $this->domId; ?>_input_value         = "";
		var <?php echo $this->domId; ?>_inputText          = "";
		var <?php echo $this->domId; ?>_onClick	         = null;
		
		
		var <?php echo $this->domId; ?>_input_dom 			 = $("#"+'<?php echo $this->domId; ?>');				
		var <?php echo $this->domId; ?>_input_dom_drop_down = $("#"+'<?php echo $this->domId; ?>_drop_down');				
		var <?php echo $this->domId; ?>_ajax_block = false;
		
		
		
		function <?php echo $this->domId; ?>GetData()
		{
			var arr = [];
			arr[0] = <?php echo $this->domId; ?>_input_value;
			arr[1] = <?php echo $this->domId; ?>_inputText;
			return arr;
		}			
		
		function <?php echo $this->domId; ?>Click(dom)
		{
			var ii_item = $(dom);			
			<?php echo $this->domId; ?>_input_dom_drop_down.css("display","none");	
						
			<?php echo $this->domId; ?>_input_value = ii_item.attr('href');
			<?php echo $this->domId; ?>_inputText  = ii_item.attr('title');			
		
			<?php echo $this->domId; ?>_input_dom.val(<?php echo $this->domId; ?>_inputText);			
			if(<?php echo $this->domId; ?>_onClick != null) 	<?php echo $this->domId; ?>_onClick(); 
			
		}		
		
		function <?php echo $this->domId; ?>Press(dom)
		{
			
			<?php echo $this->domId; ?>_input_dom_drop_down.css("display","block");
					
			//--------------------------- ajax
			if(<?php echo $this->domId; ?>_ajax_block == false) 
			{
				 <?php echo $this->domId; ?>_ajax_block = true;
				 
				 $.ajax({  
					 type: 'POST',  
					 url:  '<?php echo $this->properties['backends']; ?>', 
					 data: 'serialize_data=<?php 
					 
					 	$this->parent = NULL;
						echo serialize($this); 
					 
					 ?>&search='+<?php echo $this->domId; ?>_input_dom.val(),  				 
					 success: function(html)
					 { 				 	
						 <?php echo $this->domId; ?>_input_dom_drop_down.html(html);		
						 <?php echo $this->domId; ?>_ajax_block = false;			
					 }
				 }); 
			}			
		}		
		
		</script>     
        
        <?php
	}
	//---------------
}

?>
