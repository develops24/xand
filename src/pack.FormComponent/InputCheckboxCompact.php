<?php
 
class InputCheckboxCompact extends InputCheckbox
{

	//-------
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false && ! $this->properties['disabled'])
			return ", '".$this->key."': CheckboxGetData('{$this->domId}')";
	}

	//-------
	public function Draw()
	{
		$this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;

		$check = ($this->data == 1) ? 'Sel' : 'UnSel';
		$class = ($this->properties['disabled']) ? " checkboxDisabled checkboxDisabled{$check}" : "checkbox{$check}";

		echo "<div class='formElementCompact'>
                 <a href='' id='{$this->domId}' class='checkbox {$class}' onclick='CheckboxClick(this); return false;'>
				    ".$this->getProperty('requirements')." {$this->name}
			     </a>
              </div>";
	}
	
}