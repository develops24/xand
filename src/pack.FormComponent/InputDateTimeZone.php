<?php

/**
 * @author Kubrey <kubrey@gmail.com>
 */
class InputDateTimeZone extends InputBase {

//    private static $instance;

//    public function __construct() {
//        parent::__construct();
//        $arr = func_get_args();
//        $this->data = $arr[3];
//        $this->name = $arr[0];
//    }

//    public function &getInstance() {
//        if (!self::$instance) {
//            parent::__construct();
//            self::$instance = true;
//            echo "<script type='text/javascript' src='{{DirCore}}/pack.Jquery/jquery-ui-timepicker-addon.js'></script>";
//        }
////        return self::$instance;
//    }
    //---------------
    public function SetDefaults() {
        $this->setProperty("titleWidth", 160)
                ->setProperty("width", 200)
                ->setProperty("requirements", "")
                ->setProperty("class", "inputText");
    }

    //---------------
    public function GetDataJson() {
        if ($this->properties['ignore'] == false)
            return ", '" . $this->key . "':$('#" . $this->domId . "').val()";
    }

    //---------------
    public function Draw() {
        if ($this->data == "" || substr_count($this->data, "T") == 0) {
//            $this->data = date("Y-m-d") . "T" . date("H:i:s") . "+01:00";
        }
//        echo $this->data.'dg';
//        $dataExp = explode("T", $this->data);
//        $data = array();
//        $data[0] = $this->data;
//        $data[1] = substr($dataExp[1], 0, 8);

        if (!isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        echo "<input type='text' id='{$this->domId}' style='width:{$this->properties['width']}px; margin-right:5px;'
			   class='{$this->properties['class']}' value='".date('Y-m-d H:i:s',$this->data)."'/>";

//        echo "<input type='text' id='{$this->domId}hour' style='width:70px; margin-right:10px;'
//			   class='{$this->properties['class']}' placeholder='hh:mm:ss' value='{$data[1]}'/>";

//        echo strtotime(date('Y-m-d',$this->data).' T '.date('H:i',$this->data).' +0100');
//         echo "\n".strtotime(date('Y-m-d',$this->data).' T '.date('H:i',$this->data).' +0200');
        if ($this->getProperty('requirements') != "")
            echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";

        if (!isset($this->properties['titleDisable']))
            echo "</div>";
        ?>
        <script>
                    
                                                		  
            $(function() {
                $("#<?php echo $this->domId; ?>").datetimepicker({
                    "dateFormat"      : "yy-mm-dd",
                    "timeFormat":"HH:mm:ss",
                    "separator":" ",
                    "showButtonPanel" : true,
                    "changeMonth"     : true,
                    "changeYear"      : true,	
                    "setDate"         : new Date()
                });
//                $("#<?php //echo $this->domId; ?>").text('');															 
                                                 
            });
        </script>
        <?php
    }

    //---------------
}
?>

