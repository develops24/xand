<?php
 
class InputSwitcher extends InputBase
{		
	//---
	public function SetDefaults()
	{
		$this->setProperty("eventOnClick","")
			 ->setProperty("titleWidth",160)
			 ->setProperty("disabled", false);
						
	}

    //---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false && ! $this->properties['disabled'])	
				return ", '".$this->key."': SwitchGetData('{$this->domId}')";
	}

    //---
	public function Draw()
	{
		
	    $disabled = ($this->properties['disabled']) ? " checkboxDisabled" : "";	
	 
	
		if(! isset($this->properties['titleDisable']))
		{
			echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>";	
			if(!isset($this->properties['titleRight'])) 
					echo "<span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";		
		} 
		
		
    	$content = ($this->data == 1) 
				? "<p class='switchButton switchOff'>Off</p><p class='switchButton switchOn'>On</p>" 
				: "<p class='switchButton switchOnGray'>Off</p><p class='switchButton switchOff'>On</p>" ;	
				  
       
	   echo "<div  class='switchWrapWrap'>
				 <div id='{$this->domId}' class='switchWrap' onclick='SwitchAction(this); return false;'> {$content} </div>
		   		 ".$this->getProperty('requirements')."
       	     </div>";
	  
	
	
	    if(isset($this->properties['titleRight'])) echo "<label class='inputCheckboxLabel' for='{$this->domId}'>{$this->name}</label>";		
	   
	   	if($this->getProperty('warning') != "") 
			echo "<div style='margin-top:5px'>
				   	<div class='messageWrap messageError' style='border-radius: 0; padding:10px; display:inline-block;'>{$this->properties['warning']}</div>
				  </div>";	
	   
	   
	    if(! isset($this->properties['titleDisable'])) echo "</div>";
	   
	}
	
}
?> 