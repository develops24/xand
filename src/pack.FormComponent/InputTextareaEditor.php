<?php

/**
 * Class InputTextAreaEditor
 *
 * @тип     Class
 * @пакет    Form
 * @версия   2
 *
 *
 *
 *
 **/
class InputTextAreaEditor extends InputBase
{
    //---
    public function SetDefaults()
    {
        $this->setProperty("height", 50)
            ->setProperty("titleWidth", 160)
            ->setProperty("placeholder", "")
            ->setProperty("requirements", "");
    }

    //---
    public function GetDataJson()
    {
        if ($this->properties['ignore'] == false)
            return ", '" . $this->key . "': ContentPrepare($('#{$this->domId}').trumbowyg('html'))";
    }

    //---
    public function Draw()
    {

        if (isset($this->properties['dataForce']))
            $this->data = $this->properties['dataForce'];

        $this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;
        $this->data = str_replace("<br />", "\n", $this->data);
        $read = (isset($this->properties['readonly'])) ? 'readonly="' . $this->properties['readonly'] . '"' : "";

        if (!isset($this->properties['titleDisable']))
            echo "<div class='formElement-editor' style='padding-left:{$this->properties['titleWidth']}px'>
		  	  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        // Инициализация и вызов редактора
        echo "<div class='trumbowygEditor' id='{$this->domId}'>{$this->data}</div>";
        echo "<script>$(function() { 
                $('#{$this->domId}').trumbowyg({semantic: false}); 
             });</script>";

        if ($this->getProperty('requirements') != "")
            echo "<div class='formRequirements' style='margin: 5px 0  5px 0;'>" . $this->properties['requirements'] . "</div>";

        if (!isset($this->properties['titleDisable']))
            echo "</div>";

    }

}
