<?php
 
class GrouperCheckall extends GrouperBase
{		
	//-------
	public function SetDefaults()
	{
		$this->setProperty("class", "input_div")
			 ->setProperty("titleWidth",160) 
			 ->setProperty("ignore", true);
		
	}
	
	//---
	public function Draw()
	{
		$check = ($this->data) ? 'checkboxSel' : 'checkboxUnSel';

		?>
		<div class="grouperCheckallTitle">
			<a href="" class="checkbox <?=$check?>" onclick="ControlCheckall(this); return false;">
				<?=$this->name?>
			</a>
		</div>
		<div class = 'grouperCheckall'>

			<?php
			foreach($this->dataArray as $key => $val)
			{
				if($val->data == "" && isset($this->parent->dataArray[0][$val->key]))
					$val->data = $this->parent->dataArray[0][$val->key];

				$val->Draw();

			}
			?>

		</div>
        <?php
	}
}