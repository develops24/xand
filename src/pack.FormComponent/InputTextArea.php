<?php

/**
 * Class InputTextArea
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class InputTextArea extends InputBase
{
    /**
     *
     */
    public function setDefaults()
    {
        $this->setProperty("height", 50)
            ->setProperty("titleWidth", 160)
            ->setProperty("width", false)
            ->setProperty("placeholder", "")
            ->setProperty("requirements", "");
    }

    /**
     * @return string
     */
    public function getDataJson()
    {
        if ($this->properties['ignore'] == false)
            return ", '" . $this->key . "': getContentById('" . $this->domId . "')";
    }

    /**
     *
     */
    public function Draw()
    {
        if (isset($this->properties['dataForce']))
            $this->data = $this->properties['dataForce'];

        $this->data = str_replace("<br />", "\n", $this->data);

        $read = (isset($this->properties['readonly'])) ? 'readonly="' . $this->properties['readonly'] . '"' : "";
        $width = ($this->properties['width']) ? "width: {$this->properties['width']}px" : "";

        if (!isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
		  	      <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        echo "<textarea id='{$this->domId}' class='inputTextarea' {$read} placeholder='{$this->properties['placeholder']}'
				  style='height:{$this->properties['height']}px; {$width}'>{$this->data}</textarea>";

        if ($this->getProperty('requirements') != "")
            echo "<div class='formRequirements' style='margin: 5px 0  5px 0;'>" . $this->properties['requirements'] . "</div>";

        if (!isset($this->properties['titleDisable']))
            echo "</div>";

    }

}


