<?php
class InputNumericPassword extends InputBase
{	
	//---------------
	public function SetDefaults()
	{
		$this->setProperty("titleWidth",160)
			 ->setProperty("width", 250)
			 ->setProperty("requirements","")
             ->setProperty("placeholder", "")
             ->setProperty("class","inputText")
			 ->setProperty("maxValue","10000000")
			 ->setProperty("minValue","1")
			 ->setProperty("increment","1");
	}	
	//---------------
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	return ", '".$this->key."':$('#".$this->domId."').val()";			
	}
	//---------------
	public function Draw()
	{

		$this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;			
		if($this->data == "") 	$this->data = "";
		 
		if(! isset($this->properties['titleDisable']))
		echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
			  
		echo "<input type='text' id='{$this->domId}'
			    style='width:{$this->properties['width']}px; padding-left: 10px; '
				max='{$this->properties['maxValue']}'  
				min='{$this->properties['minValue']}'
			    step='{$this->properties['increment']}'
			    class='{$this->properties['class']}'
			    onkeyup='return InputNumericPasswordKeyUp(this);'
			    onchange='return InputNumericPasswordKeyUp(this);'
			    placeholder='{$this->properties['placeholder']}'
			    value='{$this->data}'/>";	
			   
		if($this->getProperty('requirements') != "") echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";
			   		  		
		if(! isset($this->properties['titleDisable'])) echo "</div>";
	}
	//---------------
}

?>