<?php

/**
 * Class InputDatetime
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class InputDatetime extends InputBase
{
    /**
     *
     */
    public function setDefaults()
    {
        $this->setProperty("titleWidth", 160)
            ->setProperty("width", 160)
            ->setProperty("requirements", "")
            ->setProperty("class", "inputText");
    }

    /**
     * @return string
     */
    public function getDataJson()
    {
        if ($this->properties['ignore'] == false)
            return ", '" . $this->key . "':$('#" . $this->domId . "').val()+'T'+$('#" . $this->domId . "hour').val()+'#PL#02:00'";
    }

    /**
     *
     */
    public function Draw()
    {
        $this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;
        if ($this->data == "") {
            $this->data = date("Y-m-d") . "T" . date("H:i:s") . "+00:00";
        } else {
            if (strpos($this->data, "T") === false) {
                $this->data = date("c", $this->data);
            }
        }

        $dataExp = explode("T", $this->data);

        $data = array();
        $data[0] = $dataExp[0];
        $data[1] = substr($dataExp[1], 0, 8);

        if (!isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        echo "<input type='date' id='{$this->domId}' style='width:{$this->properties['width']}px; margin-right:5px;'
			   class='{$this->properties['class']}' value='{$data[0]}'/>";

        echo "<input type='text' id='{$this->domId}hour' style='width:70px; margin-right:10px;'
			   class='{$this->properties['class']}' placeholder='hh:mm:ss' value='{$data[1]}'/>";


        if ($this->getProperty('requirements') != "") echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";

        if (!isset($this->properties['titleDisable'])) echo "</div>";
    }

}

