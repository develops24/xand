<?php

/**
 * Class I18nDate
 */
class I18nDate
{
    /**
     * @var array
     */
    private static $months_ru = array(
        'Jan' => array('янв', 'Января'),
        'Feb' => array('Фев', 'Февраля'),
        'Mar' => array('мар', 'Марта'),
        'Apr' => array('апр', 'Апреля'),
        'May' => array('мая', 'Мая'),
        'Jun' => array('июн', 'Июня'),
        'Jul' => array('июл', 'Июля'),
        'Aug' => array('авг', 'Августа'),
        'Sep' => array('сен', 'Сентября'),
        'Oct' => array('окт', 'Октября'),
        'Nov' => array('ноя', 'Ноября'),
        'Dec' => array('дек', 'Декабря')
    );


    /**
     * @param $date_stamp
     * @param bool $long
     * @return string
     */
    public static function FormatHoursRu($date_stamp, $long = false)
    {
        $index = ($long) ? 1 : 0;
        return strtr(date("j M, H:i:s", $date_stamp), self::$months_ru[$index]);
    }

    /**
     * @param $seconds
     * @return string
     */
    public static function GetDHMExtend($seconds)
    {
        $days = floor($seconds / 86400);
        $seconds = $seconds - ($days * 86400);
        $hours = floor($seconds / 3600);
        $seconds = $seconds - ($hours * 3600);
        $minutes = floor($seconds / 60);

        $result = "";

        $daysRLast = $days % 10;
        $daysRLast = ($daysRLast > 0) ? $daysRLast : $days;

        $hoursRLast = $hours % 10;
        $hoursRLast = ($hoursRLast > 0) ? $hoursRLast : $hours;

        $minutesRLast = $minutes % 10;
        $minutesRLast = ($minutesRLast > 0) ? $minutesRLast : $minutes;

        $secondsRLast = $seconds % 10;
        $secondsRLast = ($secondsRLast > 0) ? $secondsRLast : $seconds;

        $userLang = I18n::getUserLanguage();

        switch ($userLang) {

            case "ru-UA":

                $daysSuffix = "дней";
                if ($daysRLast == 1 && ($days < 10 || $days > 20))
                    $daysSuffix = "день";
                if ($daysRLast > 1 && $daysRLast < 5 && ($days < 10 || $days > 20))
                    $daysSuffix = "дня";

                $hoursSuffix = "часов";
                if ($hoursRLast == 1 && ($hours < 10 || $hours > 20))
                    $hoursSuffix = "час";
                if ($hoursRLast > 1 && $hoursRLast < 5 && ($hours < 10 || $hours > 20))
                    $hoursSuffix = "часа";

                $minutesSuffix = "минут";
                if ($minutesRLast == 1 && ($minutes < 10 || $minutes > 20))
                    $minutesSuffix = "минуту";
                if ($minutesRLast > 1 && $minutesRLast < 5 && ($minutes < 10 || $minutes > 20))
                    $minutesSuffix = "минуты";

                $secondsSuffix = "секунд";
                if ($secondsRLast == 1 && ($seconds < 10 || $seconds > 20))
                    $secondsSuffix = "секунду";
                if ($secondsRLast > 1 && $secondsRLast < 5 && ($seconds < 10 || $seconds > 20))
                    $secondsSuffix = "секунды";

                break;

            default:
                $daysSuffix = "days";
                if ($days == 1)
                    $daysSuffix = "day";

                $hoursSuffix = "hours";
                if ($hours == 1)
                    $hoursSuffix = "hour";

                $minutesSuffix = "minutes";
                if ($minutes == 1)
                    $minutesSuffix = "minute";

                $secondsSuffix = "seconds";
                if ($minutes == 1)
                    $secondsSuffix = "second";

        }

        if ($days > 0)
            $result .= $days . " " . $daysSuffix . ", ";

        if ($hours > 0 && $hours < 24)
            $result .= $hours . " " . $hoursSuffix . ", ";

        if ($minutes > 0 && $minutes < 60)
            $result .= $minutes . " " . $minutesSuffix . ", ";

        if ($seconds > 0 && $seconds < 60)
            $result .= $seconds . " " . $secondsSuffix . ", ";

        return trim($result, ", ");
    }


}