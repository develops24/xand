<?php

/**
 * Description of class
 *
 * @author kubrey
 */
class Stemplater
{
    public $data = array();


    /**
     * @param string $tplFolder
     * @param string $tplPath Имя шаблона
     * @param array $vars
     * @return string Верстка шаблона
     */
    public function render($tplFolder, $tplPath, $vars = array()) {

        $tplFolder = (!$tplFolder)
            ? PATH_REAL . DS . PATH_PUBLIC . PATH_TPL . DS . PATH_THEME
            : PATH_REAL . DS . PATH_PUBLIC . PATH_TPL . DS . $tplFolder;

        extract($vars, EXTR_SKIP);
        $this->data = array_key_exists('data', $vars) ? $vars['data'] : $vars;
        $this->data['stpl'] = $this;
        ob_start();

        $path = $tplFolder . DS . $tplPath . ".phtml";

        if (file_exists($path)) {
            include($path);
        } else {
            echo "tpl does not exist: " . $path;
        }

        $f = ob_get_clean();
        return $f;
    }

    /**
     * @param string $tplString строка с содержание шаблона
     * @param array $vars
     * @return string Верстка шаблона
     */
    public function renderString($tplString, $vars = array()) {
        extract($vars, EXTR_SKIP);
        $this->data = array_key_exists('data', $vars) ? $vars['data'] : $vars;
        $this->data['stpl'] = $this;
        ob_start();
        echo $tplString;

        $f = ob_get_clean();
        return $f;
    }

}
