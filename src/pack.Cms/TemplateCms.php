<?php

/**
 * Class TemplateCms
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class TemplateCms extends BaseTemplate
{

    public $forms = array();
    public $sources = array();


    /**
     * TemplateCms constructor.
     *
     * @param string $tplFile
     * @param bool $dir
     */
    public function __construct($tplFile = "", $dir = false)
    {
        $this->tplFile = $tplFile . ".phtml";
        $this->dir = ($dir)
            ? PATH_APP . DS . $dir . DS . "views"
            : PATH_PUBLIC . PATH_TPL . DS . PATH_THEME . DS . parent::TPL_FOLDER;
    }

    /**
     * @param $file
     *
     * @return string
     */
    public function Informer($file)
    {

        $informerObjectNameArr = explode(DS, $file);
        $informerObjectName = "";

        foreach ($informerObjectNameArr as $name)
            $informerObjectName .= ucfirst($name);

        if (class_exists($informerObjectName)) {

            $this->sources[] = '$this->assignArray((new ' . $informerObjectName . ')->getContent());';
            return $this->Inc($file);

        } else {
            return "Informer: '{$informerObjectName}' doesn't exists!";
        }
    }



    /**
     * Add menu by parent or All
     *
     * @param $file
     * @param $parent
     *
     * @return bool|string
     */
    public function Menu($file, $parent = false)
    {
        $menuObjectNameArr = explode(DS, $file);
        $menuObjectName = "";

        foreach ($menuObjectNameArr as $name)
            $menuObjectName .= ucfirst($name);

        if (class_exists($menuObjectName)) {
            $this->sources[] = '$this->assignArray((new ' . $menuObjectName . ')->getMenu(' . $parent . '));';
            return $this->Inc($file);

        } else {
            return "Informer: '{
                $menuObjectName}' doesn't exists!";
        }
    }



    /**
     * @param $par
     * @param bool $call
     *
     * @return string
     */
    public function Form($par, $call = true)
    {
        if ($call) {
            return $this->forms[$par]->Draw();
        } else {
            return (isset($this->forms[$par])) ? '<?php echo $this->Form("' . $par . '"); ?>' : "";
        }
    }


    /**
     * @param $group
     * @param $obj
     *
     * @return $this
     */
    public function AssignForm($group, $obj)
    {
        $this->forms[$group] = $obj;
        return $this;
    }

    /**
     * @param $par
     *
     * @return string
     */
    public function Img($par)
    {
        return "<img src='" . PATH_DS . PATH_TPL . DS . PATH_THEME . DS . "img" . DS . $par . "' class='normal'/>";
    }

    /**
     * @param $par
     *
     * @return string
     */
    public function ImgRight($par)
    {
        return "<img src='" . PATH_DS . PATH_TPL . DS . PATH_THEME . DS . "img" . DS . $par . "' class='right'/>";
    }

    /**
     * @param $par
     *
     * @return string
     */
    public function ImgLeft($par)
    {
        return "<img src='" . PATH_DS . PATH_TPL . DS . PATH_THEME . DS . "img" . DS . $par . "' class='left'/>";
    }




}
