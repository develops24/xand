<?php

/**
 * Class I18n
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class I18n
{

    private static $_init;
    private static $_currentModule;
    private static $_table = "dictionary";

    public static $dictionary = array();
    public static $dictionary_new = array();

    /**
     * @param $data
     */
    public static function Append($data)
    {
        self::$dictionary = array_merge(self::$dictionary, $data);
    }

    /**
     * @param string $content
     *
     * @return bool
     */
    private static function _isHtmlLayout($content = "")
    {
        if (strpos($content, "<") !== false) return true;
        if (strpos($content, ">") !== false) return true;
        if (mb_strlen($content) > 200) return true;

        return false;
    }

    /**
     *
     * @param string $content
     * @param array $params - подмена
     *
     * @return string
     */
    public static function __($content, $params = [])
    {

        if (!$content) {
            return "";
        }

        if (!self::$_init) {
            if (!defined('SITE_CURRENT_LANG')) {
                define('SITE_CURRENT_LANG', SITE_DEFAULT_LANG);
            }

            self::setCurrentModule();
            self::loadDictionary(SITE_CURRENT_LANG);
        }

        if (defined("SITE_LEARNING_MODE") && SITE_LEARNING_MODE === true) {
            $contentTranslated = preg_replace_callback("|%[A-Z0-9 ].*%|U", array("I18n", "Translate"), $content);
        } else {
            $contentTranslated = strtr($content, self::$dictionary);
            if (!$contentTranslated) {
                $contentTranslated = substr($content, 1, -1);
            }
        }

        if ($contentTranslated == $content) {
            $contentTranslated = strtr("%" . trim($content, "%") . "%", self::$dictionary);
            if ($contentTranslated == "%" . trim($content, "%") . "%") {
                $contentTranslated = trim($content, "%");
            }
        }

        if (($contentTranslated == $content || trim($content, "%") == $contentTranslated)) {

            if (!self::_isHtmlLayout($content)) {
                if (isset(self::$dictionary["%" . trim($content, '%') . "%"]) && self::$dictionary["%" . trim($content, '%') . "%"]) {
                    $contentTranslated = self::$dictionary["%" . trim($content, '%') . "%"];
                } elseif (defined("SITE_LEARNING_MODE") && SITE_LEARNING_MODE === true) {
                    self::putIntoNewDictionary(trim($content, '%'));
                }
            }
        }

        if (is_array($params) && $params) {
            foreach ($params as $key => $holder) {
                $contentTranslated = str_replace($key, $holder, $contentTranslated);
            }
        }

        return $contentTranslated;
    }

    /**
     *
     */
    private static function setCurrentModule()
    {
        self::$_currentModule = Xand::$currentModule;

        if (self::$_currentModule == "") {
            self::$_currentModule = XAND_DEFAULT_MODULE;
        }
    }

    /**
     * @param $lang
     */
    private static function loadDictionary($lang)
    {

        $raw = SDb::rows("
                SELECT original, translate, status
                FROM " . self::$_table . "
                WHERE lang=? AND (module=? OR module=?)",
            array($lang, self::$_currentModule, "common"));

        foreach ($raw as $r) {
            if ($r["status"] == 0) {
                self::$dictionary_new[$r["original"]] = $r["translate"];
            } else {
                self::$dictionary[$r["original"]] = $r["translate"];
            }
        }

        self::$_init = true;
    }

    /**
     * @param $wordWrapped
     *
     * @return string
     */
    private static function Translate($wordWrapped)
    {
        $wordWrapped = array_pop($wordWrapped);
        $word = trim($wordWrapped, "%");
        if (isset(self::$dictionary[$wordWrapped])) {
            return (self::$dictionary[$wordWrapped]) ? trim(self::$dictionary[$wordWrapped], "%") : $word;
        }

        if (!isset(self::$dictionary[$wordWrapped])) {
            self::putIntoNewDictionary($word);
            return $word;
        } else {
            return (self::$dictionary[$wordWrapped]) ? trim(self::$dictionary[$wordWrapped], "%") : $word;
        }
    }

    /**
     * @param $word
     */
    private static function putIntoNewDictionary($word)
    {
        if (!$word) return;

        $word = "%" . $word . "%";

        if (isset(self::$dictionary_new[$word]))
            return;

        if (strpos($word, "(") !== false) {
            return;
        }

        SDbQuery::table(self::$_table)->insert([
            "lang" => SITE_CURRENT_LANG,
            "status" => 0,
            "module" => self::$_currentModule,
            "original" => $word,
            "translate" => $word,
            "dateUpdate" => time(),
            "dateCreate" => time()
        ]);

        self::$dictionary[$word] = $word;
    }


}
