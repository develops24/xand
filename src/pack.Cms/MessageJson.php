<?php

/**
 * Class MessageJson
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class MessageJson extends Message
{
    public $debugMode = false;

    /**
     * @var array
     */
    protected $callbacks = [];

    /**
     * MessageJson constructor.
     */
    private function __construct() {
    }

    /**
     * @return MessageJson
     */
    public static function I() {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function Redirect($url = "/") {
        array_push($this->queue, array(
            "body" => $url,
            "container" => "redirect"
        ));
        return $this;
    }

    /**
     * @param int $timeout
     *
     * @return $this
     */

    public function Refresh($timeout = 1000) {
        array_push($this->queue, array(
            "body" => $timeout,
            "container" => "refresh"
        ));
        return $this;
    }

    /**
     * @param bool $status
     * @param array $data
     */
    public function Json($status = true, $data = array())
    {
        if (!empty($this->queue)) {

            $output = array(
                "status" => $status,
                "messages" => $this->queue
            );

            if (!empty($data))
                $output = array_merge($output, $data);

            if ($this->debugMode) {
                Debug::Dump($output);
                exit();
            }

            echo json_encode($output);
            exit();
        }
    }


    /**
     * @param $cb
     *
     * @return $this
     */
    public function callback($cb) {
        array_push($this->queue, [
            "body" => $cb,
            "container" => "callback"
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    protected function runCallbacks() {
        foreach ($this->callbacks as $cb) {
            if (!isset($this->queue['callbacks'])) {
                $this->queue['callbacks'] = [];
            }
            $this->queue['callbacks'][] = $cb;
        }
        return $this;
    }

    /**
     *
     */
    private function __clone() {
    }


}
