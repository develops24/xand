<?php

/**
 * Class Content
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Content
{
    /**
     * @var array
     */
    private static $lang = array(
        '#AM#' => '&',
        '#DK#' => '"',
        '#OK#' => "'",
        '#PL#' => "+",
        '#BR#' => '<br />',
        '#TB#' => '',
        '#TT#' => '',
        '<' => '&lt',
        '>' => '&gt'
    );

    /**
     * @var array
     */
    private static $langInvert = array(

        '&' => '#AM#',
        '"' => '#DK#',
        "'" => '#OK#',
        '&lt' => '<',
        '&gt' => '>',
        '\n' => '#TB#',
        '\t' => '#TT#'
    );

    /**
     * @param $string
     *
     * @return string
     */
    public static function Encode($string)
    {
        return strtr($string, self::$langInvert);
    }

    /**
     * @param $string
     *
     * @return string
     */
    public static function Decode($string)
    {
        return strtr($string, self::$lang);
    }

    /**
     * @param $url
     * @param bool $extraMode
     *
     * @return string
     */
    public static function createUrl($url, $extraMode = true)
    {

        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g",
            "Д" => "d", "Е" => "e", "Ж" => "j", "З" => "z", "И" => "i",
            "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " " => "-", "." => "", "/" => "-"
        );

        if ($extraMode === false)
            $tr["."] = ".";

        $url = strtr($url, $tr);

        for ($i = 0, $out_url = ""; isset($url[$i]); $i++) {
            $out_url .= (
                ($url[$i] >= 'a' && $url[$i] <= 'z') ||
                ($url[$i] >= 'A' && $url[$i] <= 'Z') ||
                ($url[$i] >= '0' && $url[$i] <= '9') ||
                $url[$i] == '_' ||
                $url[$i] == '-' ||
                (! $extraMode && $url[$i] == '.')
            )  ? $url[$i] : "";
        }

        $out_url = preg_replace('/[^A-Za-z0-9\._\-]/', '', $out_url);

        return trim(strtolower($out_url));
    }


    /**
     * @param $string
     * @param $list
     *
     * @return mixed
     */
    public static function stopWordsReplace($string, $list)
    {

        if (!file_exists($list))
            Message::I()->Error("Stop list not found")->Draw()->E();

        $text = file_get_contents($list);

        $textArr = explode(",", $text);
        foreach ($textArr as $word) {
            $word = trim($word);
            $string = str_replace($word, "[censored]", $string);
        }
        return $string;
    }

}