<?php

/**
 **/

class WidgetChartTime extends WidgetHighcharts
{	
	 
	//---
	public function Draw()
	{	 
		
	echo "
	
	<div id='{$this->eid}' style='min-width: 300px; height: ".$this->properties["height"]."px; 
			margin: 0 auto; margin-bottom: 10px;'></div>
	
	<script>
			var {$this->eid};			 
		
			chart = new Highcharts.Chart({
				chart: {
					renderTo: '{$this->eid}',
					type: 'spline',
					spacingBottom: 0
				},
				title: {
					text: '".$this->properties["title"]."'
				},
			   
				legend: {
					layout: 'vertical',
					align: 'left',
					verticalAlign: 'top',
					x: 50,
					y: 10,
					floating: true,
					borderWidth: 1,
					backgroundColor: '#FFFFFF'
				},
				xAxis: {
					gridLineWidth: 1, 
				   // tickInterval: 6400000, 
					type: 'datetime',
							dateTimeLabelFormats: {
								month: '%e. %b',
								year: '%b'
							} 
				},
				yAxis: {
					gridLineWidth: 1,
					min: 0,
					title: {
						text: ''
					},
					labels: {
						formatter: function() {
							return this.value + '';
						}
					}
				},
				 tooltip: {
							formatter: function() {
									return '<b>'+ this.series.name +'</b><br/>'+
									Highcharts.dateFormat('%e. %b', this.x) +': '+ this.y +' m';
							}
				 },
			
				plotOptions: {
					 
					spline: {
						 marker: {
									enabled: false,
									symbol: 'circle',
									radius: 2,
									states: {
										hover: {
											enabled: true
										}
									}
								},
						animation:  false, 
						dataLabels: {
							enabled: false
						},
					} 
			
					
				},
				credits: {
					enabled: false
				},
				series: ".json_encode($this->series)."
				 
			}); 
			</script>
			";
				
	}
}
?>