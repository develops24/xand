<?php

/**
 **/

class WidgetHighstock extends WidgetHighcharts
{	 

	//---
	function __construct($eid)
	{  
		parent::__construct($eid);
		$this->setProperty("height", 700)
			 ->setProperty("rangeSelectorSelected", 1);
	}
	
	//---
	public function Draw()
	{	 
		
		echo "
			<div id='{$this->eid}' style='min-width: 300px; height: ".$this->properties["height"]."px; 
				margin: 0 auto; margin-bottom: 10px;'></div>
		
				<script> 
					 $('#{$this->eid}').highcharts('StockChart', { 
							title : {
								align: 'left',
								text : '".$this->properties["title"]."'
							},

							xAxis : {

								minRange: 3600 * 1000 // one hour
							},
							
							yAxis: {
								gridLineWidth: 1,
								min: 0
							},

							rangeSelector : {
									buttons: [{
										type: 'hour',
										count: 1,
										text: '1h'
									}, {
										type: 'day',
										count: 1,
										text: '1d'
									}, {
										type: 'month',
										count: 1,
										text: '1m'
									}, {
										type: 'year',
										count: 1,
										text: '1y'
									}, {
										type: 'all',
										text: 'All'
									}],
									inputEnabled: false, // it supports only days
									selected : ".$this->properties["rangeSelectorSelected"]." // all
								},

							legend: {
								enabled: true,
								layout: 'vertical',
								align: 'left',
								verticalAlign: 'top',
								x: 10,
								y: 80,
								floating: true,
								borderWidth: 1,
								backgroundColor: '#FFFFFF',
								itemStyle: {
								   fontWeight: 'normal'
								}

							},
							 
							plotOptions: { 
								line: { 
									animation:  false  
								},
								spline: { 
									animation:  false  
								} 
							}, 
							 
							series: ".json_encode($this->series).",
							credits: {
								enabled: false
							}
						}); 
				</script>
	 
			 ";
				
	}
}
?>